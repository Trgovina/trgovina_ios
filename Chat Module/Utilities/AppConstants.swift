//
//  AppConstants.swift
//  MQTT Chat Module
//
//  Created by Rahul Sharma on 19/07/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//


import Foundation

class AppConstants  : NSObject {
    
    static let chatTabIndex = 3
    //8009
    
    /// Constant URL for calling APIs related to chat.
    static let constantURL = mChatAPIBaseUrl
    
    
    /// URL for uploading images in  multipart.
    static let uploadMultimediaURL = mMediaUploadForChat
    
    
    /// Used for loading image after uploading.
    static let imageURL = mImageUrlForChat
    
    // API Names
    static let loginAPI = "login" //post
    static let verifyEmail = "verifyEmail" //post
    static let signup = "signup" //post
    static let getUsers = "getUsers" //post
    static let uploadPhoto = "upload" //post
    static let uploadProfilePic = "upload/profile"
    static let getChats = "Chats" //get
    static let getMessages = "Messages" //get
    static let requestOTP = "RequestOTP" //post
    static let verifyOTP = "VerifyOTP" //post
    static let profile = "User/Profile" //put
 
    struct chatColor {
        
        static let chatBaseColor = UIColor(red: 0/255, green: 115/255, blue: 177/255, alpha: 1.0)
        static let chatBaseColor2 = UIColor(red: 250/255, green: 169/255, blue: 26/255, alpha: 1.0)
        static let navColor = UIColor.white
        static let chatDividerColor = UIColor(red: 219/255, green: 219/255, blue:219/255, alpha: 1.0)
        static let chatProductNotAvailableColor = UIColor(red: 239/255, green: 239/255, blue: 244/255, alpha: 0.9)
    }
    
    struct CouchbaseConstant {
        static let dbName = "mqttchatdb"
    }
    
    struct  UserDefaults {
        static let userID = "userId"
        static let indexDocID = "indexDocID"
        static let userName = "userName"
        static let userImage = "userImage"
        static let sessionToken = "userDetail"
        static let pushToken = "pushToken"
        static let mqttId = "mqttId"
        static let rechabilityNotificationKey = "rechabilityNotification"
    }
    
    struct indexDocumentConstants {
        static let userIDArray = "userIDArray"
        static let userDocIDArray = "userDocIDArray"
        static let chatDocument = "chatDocument"
    }
    
    struct MQTT {
        static let messagesTopicName = "Message/"
        static let acknowledgementTopicName = "Acknowledgement/"
        static let onlineStatus = "OnlineStatus/"
        static let typing = "Typ/"
        static let getChats = "GetChats/"
        static let getMessages = "GetMessages/"
        static let messageOffer = "MessageOffer/"
    }
}
