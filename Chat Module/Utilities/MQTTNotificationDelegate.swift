////
////  MQTTNotificationDelegate.swift
////  MQTT Chat Module
////
////  Created by Rahul Sharma on 29/07/17.
////  Copyright © 2017 Rahul Sharma. All rights reserved.
////
//
//import Foundation
//import UserNotifications
//
//class MQTTNotificationDelegate: NSObject, UNUserNotificationCenterDelegate {
//
//    @available(iOS 10.0, *)
//    func userNotificationCenter(_ center: UNUserNotificationCenter,
//                                willPresent notification: UNNotification,
//                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
//        // Play sound and show alert to the user
//        completionHandler([.alert,.sound])
//    }
//
//    @available(iOS 10.0, *)
//    func userNotificationCenter(_ center: UNUserNotificationCenter,
//                                didReceive response: UNNotificationResponse,
//                                withCompletionHandler completionHandler: @escaping () -> Void) {
//
//        // Determine the user action
//        if #available(iOS 10.0, *) {
//            switch response.actionIdentifier {
//            case UNNotificationDismissActionIdentifier:
//                print("Dismiss Action")
//            case UNNotificationDefaultActionIdentifier:
//                print("Default")
//            case "Snooze":
//                print("Snooze")
//            case "Delete":
//                print("Delete")
//            default:
//                print("Unknown action")
//            }
//        } else {
//            // Fallback on earlier versions
//        }
//        completionHandler()
//    }
//}



//
//  MQTTNotificationDelegate.swift
//  MQTT Chat Module
//
//  Created by Rahul Sharma on 29/07/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation
import UserNotifications

class MQTTNotificationDelegate: NSObject, UNUserNotificationCenterDelegate {
    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        // Play sound and show alert to the user
        if let userID = UserDefaults.standard.value(forKey: "currentUserId") as? String {
            let notificationData = notification.request.content.userInfo
            if let body = notificationData["body"] as? String {
                guard let data = body.data(using: .utf8) else { return }
                do {
                    guard let dataObj = try JSONSerialization.jsonObject(with: data, options: []) as? [String : Any] else { return }
                    guard let senderID = dataObj["senderID"] as? String else { return }
                    if userID == senderID {
                        completionHandler([.sound])
                    } else {
                        completionHandler([.sound,.alert])
                    }
                } catch let error {
                    print(error.localizedDescription)
                    completionHandler([.sound,.alert])
                }
            }
        } else {
            completionHandler([.sound,.alert])
        }
    }
    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
        
        let notificationData = response.notification.request.content.userInfo
        if let body = notificationData["body"] as? String {
            guard let data = body.data(using: .utf8) else { return }
            do {
                guard let dataObj = try JSONSerialization.jsonObject(with: data, options: []) as? [String : Any] else { return }
                self.openChatController(withData: dataObj)
            } catch let error {
                print(error.localizedDescription)
            }
        }
        completionHandler()
    }
    
    func openChatController(withData data: [String : Any]) {
        if let tabController = UIApplication.shared.keyWindow?.rootViewController as? UITabBarController {
            DispatchQueue.main.async {
                if let controller = tabController.viewControllers?[1] as? UINavigationController {
                    controller.popToRootViewController(animated: false)
                    tabController.selectedIndex = 1
                }
            }
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.5) {
                let name = NSNotification.Name(rawValue: "notificationTapped")
                NotificationCenter.default.post(name: name, object: nil, userInfo: ["chatObj":data])
            }
        }
    }
}


