//
//  ThankYouMessageCollectionViewCell.swift
//  Tac Traderz
//
//  Created by Sachin Nautiyal on 25/11/2017.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit


class ThankYouMessageCollectionViewCell: JSQMessagesCollectionViewCell {
    
    @IBOutlet weak var thanksViewOutlet: UIView!
    @IBOutlet weak var thankYouMessageLabelOutlet: UILabel!
    var chatVMObj : ChatViewModel!
    var msgObj  : Message! {
        didSet {
            
            if(self.thanksViewOutlet != nil)
            {
                self.thanksViewOutlet.roundCorners(corners:[.topRight,.bottomRight, .bottomLeft], radius: 10)
            }
            if (chatVMObj != nil && !msgObj.isSwap!) {
                var price = ""
                if let currency = chatVMObj.currency, let msg = msgObj.messagePayload {
                    price = currency+" "+msg
                } else {
                    price = msgObj.messagePayload ?? ""
                }
                let originalString = NSLocalizedString("Thanks for your offer of", comment:"Thanks for your offer of")
                let originalStrings = NSLocalizedString("I accept it", comment:"I accept it")
                self.thankYouMessageLabelOutlet.text = originalString +  "\(price)." +  originalStrings
            }
            else
            {
                if(msgObj.swapType == "2"){
                self.thankYouMessageLabelOutlet.text = "Thanks for your offer. Lets swap it."
                }
                else{
                self.thankYouMessageLabelOutlet.text = "Sorry I dont accept your swap offer."
                }
 
             }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code //215 50
    }
}


class ThankYouMessageMediaItem : NSObject, JSQMessageMediaData {
    
    let locationCell = ThankYouMessageCollectionViewCell(frame:CGRect(x: 0, y: 0, width: 215, height: 50))
    
    func mediaView() -> UIView? {
        return locationCell
    }
    
    func mediaPlaceholderView() -> UIView {
        return locationCell
    }
    
    func mediaViewDisplaySize() -> CGSize {
        return CGSize(width: 215, height: 50)
    }
    
    func mediaHash() -> UInt {
        return UInt(60000 + arc4random_uniform(1000))
    }
}

class SelfThankYouMessageMediaItem : NSObject, JSQMessageMediaData {
    
    let thanksYouCell = ThankYouMessageCollectionViewCell(frame:CGRect(x: 0, y: 0, width: 215, height: 50))
    
    func mediaView() -> UIView? {
        return thanksYouCell
    }
    
    func mediaPlaceholderView() -> UIView {
        return thanksYouCell
    }
    
    func mediaViewDisplaySize() -> CGSize {
        return CGSize(width: 215, height: 50)
    }
    
    func mediaHash() -> UInt {
        return UInt(60000 + arc4random_uniform(1000))
    }
}



