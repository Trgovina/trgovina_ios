//
//  PayPalLinkSharedCell.swift
//  Tac Traderz
//
//  Created by Sachin Nautiyal on 06/11/2017.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit


class PayPalLinkSharedCell: UICollectionViewCell {

    @IBOutlet weak var paypalText: UILabel!
    
    var priceText : String! {
        didSet {
            paypalText.text = "Paypal link Shared"
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}
class PayPalLinkSharedCellMediaItem : NSObject, JSQMessageMediaData {
    
    let CounterOfferCell = PayPalLinkSharedCell(frame:CGRect(x: 0, y: 0, width: 180, height: 60))
    
    func mediaView() -> UIView? {
        return CounterOfferCell
    }
    
    func mediaPlaceholderView() -> UIView {
        return CounterOfferCell
    }
    
    func mediaViewDisplaySize() -> CGSize {
        return CGSize(width: 180, height: 60)
    }
    
    func mediaHash() -> UInt {
        return UInt(60000 + arc4random_uniform(1000))
    }
}
