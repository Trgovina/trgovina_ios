//
//  Helper.h

//
//  Created by Rahul Sharma on 9/3/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WebServiceConstants.h"
#import "UserDetails.h"
#import "ProductDetails.h"
@interface Helper : NSObject


/**
 Method to get Posted time.

 @param seconds postedOn timeStamp.
 @return time.
 */
+(NSString *)PostedTimeSincePresentTime:(NSTimeInterval)seconds;

/**
 This method is invoked to get the duration between posted time to the present time.

 @param epochTime epoch time stamp.
 @return duration time.
 */
+(NSString *)convertEpochToNormalTime :(NSString *)epochTime;


+(NSString *)convertEpochToNormalTimeInshort :(NSString *)epochTime;

/**
 This method is to get the customisedActivity statement for Activity.

 @param username name of the user.
 @param statment statement string.
 @return attributed Statement.
 */
+(NSMutableAttributedString*)customisedActivityStmt:(NSString*)username :(NSString*)statment;


/**
 This method is to get the customisedActivity statement for Activity.

 @param username name of the user.
 @param secondUserName name of the second user.
 @param time time stamp.
 @param statment statement to be customised.
 @return attributed statement.
 */
+(NSMutableAttributedString*)customisedActivityStmt:(NSString*)username seconUserName:(NSString *)secondUserName  timeForPost:(NSString *)time : (NSString*)statment;




/**
 Method to measure height of the Label on the basis of text.

 @param label Label refrence.
 @return height.
 */
+ (CGFloat)measureHieightLabel: (UILabel *)label;



/**
 Method to measure height of the Label on the basis of text.
 
 @param label Label refrence.
 @return height.
 */
+(CGFloat )heightOfText:(UILabel *)label;


+(void)setToLabel:(UILabel*)lbl Text:(NSString*)txt WithFont:(NSString*)font FSize:(float)_size Color:(UIColor*)color;
+(void)setButton:(UIButton*)btn Text:(NSString*)txt WithFont:(NSString*)font FSize:(float)_size TitleColor:(UIColor*)t_color ShadowColor:(UIColor*)s_color;


/**
 This method is invoked to store user details on login/Signup.

 @param user UserDetails Model Object.
 */
+(void)storeUserLoginDetails:(UserDetails *)user;


/**
 Get UserName on the basis of login.

 @return logged in user name.
 */
+(NSString *)userName;


/**
 Method to get the authToken.

 @return return auth token for logged user.
 */
+(NSString *)userToken;


/**
 Method to get the MQTTID.

 @return MQTTID of logged user.
 */
+(NSString *)getMQTTID;


/**
 Get logged user profile pic Url in string format.

 @return profile pic url string.
 */
+(NSString *)userProfileImageUrl;



/**
 Method to get the push token.

 @return push token for logged user.
 */
+(NSString *)deviceToken;

/**
 This method is invoked to get the currency symbol on the basis of currency.

 @param curr currency.
 @return currency symbol.
 */
+(NSString*)returnCurrency:(NSString *)curr;



/**
 This method is to convert postUrl to tiny url.

 @param postDic postDetails.
 @return tiny Url.
 */
+ (NSString*)getWebLinkForFeed:(NSString *)postDic;




#pragma mark -
#pragma mark - Check Facebook Login.
/**
 Check Facebook login from anyviewController.

 @param VCReferenceObject viewController reference object.
 */
+(void)checkFbLoginforViewController :(UIViewController *)VCReferenceObject;


/**
 This method is to convert media links to directory path for Instagram sharing.

 @param param postDetails.
 @return path.
 */
+(NSString *)instagramSharing:(NSString *)param;


/**
 Facebook Sharing Method.

 @param params post details.
 */
+ (void)makeFBPostWithParams:(ProductDetails *)params;


/**
 This method is to show alert popup with Ok Action.

 @param title alert title.
 @param message alert message.
 @param viewControllerRefrence viewController refrence to present popup.
 */
+(void)showAlertWithTitle:(NSString*)title Message:(NSString*)message viewController :(UIViewController *)viewControllerRefrence ;


/**
 Email Validation method.

 @param emailToValidate email string to validate.
 @return Yes if validation is passed, else NO.
 */
+(BOOL)emailValidationCheck:(NSString *)emailToValidate;


/**
 Store PayPal link for logged user.

 @param paypalLink paypal.
 */
+(void)storePaypalLinkWithPaypalLink : (NSString *)paypalLink;

/**
 Get paypal link for user.

 @return paypal link.
 */
+(NSString *)getPayPalLink;


/**
 Show Background message for No Internet.

 @param show Bool Yes to show and NO to hide.
 @param view UIView refrence for frame size.
 @return noInternet customized view.
 */
+(UIView *)showMessageForNoInternet :(BOOL)show forView : (UIView *)view ;


/**
 Resize image downloaded from Google/Facebook login.

 @param image image object for resize.
 @return resized image.
 */
+ (UIImage *)resizeImage:(UIImage *)image ;



/**
 This method returns size of item block for RFQuilt Layout collection.

 @param product ProductDetails refrence object.
 @param view UIView refrence from viewController.
 @return CGSize width and Height.
 */
+(CGSize)blockSizeOfProduct :(ProductDetails *)product view :(UIView *)view forHome :(BOOL)isHome;

#pragma mark -
#pragma mark - Show Unfollow Alert

/**
 This method show alert for asking permission to unfollow the user.
 
 @param profieImage     profileImage object of UIImage.
 @param profileName     profileName Of user in string Format.
 @param vcReference     reference of viewController.
 @param completionBlock Bolck will call method only in case of unfollowaction.

 */
+ (void)showUnFollowAlert:(UIImage *)profieImage
                      and:(NSString *)profileName 
  viewControllerReference:(UIViewController *)vcReference
             onComplition:(void (^)(BOOL isUnfollow))completionBlock;



/**
 Present Activity Controller with existing apps (Twitter, Instagram etc) for sharing.

 @param controller UIActivityController object.
 @param refrenceVC viewController refrence object.
 */
+ (void)presentActivityController:(UIActivityViewController *)controller forViewController:(UIViewController *)refrenceVC ;


/**
 This method will check limit for Price textfield after decimal.

 @param textField price Textfield
 @param string newString
 @param range range
 @return Bool Yes if valid char else NO.
 */
+(BOOL )textFieldLimitAfterDot :(UITextField *)textField  newString:(NSString *)string forRange :(NSRange )range;



+(void)disableNotificationForUser :(NSString *)currentUserId andProductId :(NSString *)productId;

+(void)EnableNotificationForUser;


#pragma mark - Current Language

+(NSString *)currentLanguage;


@end
