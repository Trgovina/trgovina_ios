//
//  UIButton+FollowButtonCustomization.h

//
//  Created by Rahul_Sharma on 18/05/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIButton (FollowButtonCustomization)
-(void)makeButtonAsFollow;
-(void)makeButtonAsFollowing;
-(void)makeButtonAsRequested;

-(void)makeimageFollowingButton;
-(void)makeimageFollowButton;
-(void)makeimageRequestedButton;

#define  textForRequestedButton @" Requested"

#define  imageNameForFollowButton @"contacts_plus_icon"
#define  imageNameForFollowingButton @"contact_correct_icon"
#define  imageNameForRequestedButton @"edit_profile_two_timing_icon"


#define followButtonTextColor mBaseColor
#define followButtonBackGroundColor [UIColor whiteColor]

#define followingButtonTextColor [UIColor whiteColor]
#define followingButtonBackGroundColor mBaseColor

#define requstedButtonBackGroundColor [UIColor colorWithRed:0.7804 green:0.7804 blue:0.7804 alpha:1.0]

#define requstedButtonTextColor [UIColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:1.0]




#define imageForFollowButton @"activity_you_following_icon_unselector"
#define imageForFollowingButton @"activity_you_following_icon_selector"
#define imageForRequestedButton @"activity_you_requested_btn"

@end
