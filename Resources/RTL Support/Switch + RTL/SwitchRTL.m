//
//  Switch+RTL.m
//  Trgovina
//
//  Created by Rahul Sharma on 03/01/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

#import "SwitchRTL.h"

@implementation SwitchRTL

- (id)init
{
    self = [super init];
    if (self) {
        
    }
    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    if([[RTL sharedInstance] isRTL]) {
        self.semanticContentAttribute = UISemanticContentAttributeForceRightToLeft ;
    }
    else
    {
        self.semanticContentAttribute = UISemanticContentAttributeForceLeftToRight ;
    }
}

@end
