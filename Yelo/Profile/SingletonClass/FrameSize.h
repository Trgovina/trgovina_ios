//
//  FrameSize.h
//  Trgovina
//
//  Created by 3Embed on 09/07/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FrameSize : NSObject

@property (nonatomic) CGRect frame ;

-(void)initWithData:(CGRect) frameSize;
+ (id)sharedInstance;
@end
