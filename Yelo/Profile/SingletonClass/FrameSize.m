//
//  FrameSize.m
//  Trgovina
//
//  Created by 3Embed on 09/07/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

#import "FrameSize.h"

@implementation FrameSize

static FrameSize *instance = nil;

+(id)sharedInstance
{
    if(instance == nil)
    {
        instance= [FrameSize new];
        
    }
    return instance;
}


-(void)initWithData :(CGRect) frameSize
{
    self.frame = frameSize ;
}


@end
