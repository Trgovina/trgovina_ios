//
//  ProfileViewController.m
//  Tac Traderz
//
//  Created by 3Embed on 16/05/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

#import "ProfileViewController.h"
#import "UserDetailsTableViewCell.h"
#import "UserPostsTableViewCell.h"
#import "ProfileHeadersTableViewCell.h"
#import "ZoomInteractiveTransition.h"
#import "PGDiscoverPeopleViewController.h"
#import "EditProfileViewController.h"
#import "OptionsViewController.h"
#import "CameraViewController.h"
#import "ReportPostViewController.h"
#import "FrameSize.h"


#define mEditProfile                @"EDIT PROFILE"
#define mlikeStoryBoardId           @"likeStoryBoardId"
#define mDiscoverPeopleImage        @"profile_add_user_icon_off"
#define mSettingButtonImage         @"profile_setting_button"
#define mEditProfileImage           @"editprofile"
#define mResponseKeyForProfileDetails   @"profileData"

@interface ProfileViewController ()<UITableViewDataSource, UITableViewDelegate,ZoomTransitionProtocol,WebServiceHandlerDelegate>
{
    UIView *header;
    CGFloat tableHeaderHeight;
    CGRect headerRect;
    UIView *navBackView,*dividerView;
    CGFloat navHeight;
    BOOL sellingFlag,getDetails;
    
}
@property (nonatomic, strong) ZoomInteractiveTransition * transition;
@end

@implementation ProfileViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.transition = [[ZoomInteractiveTransition alloc] initWithNavigationController:self.navigationController];
    self.profile = [[Profile alloc]init];
    FrameSize *frame = [FrameSize sharedInstance];
    
    [frame initWithData:self.view.frame];
    self.tableViewOutlet.rowHeight =  UITableViewAutomaticDimension;
    self.profilePic.layer.borderColor = [UIColor whiteColor].CGColor;
    sellingFlag = YES;
    getDetails = YES;
    tableHeaderHeight = 150;
    header = self.tableViewOutlet.tableHeaderView;
    self.tableViewOutlet.tableHeaderView  = nil;
    self.tableViewOutlet.contentInset = UIEdgeInsetsMake(tableHeaderHeight , 0, 0, 0);
    self.tableViewOutlet.contentOffset = CGPointMake(0,-tableHeaderHeight );
    
    [self headerForTableView];

     navHeight = [UIApplication sharedApplication].statusBarFrame.size.height +  self.navigationController.navigationBar.frame.size.height ;
    self.navigationController.view.backgroundColor = [UIColor clearColor];
    navBackView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, navHeight)];
    navBackView.backgroundColor=[UIColor clearColor];
    dividerView = [[UIView alloc]initWithFrame:CGRectMake(0,navHeight- 0.5, self.view.frame.size.width,0.5)];
    dividerView.backgroundColor = [UIColor clearColor] ;
    [navBackView addSubview:dividerView];
    [self.view addSubview:navBackView];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    
    [self addingRefreshControl];
    if(_isMemberProfile)
    {
        self.editProfileButtonOutlet.hidden = YES ;
    }
    
//    if([[RTL sharedInstance] isRTL]){
//        self.slidingScrollView.transform = CGAffineTransformMakeRotation(M_PI);
//        self.productSectionContentView.transform = CGAffineTransformMakeRotation(M_PI);
//    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
//    [self PaypalLinkIsSaved:[[NSUserDefaults standardUserDefaults]objectForKey:payPalLink]];
//    [self gettingDetailsOfUser];
//    [self createNavSettingButton];
    [self.navigationController.navigationBar setBackgroundColor:[UIColor clearColor]];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    self.navigationController.navigationBar.translucent = YES;

}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
    headerRect = CGRectMake(0, -tableHeaderHeight, self.tableViewOutlet.bounds.size.width,tableHeaderHeight);
    
    header.frame = headerRect;
}


#pragma mark - TableView Delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}



-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section == 0 && indexPath.row == 0){
    UserDetailsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"UserDetailsTableViewCell"];
        cell.profileVC = self;
        cell.profileObj = self.profile;
        if(getDetails){ // Get details first time
        getDetails = NO;
        [cell getProfileDetails];
        }
        [cell setProfileDetails:self.profile.profileDetails];
    return cell;
    }
    else{
      UserPostsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"UserPostsTableViewCell"];
        cell.profileObj = self.profile;
        cell.profileVC = self;
        
        if(self.isMemberProfile && ![[Helper userName]isEqualToString:self.memberName])
        {
            cell.soldViewLeadingConstraint.constant = -self.view.frame.size.width;
        }
        else
        {
            cell.soldViewLeadingConstraint.constant = 0;
        }
        [cell updateConstraints];
        [cell updateConstraintsIfNeeded];
        return cell;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section == 0)
    {
        return UITableViewAutomaticDimension;
    }
    return UITableViewAutomaticDimension;
}



- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if(section == 1)
    {
      return 50;
    }
    return 0;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    if(section == 1)
    {
    static NSString *CellIdentifier = @"ProfileHeadersCell";
    ProfileHeadersTableViewCell *headerView = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    headerView.profileVC = self;
    headerView.profileObj = self.profile ;
     if(self.isMemberProfile && ![[Helper userName]isEqualToString:self.memberName])
     {
         headerView.memberProfileHeader.hidden = NO;
         headerView.userProfileHeader.hidden = YES;
     }
    else
    {
        headerView.memberProfileHeader.hidden = YES;
        headerView.userProfileHeader.hidden = NO;
        
    }
        
    [headerView selectedButtonIndex:self.profile.selectedButtonIndex];
    self.profile.sectionHeaderCell = headerView;
    if(sellingFlag){
    sellingFlag = NO;
    [headerView sellingButtonAction:headerView.sellingButton];
    }
    return headerView;
    }
    return nil;
}






#pragma mark -
#pragma mark - Table HeaderView -


/**
 Custom Header for tableview.
 This method will add scrollView, ImageViews, Page control and set as a header
 for tableview.
 */
-(void) headerForTableView
{
    [self.tableViewOutlet addSubview:header];
}




#pragma mark -
#pragma mark - ScrollView Delegate -

- (void)scrollViewDidScroll:(UIScrollView *)sender {
    
    if(sender == self.tableViewOutlet){
        
    if(self.tableViewOutlet.contentOffset.y < -tableHeaderHeight)
       {
            headerRect.origin.y = self.tableViewOutlet.contentOffset.y;
            headerRect.size.height = -self.tableViewOutlet.contentOffset.y;
            header.frame = headerRect;
    
       }
    else{
    [self offsetDidUpdate:CGPointMake(0,self.tableViewOutlet.contentOffset.y + 150)];
       
        CGRect frame ;
        frame = CGRectMake(0, tableHeaderHeight -100, self.view.frame.size.width,0);
    if(!CGRectIntersectsRect(self.tableViewOutlet.bounds ,frame ))
    {
         self.tableViewOutlet.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
        [self.tableViewOutlet layoutIfNeeded];
    }
    else
    {
       self.tableViewOutlet.contentInset = UIEdgeInsetsMake(tableHeaderHeight , 0, 0, 0);
        [self.tableViewOutlet layoutIfNeeded];
    }
  }
  }
}


- (void)offsetDidUpdate:(CGPoint)newOffset
{
    if (newOffset.y < 0.2 && newOffset.y!=0)
    {
        
      //  CGAffineTransform translate = CGAffineTransformMakeTranslation(0, newOffset.y/2);
        
     //   CGFloat scaleFactor = (150 - (newOffset.y/3)) /150;
        
     //   CGAffineTransform translateAndZoom = CGAffineTransformScale(translate, scaleFactor, scaleFactor);
        
        float alpha = -newOffset.y/100.0;
        if(alpha < 0.5)
        {
        if([self.profile.profileDetails.profilePicUrl isEqualToString:@"defaultUrl"])
            {
              self.headerProfileImage.alpha = 1;
            }
            else
            {
              self.headerProfileImage.alpha = 0.5;
            }
            
        }
        else
        {
         if([self.profile.profileDetails.profilePicUrl isEqualToString:@"defaultUrl"])
         {
            self.headerProfileImage.alpha = 1;
         }
            else
            {
             self.headerProfileImage.alpha = alpha;
            }
            
        }
        self.profilePic.alpha = 1 - alpha;
        
       // self.headerProfileImage.transform = translateAndZoom;
    }
     else
        {
         if([self.profile.profileDetails.profilePicUrl isEqualToString:@"defaultUrl"]) {
                 self.headerProfileImage.alpha = 1;
         }
         else {
                self.headerProfileImage.alpha = 0.5;
            }
            
       
            CGFloat movedOffset = -self.tableViewOutlet.contentOffset.y - 150;
            CGFloat ratio;
            ratio= -((movedOffset)/150) ;
            navBackView.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:ratio];
            if (ratio>1) {
              
                dispatch_async(dispatch_get_main_queue(),^{
                   self.tableViewTopConstraint.constant = navHeight;
                    [self.view layoutIfNeeded];
                });
                
                
                navBackView.backgroundColor = [UIColor whiteColor];
                dividerView.backgroundColor = [UIColor colorWithRed:225/255.0f green:225/255.0f blue:225/255.0f alpha:1.0f];
                self.navigationItem.title = self.profile.profileDetails.username;

                if(!self.isMemberProfile)
                {
                    
                    [self buttonName:self.discoverPeopleButtonOutlet imageNameForONStatus:mAddPeopleIconON];
                    
                    [self buttonName:self.settingsButtonOutlet imageNameForONStatus:mSettingsON];
                    
                    [self buttonName:self.editProfileButtonOutlet imageNameForONStatus:mEditProfileON];
                }
                else
                {
                    [self buttonName:self.discoverPeopleButtonOutlet imageNameForONStatus:mNavigationBackButtonImageName];
                    [self buttonName:self.settingsButtonOutlet imageNameForONStatus:@"more_button"];
                }
    
            }else{
                dispatch_async(dispatch_get_main_queue(),^{
                   self.tableViewTopConstraint.constant = 0;
                    [self.view layoutIfNeeded];
                   
                });
                
                
                navBackView.backgroundColor = dividerView.backgroundColor = [UIColor clearColor];
                self.navigationItem.title =@"";
                
                if(!self.isMemberProfile)
                {
                    [self buttonName:self.settingsButtonOutlet imageNameForOFFStatus:mSettingsOFF];
                    [self buttonName:self.discoverPeopleButtonOutlet imageNameForOFFStatus:mAddPeopleIconOFF];
                    [self buttonName:self.editProfileButtonOutlet imageNameForOFFStatus:mEditProfileOFF];
                    
                }
                else
                {
                    [self buttonName:self.discoverPeopleButtonOutlet imageNameForOFFStatus:@"item backButton"];
                    [self buttonName:self.settingsButtonOutlet imageNameForOFFStatus:@"item_Report"];
    
                }
            }
    
    
    
        }
}

-(void)buttonName:(UIButton *)buttonName imageNameForONStatus :(NSString *)imageName
{
    [[buttonName layer] setShadowOpacity:0.0];
    [[buttonName layer] setShadowRadius:0.0];
    [[buttonName layer] setShadowColor:[UIColor clearColor].CGColor];
    [buttonName setImage:[UIImage imageNamed:imageName] forState:UIControlStateNormal];
    
}

-(void)buttonName:(UIButton *)buttonName imageNameForOFFStatus :(NSString *)imageName
{
    [[buttonName layer] setShadowColor:[UIColor blackColor].CGColor];
    buttonName.layer.shadowOffset = CGSizeMake(0, -2);
    buttonName.layer.shadowRadius = 5;
    buttonName.layer.shadowOpacity = 0.5;
    [buttonName setImage:[UIImage imageNamed:imageName] forState:UIControlStateNormal];
}
//-(void)buttonName:(UIBarButtonItem *)buttonItem imageNameForONStatus :(NSString *)imageName
//{
//
// [buttonItem setImage:[UIImage imageNamed:imageName]];
//
//}
//
//-(void)buttonName:(UIBarButtonItem *)buttonItem imageNameForOFFStatus :(NSString *)imageName
//{
//
//  [buttonItem setImage:[UIImage imageNamed:imageName]];
//}



#pragma mark -
#pragma mark - ZoomTransitionProtocol -

/**
 Animation method.This method open product details screen by taking snapshot for product image.
 and show animation transition.
 
 @param isSource Boll value Yes if transition from source to destination.
 
 @return return image for transition.
 */
-(UIView *)viewForZoomTransition:(BOOL)isSource
{
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:1];
    UserPostsTableViewCell *sectionCell = [self.tableViewOutlet cellForRowAtIndexPath:indexPath];
    
    if(self.profile.selectedButtonIndex == 0)
    {
        NSIndexPath *selectedIndexPath = [[sectionCell.sellingCollectionView indexPathsForSelectedItems] firstObject];
        UserProfileCollectionViewCell *cell = (UserProfileCollectionViewCell *)[sectionCell.sellingCollectionView cellForItemAtIndexPath:selectedIndexPath];
        
        return cell.postedImagesOutlet;
        
    }
    else if(self.profile.selectedButtonIndex == 3)
    {
        NSIndexPath *selectedIndexPath = [[sectionCell.likesCollectionView indexPathsForSelectedItems] firstObject];
        UserProfileCollectionViewCell *cell = (UserProfileCollectionViewCell *)[sectionCell.likesCollectionView cellForItemAtIndexPath:selectedIndexPath];
        return cell.postedImagesOutlet;
        
    }
    else //(self.profileObj.selectedButtonIndex == 2)
    {
        NSIndexPath *selectedIndexPath = [[sectionCell.soldCollectionView indexPathsForSelectedItems] firstObject];
        UserProfileCollectionViewCell *cell = (UserProfileCollectionViewCell *)[sectionCell.soldCollectionView cellForItemAtIndexPath:selectedIndexPath];
        return cell.postedImagesOutlet;
        
    }
}


- (IBAction)settingsButtonAction:(id)sender {

    if(self.isMemberProfile)
    {
        if(![[Helper userToken] isEqualToString:mGuestToken])
        {
            ReportPostViewController *reportVc = [self.storyboard instantiateViewControllerWithIdentifier:@"reprtPostVc"];
            reportVc.fullName = self.profile.profileDetails.fullName;
            reportVc.user = YES;
            reportVc.userName = self.profile.profileDetails.username ;
            reportVc.profilePicUrl = self.profile.profileDetails.profilePicUrl;
            UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:reportVc];
            [self presentViewController:nav animated:YES completion:nil];
        }
        else
        {
            if([[Helper userToken] isEqualToString:mGuestToken])
            {
                [self presentLoginForGuestUsers];
            }
        }
        
    }
    else
    {
    OptionsViewController *newView = [self.storyboard instantiateViewControllerWithIdentifier:@"SettingsStoryboard"];
    newView.token = flStrForObj([Helper userToken]);
    newView.delegate=self;
    [self.navigationController pushViewController:newView animated:YES];
    }
    
}

- (IBAction)editButtonAction:(id)sender {
    
    EditProfileViewController *newView = [self.storyboard instantiateViewControllerWithIdentifier:@"EditProfileStoryboard"];
    newView.necessarytocallEditProfile = YES;
    newView.pushingVcFrom = @"ProfileScreen";
    newView.profilepic = self.profilePic.image;
    
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:newView];
    [self.navigationController presentViewController:navigationController animated:YES completion:nil];


}
- (IBAction)addPeopleButtonAction:(id)sender {
    if(self.isMemberProfile)
    {
        [self.navigationController popViewControllerAnimated:YES];
    }
    else{
    PGDiscoverPeopleViewController *newView = [self.storyboard instantiateViewControllerWithIdentifier:@"discoverPeopleStoryBoardId"];
    [self.navigationController pushViewController:newView animated:YES];
    }
}


#pragma mark -
#pragma mark - GIDSignInDelegate -

- (void)signIn:(GIDSignIn *)signIn didSignInForUser:(GIDGoogleUser *)user withError:(NSError *)error {
    if (error == nil) {
        
        NSString  *googlePlusUserAccessToken = user.authentication.accessToken;
        
        NSDictionary *requestDict = @{mauthToken : flStrForObj([Helper userToken]),
                                      mGooglePlusId :user.userID,
                                      mFacebookAccessToken : googlePlusUserAccessToken
                                      };
        [WebServiceHandler verifyWithGooglePlus:requestDict andDelegate:self];
        
        
    } else {
        
    }
}

#pragma mark - Webservice Delegate -

- (void) didFinishLoadingRequest:(RequestType)requestType withResponse:(id)response error:(NSError*)error {
    [[ProgressIndicator sharedInstance] hideProgressIndicator];
    if (error) {
        [Helper showAlertWithTitle:NSLocalizedString(alertError, alertError) Message:NSLocalizedString(mCommonServerErrorMessage, mCommonServerErrorMessage) viewController:self];
    }
    
    switch (requestType) {
        case RequestToVerifyWithGooglePlus:
        {
            switch ([response[@"code"] integerValue]) {
                case 200: {
                    
                    NSIndexPath *index = [NSIndexPath indexPathForRow:0 inSection:0];
                    UserDetailsTableViewCell *cell = [self.tableViewOutlet cellForRowAtIndexPath:index];
                    cell.googleVerifyImage.image = [UIImage imageNamed:@"g+ verified"];
                    cell.verifyGoogleButtonOutlet.enabled = NO;
                    self.profile.profileDetails.googleVerified = @"1";
                }
                    break;
                case 409 :{
                    
                    [Helper showAlertWithTitle:nil Message:NSLocalizedString(googleAccountAlreadyLinked, googleAccountAlreadyLinked) viewController:self];
                    
                }
                    break;
                default:
                    break;
            }
        }
            break;
        default:
            break;
    }
    // Case user Selling Post/Sold Posts details request.
    
    
}


- (IBAction)startSellingButtonAction:(id)sender {
    CameraViewController *cameraVC=[self.storyboard instantiateViewControllerWithIdentifier:@"CameraStoryBoardID"];
    cameraVC.sellProduct = TRUE;
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:cameraVC];
    
    [self presentViewController:nav animated:NO completion:nil];
    
}
- (IBAction)startDiscoverButtonAction:(id)sender {
   
  [self.tabBarController setSelectedIndex:0];
    
}


#pragma mark -
#pragma mark - Present Login -
/**
 This method will invoked for guest users and present login/signup screen.
 */
-(void)presentLoginForGuestUsers
{
    UINavigationController *navigationController = [CommonMethods presentLoginScreenController];
    [self presentViewController:navigationController animated:YES completion:nil];
}

#pragma mark -
#pragma mark - Refresh -

-(void)addingRefreshControl {
    self.refreshControl = [[UIRefreshControl alloc]init];
    [self.tableViewOutlet addSubview:self.refreshControl];
   [self.refreshControl addTarget:self action:@selector(refreshData:) forControlEvents:UIControlEventValueChanged];
}

-(void)refreshData:(id)sender {
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
    UserDetailsTableViewCell *cell = [self.tableViewOutlet cellForRowAtIndexPath:indexPath];
    [cell getProfileDetails];

    ProfileHeadersTableViewCell *sectionCell = (ProfileHeadersTableViewCell *) self.profile.sectionHeaderCell;
    
    if(sectionCell.sellingButton.selected)
    {  sectionCell.sellingButton.selected = NO;
        [self.profile.sellingPostData removeAllObjects];
        [sectionCell sellingButtonAction:sectionCell.sellingButton];
    }
    else if (sectionCell.exchangesButton.selected)
    {   sectionCell.exchangesButton.selected = NO;
        [self.profile.exchangePostData removeAllObjects];
        [sectionCell exchangesButtonAction:sectionCell.exchangesButton];
    }
    else if (sectionCell.soldButton.selected)
    {   sectionCell.soldButton.selected = NO;
        [self.profile.soldPostData removeAllObjects];
        [sectionCell soldButtonAction:sectionCell.soldButton];
    }
    else if(sectionCell.likesButton.selected){
        sectionCell.likesButton.selected = NO;
        [self.profile.exchangePostData removeAllObjects];
        [sectionCell likesButtonAction:sectionCell.likesButton];
    }
}




@end
