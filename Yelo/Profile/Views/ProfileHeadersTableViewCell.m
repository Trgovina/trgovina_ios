//
//  ProfileHeadersTableViewCell.m
//  Tac Traderz
//
//  Created by 3Embed on 16/05/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

#import "ProfileHeadersTableViewCell.h"
#import "UserPostsTableViewCell.h"
#import "SectionCollectionViewCell.h"
#import "Exchanged.h"

@implementation ProfileHeadersTableViewCell 

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)selectedButtonIndex:(NSUInteger)index
{
    switch (index) {
        case 0:
        {
            self.sellingButton.selected = self.memberSellingButton.selected = YES;
            
            self.exchangesButton.selected = self.soldButton.selected = self.likesButton.selected = self.memberSoldButton.selected = NO ;
            
            self.sellingUnderView.hidden = NO; self.memberSellingUnderView.hidden = NO ;
            self.exchangesUnderView.hidden = self.soldUnderView.hidden = self.likesUnderView.hidden = self.memberSoldUnderView.hidden = YES;
        }
            break;
        case 1:
        {
            self.exchangesButton.selected = YES;
            self.sellingButton.selected = self.soldButton.selected = self.likesButton.selected = NO ;
            
            self.exchangesUnderView.hidden = NO ;
            self.sellingUnderView.hidden = self.soldUnderView.hidden = self.likesUnderView.hidden = YES;
        }
            break;
        case 2:
        {
            self.soldButton.selected = self.memberSoldButton.selected = YES;
            self.exchangesButton.selected = self.sellingButton.selected = self.likesButton.selected = self.memberSellingButton.selected =NO ;
            
            self.soldUnderView.hidden = NO; self.memberSoldUnderView.hidden =NO ;
            self.exchangesUnderView.hidden = self.sellingUnderView.hidden = self.likesUnderView.hidden =self.memberSellingUnderView.hidden = YES;
        }
            break;
        case 3:
        {
            self.likesButton.selected = YES;
            self.exchangesButton.selected = self.soldButton.selected = self.sellingButton.selected = NO ;
            
            self.likesUnderView.hidden = NO ;
            self.exchangesUnderView.hidden = self.soldUnderView.hidden = self.sellingUnderView.hidden = YES;
        }
            break;
            
        default:
            break;
    }
}



- (IBAction)sellingButtonAction:(id)sender {
    
    if(!self.sellingButton.selected){
    self.sellingButton.selected = self.memberSellingButton.selected = YES;
    self.exchangesButton.selected = self.soldButton.selected = self.likesButton.selected = self.memberSoldButton.selected = NO ;
    
        self.sellingUnderView.hidden = NO; self.memberSellingUnderView.hidden = NO ;
    self.exchangesUnderView.hidden = self.soldUnderView.hidden = self.likesUnderView.hidden = self.memberSoldUnderView.hidden = YES;
    self.profileObj.selectedButtonIndex = 0;
    sellingCurrentIndex = 0;
        
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:1];
        UserPostsTableViewCell *cell = [self.profileVC.tableViewOutlet cellForRowAtIndexPath:indexPath];
        
    CGRect frame = cell.slidingScrollView.bounds;
                frame.origin.x = 0;
                [cell.slidingScrollView scrollRectToVisible:frame animated:YES];
        
                if(self.profileObj.sellingPostData.count>0)
                {
                cell.sellingCollectionView.backgroundView = nil;
                }
                else
                {
                    ProgressIndicator *HomePI = [ProgressIndicator sharedInstance];
                    [HomePI showPIOnView:self.profileVC.view withMessage:NSLocalizedString(LoadingIndicatorTitle, LoadingIndicatorTitle)];
                 [self requestForPostsWithIndex:sellingCurrentIndex withSoldType:@"0"];
                }
        [cell.sellingCollectionView reloadData];
        dispatch_async(dispatch_get_main_queue(), ^{
            [self calculateCollectionViewHeightForTag:0];
            [self reloadTableView];
            
        });
 }
}


- (IBAction)exchangesButtonAction:(id)sender {
    
    if(!self.exchangesButton.selected){
    self.exchangesButton.selected = YES;
    self.sellingButton.selected = self.soldButton.selected = self.likesButton.selected = NO ;

    self.exchangesUnderView.hidden = NO ;
    self.sellingUnderView.hidden = self.soldUnderView.hidden = self.likesUnderView.hidden = YES;
    self.profileObj.selectedButtonIndex = 1;
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:1];
    UserPostsTableViewCell *cell = [self.profileVC.tableViewOutlet cellForRowAtIndexPath:indexPath];
    
    CGRect frame = cell.slidingScrollView.bounds;
    frame.origin.x = CGRectGetWidth(self.profileVC.view.frame);
    [cell.slidingScrollView scrollRectToVisible:frame animated:YES];
    
    if(self.profileObj.exchangePostData.count>0)
    {
        cell.exchangeTableView.backgroundView = nil;
    }
    else
    {
        ProgressIndicator *HomePI = [ProgressIndicator sharedInstance];
        [HomePI showPIOnView:self.profileVC.view withMessage:NSLocalizedString(LoadingIndicatorTitle, LoadingIndicatorTitle)];
        
        NSDictionary *requestDict = @{
                                      mauthToken :[Helper userToken],
                                      moffset : @"0",
                                      mlimit : @"40",
                                      };
        
        [WebServiceHandler getExchangedPosts:requestDict andDelegate:self];

       [self showNoExchangdPostsFor:cell.exchangeTableView];
    }
    [cell.exchangeTableView reloadData];
    dispatch_async(dispatch_get_main_queue(), ^{
            [self calculateCollectionViewHeightForTag:3];
            [self reloadTableView];
        });
 }

}

- (IBAction)soldButtonAction:(id)sender {
    
    if(!self.soldButton.selected){
    if([[Helper userToken] isEqualToString:mGuestToken])
    {
        [self.profileVC presentLoginForGuestUsers];
    }
    else{
    self.soldButton.selected = self.memberSoldButton.selected = YES;
    self.exchangesButton.selected = self.sellingButton.selected = self.likesButton.selected =self.memberSellingButton.selected = NO ;
    
        self.soldUnderView.hidden = NO; self.memberSoldUnderView.hidden = NO ;
    self.exchangesUnderView.hidden = self.sellingUnderView.hidden = self.likesUnderView.hidden = self.memberSellingUnderView.hidden = YES;
   self.profileObj.selectedButtonIndex = 2;
        
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:1];
        UserPostsTableViewCell *cell = [self.profileVC.tableViewOutlet cellForRowAtIndexPath:indexPath];
        
        CGRect frame = cell.slidingScrollView.bounds;
        
        if(self.profileVC.isMemberProfile && ![[Helper userName]isEqualToString:self.profileVC.memberName]){
            frame.origin.x = CGRectGetWidth(self.profileVC.view.frame);
            [cell.slidingScrollView scrollRectToVisible:frame animated:YES];
        }
        else
        {
            frame.origin.x = CGRectGetWidth(self.profileVC.view.frame) *2;
            [cell.slidingScrollView scrollRectToVisible:frame animated:YES];
        }
        
            if(self.profileObj.soldPostData.count>0)
            {
                cell.soldCollectionView.backgroundView = nil;
                [cell.soldCollectionView reloadData];
            }
            else
            {
//                if(!refresh){
                    ProgressIndicator *HomePI = [ProgressIndicator sharedInstance];
                    [HomePI showPIOnView:self.profileVC.view withMessage:NSLocalizedString(LoadingIndicatorTitle, LoadingIndicatorTitle)];
//                }
                
            [self showingBackgroundMessageFor:cell.soldCollectionView type:1];
                soldPostServiceCall = YES;
                [self requestForPostsWithIndex:0 withSoldType:@"1"];
            }
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            [self calculateCollectionViewHeightForTag:1];
            [self reloadTableView];
        });
        
        }

}

- (IBAction)likesButtonAction:(id)sender {
    
    if(!self.likesButton.selected){
    self.likesButton.selected = YES;
    self.exchangesButton.selected = self.soldButton.selected = self.sellingButton.selected = NO ;
    
    self.likesUnderView.hidden = NO ;
    self.exchangesUnderView.hidden = self.soldUnderView.hidden = self.sellingUnderView.hidden = YES;
    self.profileObj.selectedButtonIndex = 3;
        
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:1];
        UserPostsTableViewCell *cell = [self.profileVC.tableViewOutlet cellForRowAtIndexPath:indexPath];
        
                CGRect frame = cell.slidingScrollView.bounds;
                frame.origin.x = CGRectGetWidth(self.profileVC.view.frame) *3;
                [cell.slidingScrollView scrollRectToVisible:frame animated:YES];
        
                if(self.profileObj.likesPostData.count>0)
                {
                    cell.likesCollectionView.backgroundView = nil;
                }
                else
                {
//                    if(!refresh){
                        ProgressIndicator *HomePI = [ProgressIndicator sharedInstance];
                        [HomePI showPIOnView:self.profileVC.view withMessage:NSLocalizedString(LoadingIndicatorTitle, LoadingIndicatorTitle)];
//                    }
                    
                   // self.favouritesCurrentIndex = 0;
             [self showingBackgroundMessageFor:cell.likesCollectionView type:2];
               [self requestForFavouritesWithIndex:0];
                }
                [cell.likesCollectionView reloadData];
        dispatch_async(dispatch_get_main_queue(), ^{
            [self calculateCollectionViewHeightForTag:2];
            [self reloadTableView];
        });
        
            }
    
}


-(void)setCollectionViewForSection :(NSInteger )row
{
     NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:1];
    UserPostsTableViewCell *cell = [self.profileVC.tableViewOutlet cellForRowAtIndexPath:indexPath];
    
    NSIndexPath *indexPath1 = [NSIndexPath indexPathForRow:row inSection:0];
    
    [cell.sectionCollectionView scrollToItemAtIndexPath:indexPath1 atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:YES];
}



/**
 Request Selling Posts
 
 */
-(void)requestForPostsWithIndex:(NSInteger )offestIndex withSoldType :(NSString *)sold{
    
    if (self.profileVC.isMemberProfile && self.profileVC.memberName) {
        NSDictionary *requestDict = @{
                                      mauthToken :[Helper userToken],
                                      mmemberName :self.profileVC.memberName,
                                      mlimit:mLimitValue,
                                      moffset: [NSNumber numberWithInteger:20 *offestIndex],
                                      @"sold" : sold
                                      };
        if([[Helper userToken]isEqualToString:mGuestToken])
        {
            [WebServiceHandler getMemberPostsForGuest:requestDict andDelegate:self];
        }
        else
        {
            [WebServiceHandler getMemberPosts:requestDict andDelegate:self];
        }
    }
    else {
    
        NSDictionary *requestDict = @{
                                      mauthToken :[Helper userToken],
                                      moffset : [NSNumber numberWithInteger:20 *offestIndex],
                                      mlimit : mLimitValue,
                                      @"sold" : sold
                                      };
        
        [WebServiceHandler getUserPosts:requestDict andDelegate:self];
    }
}


/**
 Request Favourite Posts
 
 */
-(void)requestForFavouritesWithIndex:(NSInteger )offsetIndex
{
    NSDictionary *requestDict = @{
                                  mauthToken :flStrForObj([Helper userToken]),
                                  mmemberName :[Helper userName],
                                  mlimit:mLimitValue,
                                  moffset:[NSNumber numberWithInteger:20 *offsetIndex]
                                  };
    [WebServiceHandler RequestTypePostsLikedByUser:requestDict andDelegate:self];
}


#pragma mark - WebService Handler -

-(void)internetIsNotAvailable:(RequestType)requsetType {
    [[ProgressIndicator sharedInstance] hideProgressIndicator];
    
    [self.profileVC.refreshControl endRefreshing];
}



- (void) didFinishLoadingRequest:(RequestType)requestType withResponse:(id)response error:(NSError*)error {
    [[ProgressIndicator sharedInstance] hideProgressIndicator];
    [self.profileVC.refreshControl endRefreshing];
    
    if (error) {
        [Helper showAlertWithTitle:NSLocalizedString(alertError, alertError) Message:NSLocalizedString(mCommonServerErrorMessage, mCommonServerErrorMessage) viewController:self.profileVC];
    }
    
    NSDictionary *responseDict = (NSDictionary*)response;
    
    switch (requestType) {
            // Case user Selling Post/Sold Posts details request.
        case RequestTypeGetPosts:
        {
            switch ([responseDict[@"code"] integerValue]) {
                case 200: {
                    if(soldPostServiceCall)
                    {
                        soldPostServiceCall = NO;
                        [self handlingResponseForSoldPosts:response];
                    }
                    else
                    {
                        [self handlingResponseForRequestPosts:response];
                        
                    }
                }
                    break;
                case 204:{
//                    [self stopAnimation];
                    if(soldPostServiceCall)
                    {
                        soldPostServiceCall = NO;
                        [self handlingResponseForSoldPosts:response];
                    }
                    else
                    {
                        [self handlingResponseForRequestPosts:response];
                    }
                    
                }
                    break;
                default :
                    break;
            }
        }
            break;
            // Case to handle Favorites of User.
        case RequestTypePostsLikedByUser:
        {
            switch ([responseDict[@"code"] integerValue]) {
                case 200: {
                    [self handlingResponseForFavourites:response];
                }
                    break;
                    
                case 204:{
                    [self handlingResponseForFavourites:response];
                }
                    break;
                default:
                    break;
            }
        }
            break;
        case RequestTypeUserExchangedPosts:
        {
            switch ([responseDict[@"code"] integerValue]) {
                case 200: {
                    [self handlingResponseForExchangedPosts:response];
                }
                    break;
                    
                case 204:{
                    [self handlingResponseForExchangedPosts:response];
                }
                    break;
                default:
                    break;
            }
        }
            break;

       default:
            break;
    }
 }
    
    /*-----------------------------------------------*/
#pragma mark -
#pragma mark - Handling Response
    /*-----------------------------------------------*/
    
-(void)handlingResponseForRequestPosts :(NSDictionary *)response
    {
    if(sellingCurrentIndex == 0){
        [self.profileObj.sellingPostData removeAllObjects];
        self.profileObj.sellingPostData = [ProductDetails arrayOfProducts:response[@"data"]];
        }
//        else
//        {
//            NSArray *productsWithPaging = [ProductDetails arrayOfProducts:response[@"data"]];
//            [self.profileObj.sellingPostData addObjectsFromArray:productsWithPaging];
//        }
        
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:1];
        UserPostsTableViewCell *cell = [self.profileVC.tableViewOutlet cellForRowAtIndexPath:indexPath];
        if(self.profileObj.sellingPostData.count > 0)
        {
            
            
            cell.sellingCollectionView.backgroundView = nil;
            [cell.sellingCollectionView reloadData];
            
            dispatch_async(dispatch_get_main_queue(), ^{
               [self calculateCollectionViewHeightForTag:0];
               [self reloadTableView];
            });
          //  self.sellingCurrentIndex++;
          //  self.sellingPaging ++ ;
        }
        else{
           [cell.sellingCollectionView reloadData];
          [self showingBackgroundMessageFor:cell.sellingCollectionView type:0];
        }

 }


-(void)handlingResponseForSoldPosts :(NSDictionary *)response
{
//    if(self.soldCurrentIndex == 0){
     [self.profileObj.soldPostData removeAllObjects];
        self.profileObj.soldPostData = [ProductDetails arrayOfProducts:response[@"data"]];
//    }
//    else
//    {
//        NSArray *productsWithPaging = [ProductDetails arrayOfProducts:response[@"data"]];
//        [soldPostData addObjectsFromArray:productsWithPaging];
//    }
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:1];
    UserPostsTableViewCell *cell = [self.profileVC.tableViewOutlet cellForRowAtIndexPath:indexPath];
    
    if(self.profileObj.soldPostData.count > 0)
    {
        cell.soldCollectionView.backgroundView = nil;
        [cell.soldCollectionView reloadData];
        dispatch_async(dispatch_get_main_queue(), ^{
             [self calculateCollectionViewHeightForTag:1];
             [self reloadTableView];
        });
    }
    else {
        [cell.soldCollectionView reloadData];
        [self showingBackgroundMessageFor:cell.soldCollectionView type:1];
    }
    
}


-(void)handlingResponseForFavourites :(NSDictionary *)response
{
    //if(self.favouritesCurrentIndex == 0){
        [self.profileObj.likesPostData  removeAllObjects];
        self.profileObj.likesPostData = [ProductDetails arrayOfProducts:response[@"data"]];
//    }
//    else
//    {
//        NSArray *productsWithPaging = [ProductDetails arrayOfProducts:response[@"data"]];
//        [favouritePostData addObjectsFromArray:productsWithPaging];
//    }
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:1];
    UserPostsTableViewCell *cell = [self.profileVC.tableViewOutlet cellForRowAtIndexPath:indexPath];

    if(self.profileObj.likesPostData.count > 0)
    {
        cell.likesCollectionView.backgroundView = nil;
         [cell.likesCollectionView reloadData];
        dispatch_async(dispatch_get_main_queue(), ^{
         [self calculateCollectionViewHeightForTag:2];
         [self reloadTableView];
        });
        
//        self.favouritesCurrentIndex++;
//        self.favouritePaging++ ;
    }
    else{
        [cell.likesCollectionView reloadData];
        [self showingBackgroundMessageFor:cell.likesCollectionView type:2];
    }
}

-(void)handlingResponseForExchangedPosts :(NSDictionary *)response
{
    //if(self.favouritesCurrentIndex == 0){
    [self.profileObj.exchangePostData  removeAllObjects];
    self.profileObj.exchangePostData = [Exchanged arrayOfExchangedPosts:response[@"data"]];
    //    }
    //    else
    //    {
    //        NSArray *productsWithPaging = [ProductDetails arrayOfProducts:response[@"data"]];
    //        [favouritePostData addObjectsFromArray:productsWithPaging];
    //    }
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:1];
    UserPostsTableViewCell *cell = [self.profileVC.tableViewOutlet cellForRowAtIndexPath:indexPath];
    
    if(self.profileObj.exchangePostData.count > 0)
    {
        cell.exchangeTableView.backgroundView = nil;
        [cell.exchangeTableView reloadData];
        dispatch_async(dispatch_get_main_queue(), ^{
            [self calculateCollectionViewHeightForTag:3];
             [self reloadTableView];
        });
    }
    else
    {
        [cell.exchangeTableView reloadData];
        [self showNoExchangdPostsFor:cell.exchangeTableView];
    }
}


/*---------------------------------------*/
#pragma
#pragma mark - Calculate CollectionView Height
/*---------------------------------------*/

/**
 Calculate height based on content.
 */
- (void)calculateCollectionViewHeightForTag :(NSInteger )tag {
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:1];
    UserPostsTableViewCell *cell = [self.profileVC.tableViewOutlet cellForRowAtIndexPath:indexPath];
    
    if (self.profileObj.isMemberProfile && !self.profileObj.profileDetails.isOwnProfile) {
        cell.slidingScrollView.contentSize =  CGSizeMake(self.profileVC.view.frame.size.width *2,cell.slidingScrollView.contentSize.height ) ;
    }
    switch (tag) {
        case 0:
        {
            cell.heightConstraint.constant = cell.sellingCollectionView.contentSize.height + 100;
            if(cell.heightConstraint.constant < self.profileVC.view.frame.size.height)
            {
                cell.heightConstraint.constant =  self.profileVC.view.frame.size.height - 100;
            }
        }
            break;
        case 1:
        {
            cell.heightConstraint.constant = cell.soldCollectionView.contentSize.height + 100;
            if(cell.heightConstraint.constant < self.profileVC.view.frame.size.height)
            {
                cell.heightConstraint.constant =  self.profileVC.view.frame.size.height - 100;
            }
        }
            break;
            
        case 2:
        {
            cell.heightConstraint.constant = cell.likesCollectionView.contentSize.height + 100;
            if(cell.heightConstraint.constant < self.profileVC.view.frame.size.height)
            {
                cell.heightConstraint.constant =  self.profileVC.view.frame.size.height - 100;
            }
        }
            break;
        case 3:
        {
            cell.heightConstraint.constant = cell.exchangeTableView.contentSize.height + 100;
            if(cell.heightConstraint.constant < self.profileVC.view.frame.size.height)
            {
                cell.heightConstraint.constant =  self.profileVC.view.frame.size.height - 100;
            }
        }
        default:
            
            break;
    }
}

-(void)reloadTableView
{
    self.profileVC.tableViewOutlet.contentInset = UIEdgeInsetsMake(150 , 0, 0, 0);
    [self.profileVC.tableViewOutlet layoutIfNeeded];
    [self.profileVC.tableViewOutlet reloadData];
}


#pragma mark -
#pragma mark - CollectionView BackgroundView For Empty Posts -


/**
 This method is invoked to show custom background view for collectionview
 in case if product list is empty for any of section.
 
 @param type integer tag for sections.(selling/Sold/Favourites).
 */
-(void)showingBackgroundMessageFor:(UICollectionView *)collectionView type:(NSInteger)type
{
    switch (type) {
        case 0:
        {
            self.profileVC.startDiscoveringButton.hidden = YES;
            self.profileVC.noPostsEmptyImage.image = [UIImage imageNamed:@"empty_selling"];
            self.profileVC.noPostsDescriptionLabel.text = NSLocalizedString(emptySellingPosts, emptySellingPosts);
            if(self.profileVC.isMemberProfile){
            self.profileVC.startSellingButton.hidden = YES;
                self.profileVC.noPostsDescriptionLabel.hidden = @"";
            }
            else
            {
                self.profileVC.startSellingButton.hidden = NO;
                self.profileVC.noPostsLabel.text = NSLocalizedString(emptyPostsText, emptyPostsText);
            }
            UIView *backGroundViewForNoPost = [[UIView alloc]initWithFrame:CGRectMake(0, 0, collectionView.frame.size.width,collectionView.frame.size.height)];
            self.profileVC.backgroundViewForNoPosts.frame = CGRectMake(0, 30,self.profileVC.view.frame.size.width, 150);
            [backGroundViewForNoPost addSubview:self.profileVC.backgroundViewForNoPosts];
            collectionView.backgroundView = backGroundViewForNoPost;
        }
            
            break;
        case 1:
        {
            self.profileVC.noPostsEmptyImage.image = [UIImage imageNamed:@"empty_selling"];
            
           self.profileVC.noPostsLabel.text = NSLocalizedString(emptyPostsText, emptyPostsText);
            self.profileVC.startDiscoveringButton.hidden = YES ;
            self.profileVC.startSellingButton.hidden = YES ;
            
            if(self.profileVC.isMemberProfile)
            {
                self.profileVC.noPostsDescriptionLabel.text = @"";
            }
            else
            {
                self.profileVC.noPostsDescriptionLabel.text = @"Još uvijek niste napravili prodaju!";
            }
            
            UIView *backGroundViewForNoPost = [[UIView alloc]initWithFrame:CGRectMake(0, 0, collectionView.frame.size.width,collectionView.frame.size.height)];
            self.profileVC.backgroundViewForNoPosts.frame = CGRectMake(0, 30,self.profileVC.view.frame.size.width, 150);
            [backGroundViewForNoPost addSubview:self.profileVC.backgroundViewForNoPosts];
            collectionView.backgroundView = backGroundViewForNoPost;
        }
            break;
        case 2:
        {
            self.profileVC.startSellingButton.hidden = YES ;
            self.profileVC.noPostsEmptyImage.image = [UIImage imageNamed:@"empty_favourite"];
            self.profileVC.noPostsLabel.text = NSLocalizedString(emptyFavouritesText1, emptyFavouritesText1);
            self.profileVC.noPostsDescriptionLabel.text = NSLocalizedString(emptyFavouritesText2, emptyFavouritesText2);
            
            self.profileVC.startDiscoveringButton.hidden = NO ;
            
            UIView *backGroundViewForNoPost = [[UIView alloc]initWithFrame:CGRectMake(0, 0, collectionView.frame.size.width,collectionView.frame.size.height)];
            self.profileVC.backgroundViewForNoPosts.frame = CGRectMake(0, 30,self.profileVC.view.frame.size.width, 150);
            [backGroundViewForNoPost addSubview:self.profileVC.backgroundViewForNoPosts];
            collectionView.backgroundView = backGroundViewForNoPost;
        }
            break;
            
        default:
            break;
    }
}

-(void)showNoExchangdPostsFor:(UITableView *)tableView
{
        self.profileVC.noPostsEmptyImage.image = [UIImage imageNamed:@"empty_selling.png"];
        self.profileVC.noPostsLabel.text = NSLocalizedString(emptyPostsText, emptyPostsText);
        self.profileVC.startDiscoveringButton.hidden =  self.profileVC.startSellingButton.hidden = YES ;
        self.profileVC.startDiscoveringButton.hidden = YES;
    
    self.profileVC.noPostsDescriptionLabel.text = @"oš uvijek niste napravili razmjenu!";
        UIView *backGroundViewForNoPost = [[UIView alloc]initWithFrame:CGRectMake(0, 0, tableView.frame.size.width,tableView.frame.size.height)];
        self.profileVC.backgroundViewForNoPosts.frame = CGRectMake(0, 30,self.profileVC.view.frame.size.width, 150);
        [backGroundViewForNoPost addSubview:self.profileVC.backgroundViewForNoPosts];
        tableView.backgroundView = backGroundViewForNoPost;
}


@end
