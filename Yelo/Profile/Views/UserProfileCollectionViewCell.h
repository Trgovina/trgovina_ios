//
//  UserProfileCollectionViewCell.h

//
//  Created by Rahul Sharma on 3/30/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UserProfileCollectionViewCell : UICollectionViewCell

/**
 *  uiimageview outlet and it is in cell.
 */
@property (weak, nonatomic) IBOutlet UIImageView *postedImagesOutlet;


/**
 Set Products on homescreen.
 
 @param product productModel.
 */
-(void)setProducts:(ProductDetails *)product;

@property (strong, nonatomic) IBOutlet UIView *featuredView;

@property (weak, nonatomic) IBOutlet UIImageView *tickMarkImage;

@property (weak, nonatomic) IBOutlet UIView *priceNameView;
@property (weak, nonatomic) IBOutlet UILabel *price;
@property (weak, nonatomic) IBOutlet UILabel *productName;
@property (weak, nonatomic) IBOutlet UILabel *swapFor;
@property (weak, nonatomic) IBOutlet UIImageView *swapIcon;

@end
