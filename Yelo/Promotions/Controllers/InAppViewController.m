

//
//  InAppViewController.m
//  Datum
//
//  Created by rahul Sharma on 10/03/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import "InAppViewController.h"
#import "RageIAPHelper.h"
#import "PromotionsPlanTableViewCell.h"
#import "ProductDetailsViewController.h"

@interface InAppViewController ()<UITableViewDataSource , UITableViewDelegate , WebServiceHandlerDelegate >
{
    NSMutableArray  *plans;
    NSInteger selectedRow ;
    SKProduct *selectedProduct;
}

@end

@implementation InAppViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    selectedRow = -1;
    
    plans = [NSMutableArray new];
    [self.productImage sd_setImageWithURL:[NSURL URLWithString:self.product.mainUrl] placeholderImage:[UIImage imageNamed:@""]];
    NSDecimalNumber *amountNumber = [NSDecimalNumber decimalNumberWithString:self.product.price];
    NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"sr"];
    NSNumberFormatter *currencyStyle = [[NSNumberFormatter alloc] init];
    [currencyStyle setNumberStyle:NSNumberFormatterCurrencyStyle];
    [currencyStyle setLocale:locale];
    NSString *currency = [currencyStyle stringFromNumber:amountNumber];
    if ([currency length] > 0) {
        currency = [currency substringToIndex:[currency length] - 1];
        self.priceLabel.text = currency ;
    }
    self.productNameLabel.text = self.product.productName ;
 //   NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:self.product.currency];
    NSString *currencySymbol = self.product.currency;
    self.currencyLabel.text = currencySymbol ;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(productPurchased:) name:IAPHelperProductPurchasedNotification object:nil];
    
    ProgressIndicator *progress = [ProgressIndicator sharedInstance];
    [progress showPIOnView:self.view withMessage:@"Connecting.."];
    [[RageIAPHelper sharedInstance] requestProductsWithCompletionHandler:^(BOOL success, NSArray *products) {
        if (success) {
            [plans addObjectsFromArray:products];
            
            if(plans.count > 0)
            {
                self.purchaseButtonOutlet.enabled = YES ;
            }
            
            [self.plansTableView reloadData];
            [[ProgressIndicator sharedInstance] hideProgressIndicator];
        }
    }];
    
    
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.translucent = NO;
    self.navigationController.navigationBar.shadowImage = nil;
}

/**
 *  It is to check products and send service request acording to the product identifier.
 *
 *  @param notification notification for product purchase.
 */
- (void)productPurchased:(NSNotification *)notification {
    
    NSString * productIdentifier = notification.object;
    [plans enumerateObjectsUsingBlock:^(SKProduct * product, NSUInteger idx, BOOL *stop) {
        if ([product.productIdentifier isEqualToString:productIdentifier]) {
            
            ProgressIndicator *pi = [ProgressIndicator sharedInstance];
            [pi showPIOnView:self.view withMessage:@"Buying..."];
            
            NSString *numberOfViews;
            if([product.productIdentifier isEqualToString:INAPP_KEY_100_Clicks])
            {
                numberOfViews = @"100";
            }
            else if ([product.productIdentifier isEqualToString:INAPP_KEY_200_Clicks])
            {
                numberOfViews = @"200";
            }
            else if ([product.productIdentifier isEqualToString:INAPP_KEY_300_Clicks])
            {
                numberOfViews = @"300";
            }
            else if ([product.productIdentifier isEqualToString:INAPP_KEY_500_Clicks])
            {
                numberOfViews = @"500";
            }
            
            
            
            NSDictionary *requestDic = @{
                                         mpostid : self.product.postId,
                                         @"purchaseId" : product.productIdentifier,
                                         mauthToken : [Helper userToken],
                                         mposttype : @"0",
                                         @"noOfViews" : numberOfViews,
                                         @"promotionTitle":product.localizedTitle
                                         };
            
            [WebServiceHandler PurchasePromoPlans:requestDic andDelegate:self];
            return ;
        }
    }];
}



#pragma mark-
#pragma mark - Webservice Delegate

-(void)didFinishLoadingRequest:(RequestType)requestType withResponse:(id)response error:(NSError *)error
{
    [[ProgressIndicator sharedInstance] hideProgressIndicator];
    if (error) {
        [Helper showAlertWithTitle:NSLocalizedString(alertError, alertError) Message:[error localizedDescription] viewController:self];
    }
    if(requestType == RequestForPurchasePlans)
    {
        switch ([response[@"code"] integerValue]) {
            case 200:
            {
                [[NSNotificationCenter defaultCenter]postNotificationName:mUpdatePromotedPost object:nil];
                self.product.isPromoted = YES;
                [self dismissViewControllerAnimated:YES completion:nil];
            }
                break;
            default:
                [Helper showAlertWithTitle:NSLocalizedString(alertTitle, alertTitle) Message:response[@"message"] viewController:self];
                break;
        }
        
    }
    
}

#pragma mark-
#pragma mark - Tableview Datasource & Delegate-

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return plans.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    PromotionsPlanTableViewCell *plansCell = [tableView dequeueReusableCellWithIdentifier:@"promotionsPlansCell"];
    SKProduct *product = plans[indexPath.row];
    
    plansCell.numberOfLikes.text = product.localizedTitle ;
    plansCell.priceOfPlan.text = [NSString stringWithFormat:@"%@ %@",product.priceLocale.currencySymbol,[product.price stringValue]];
    if(indexPath.row == 0 && selectedRow == -1)
    {
        [plansCell updateViewForActiveState:YES];
        selectedProduct = plans[indexPath.row];
        selectedRow = 0;
    }
    else if (indexPath.row == selectedRow) {
        
        [plansCell updateViewForActiveState:YES];
    }
    else{
        [plansCell updateViewForActiveState:NO];
    }
    return plansCell ;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if(selectedRow != -1 && indexPath.row!= selectedRow)
    {
        NSIndexPath *ind =[NSIndexPath indexPathForRow:selectedRow inSection:0 ];
        PromotionsPlanTableViewCell *cell=[tableView cellForRowAtIndexPath:ind];
        [cell updateViewForActiveState:NO];
    }
    
    NSIndexPath *ind =[NSIndexPath indexPathForRow:indexPath.row inSection:0 ];
    PromotionsPlanTableViewCell *cell=[tableView cellForRowAtIndexPath:ind];
    [cell updateViewForActiveState:YES];
    selectedRow = indexPath.row ;
    selectedProduct = plans[indexPath.row];
    self.purchaseButtonOutlet.enabled = YES ;
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 70;
}


-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    
    return 40;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    
    self.sectionHeaderForTableview.frame = CGRectMake(0, 0, self.view.frame.size.width,30 );
    
    return self.sectionHeaderForTableview;
}



- (IBAction)navCancelButtonAction:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark -
#pragma mark - Purchase Button

- (IBAction)purchaseButtonAction:(id)sender {
    [RageIAPHelper sharedInstance].referenceVC = self ;
    [[RageIAPHelper sharedInstance] buyProduct:selectedProduct];
}




- (IBAction)showProductButtonAction:(id)sender {
    ProductDetailsViewController *newView = [self.storyboard instantiateViewControllerWithIdentifier:mInstaTableVcStoryBoardId];
    newView.postId = self.product.postId;
    newView.noAnimation = YES;
    [self.navigationController pushViewController:newView animated:YES];
}
@end


