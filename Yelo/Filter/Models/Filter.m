//
//  Filter.m
//  CollegeStax
//
//  Created by 3Embed on 17/05/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

#import "Filter.h"

@implementation Filter

-(instancetype)initWithDictionary:(NSDictionary *)response
{
    self = [super init];
    if (!self) { return nil; }
    
    self.fieldName = flStrForObj(response[@"fieldName"]);
    self.values = flStrForObj(response[@"values"]);
    self.isMandatory = [flStrForObj(response[@"isMandatory"])boolValue];
    self.type = [flStrForObj(response[@"type"])integerValue];
    self.filterId =  flStrForObj(response[@"id"]);
    self.LanguageCode = [Helper currentLanguage];
    return self;
}


+(NSMutableArray *) arrayOfFilters :(NSArray *)responseData
{
    NSMutableArray *filters = [[NSMutableArray alloc]init];
    for(NSDictionary *filterDic in responseData) {
        
        Filter *filter = [[Filter alloc]initWithDictionary:filterDic] ;
        [filters addObject:filter];
    }
    
    return filters ;
}

@end
