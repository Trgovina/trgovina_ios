//
//  Filter.h
//  CollegeStax
//
//  Created by 3Embed on 17/05/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Filter : NSObject

@property(nonatomic,copy)NSString *fieldName;
@property(nonatomic,copy)NSString *filterId;
@property(nonatomic,copy)NSString *LanguageCode;


@property(nonatomic,copy)NSString *values;
@property(nonatomic)BOOL isMandatory;
@property(nonatomic)NSUInteger type;

- (instancetype)initWithDictionary:(NSDictionary *)response;
+(NSMutableArray *) arrayOfFilters :(NSArray *)responseData ;

@end
