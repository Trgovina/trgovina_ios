//
//  FilterListings.h
//  CollegeStax
//
//  Created by 3Embed on 22/05/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SubCategory.h"

@interface FilterListings : NSObject
@property(nonatomic,retain)NSArray * subcategoryArray;
@property(nonatomic,copy)NSString *category;
@property(nonatomic,copy)NSString *categoryImage;
@property(nonatomic,copy)NSString *subcategoryImage;
@property(nonatomic,copy)NSString *subcategoryName;
@property(nonatomic,copy)NSString *categoryNodeId;
@property(nonatomic,copy)NSString *subcategoryNodeId;
@property(nonatomic,copy)NSString *subcategoryId;
@property(nonatomic,copy)NSString *LanguageCode;


@property(nonatomic,retain)Category *categoryModelObject;
@property(nonatomic,retain)SubCategory *subcategory;
@property(nonatomic,retain)NSArray *filtersArray;
@property(nonatomic)CGFloat advanceFilterCellHeight;
@property(nonatomic,retain)NSMutableArray *subFilterKeyValue;
@property(nonatomic,retain)NSMutableDictionary *filtersDictionary;



@property(nonatomic,copy)NSString *defaultLocation;
@property(nonatomic,copy)NSString *defaultLatitude;
@property(nonatomic,copy)NSString *defaultLongitude;

@property(nonatomic,copy)NSString *location;
@property(nonatomic,copy)NSString *latitude;
@property(nonatomic,copy)NSString *longitude;
@property(nonatomic,copy)NSString *distanceMax;

@property(nonatomic,copy)NSString *sortBy;
@property(nonatomic,copy)NSString *postedWithin;
@property(nonatomic,copy)NSString *currency;
@property(nonatomic,copy)NSString *from;
@property(nonatomic,copy)NSString *to;
@property(nonatomic,copy)NSString *price;
@property(nonatomic,copy)NSString *currencyCode;
@property(nonatomic,copy)NSString *currencySymbol;
@property(nonatomic,copy)NSString *distanceFilter;

@property(nonatomic)NSUInteger pagingIndex;

// leading constraint for moving label
@property(nonatomic)NSInteger leadingConstraint;

-(NSMutableArray *)getFilterArray;
-(NSDictionary *)getParamDictionary;
-(void)resetFilterListings;

@end
