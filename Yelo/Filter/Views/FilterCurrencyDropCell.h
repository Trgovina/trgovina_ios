//
//  FilterCurrencyDropCell.h
//  Trgovina
//
//  Created by apple on 22/11/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface FilterCurrencyDropCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *filterCurrencyLabel;

@end

NS_ASSUME_NONNULL_END
