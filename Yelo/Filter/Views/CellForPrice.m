//
//  CellForPrice.m

//
//  Created by Rahul Sharma on 03/01/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import "CellForPrice.h"
#import "FilterCurrencyDropCell.h"


#define  ACCEPTABLE_CHARACTERSFORPRICE @"1234567890$₹."
@interface CellForPrice()<CurrencyDelegate , UITextFieldDelegate,UITableViewDataSource, UITableViewDelegate>

{
    UIView *filterCurrencyPopUupView;
    NSArray *filtercurrencyData;
}
@end

@implementation CellForPrice 

- (void)awakeFromNib {
    [super awakeFromNib];
    self.labelDIN.textColor=[UIColor blackColor];
    self.labelForCurrency.textColor=mBaseColor;
    
    filtercurrencyData   = [NSArray arrayWithObjects:@"EUR",@"DIN", nil];
    filterCurrencyPopUupView = [[UIView alloc]init];
    filterCurrencyPopUupView.hidden = YES;
    // Initialization code
     self.textField.layer.borderColor = [UIColor colorWithRed:225/255.0f green:225/255.0f blue:225/255.0f alpha:1.0f].CGColor;
    
}


-(void)setObjectsForIndex :(NSIndexPath *)index minPrice:(NSString *)minPrice maxPrice:(NSString *)maxPrice currencyCode:(NSString *)currencyCode
{
    switch (index.row) {
        case 0: self.labelForTitle.text = NSLocalizedString(filterCurrencyTitle, filterCurrencyTitle) ;
           self.dropDownImage.hidden = self.labelForCurrency.hidden = self.currencyButtonOutlet.hidden= self.labelDIN.hidden =  self.currencyBetweenView.hidden= NO;
            self.textField.hidden = YES;
          
            if ([currencyCode isEqualToString:@"EUR"]){
                self.labelForCurrency.textColor = mBaseColor;
                self.labelDIN.textColor = [UIColor blackColor];
            }
            else{
            self.labelDIN.textColor = mBaseColor;
            self.labelForCurrency.textColor = [UIColor blackColor];
            }

            break;
        case 1: self.labelForTitle.text = NSLocalizedString(filterPriceFrom, filterPriceFrom);
            self.textField.hidden = NO;
            self.textField.tag = 1;
            if(minPrice.length > 0)
            {
                self.textField.text = minPrice ;
            }
            else
            {
                self.textField.text = @"";
                self.textField.layer.borderColor = [UIColor colorWithRed:225/255.0f green:225/255.0f blue:225/255.0f alpha:1.0f].CGColor;
            }
          self.dropDownImage.hidden = self.labelForCurrency.hidden = self.currencyButtonOutlet.hidden= self.labelDIN.hidden =  self.currencyBetweenView.hidden = YES;

            break;
        case 2: self.labelForTitle.text = NSLocalizedString(filterPriceTo, filterPriceTo);
            self.textField.tag = 2;
            self.textField.hidden = NO;
            
            if(maxPrice.length > 0)
            {
                self.textField.text = maxPrice ;
            }
            else
            {
                self.textField.text = @"";
                 self.textField.layer.borderColor = [UIColor colorWithRed:225/255.0f green:225/255.0f blue:225/255.0f alpha:1.0f].CGColor;
            }
           self.dropDownImage.hidden = self.labelForCurrency.hidden = self.currencyButtonOutlet.hidden =  self.currencyBetweenView.hidden = self.labelDIN.hidden= YES;
            break;
        default:
            break;
    }

    
}

#pragma mark - TextField Delegate Method

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    self.textField.layer.borderColor = mBaseColor.CGColor;
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    
            NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
            NSArray  *arrayOfString = [newString componentsSeparatedByString:@"."];
            if ([arrayOfString count] > 2 ) {
                return NO;
            }
            else {
                //checking number of characters after dot.
                NSRange range = [newString rangeOfString:@"."];
                if ([newString containsString:@"."] && range.location == (newString.length - 4))
                    return NO;
                else
                    return YES;
            }
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    if(self.textField.text.length==0)
    {
        self.textField.layer.borderColor = [UIColor colorWithRed:225/255.0f green:225/255.0f blue:225/255.0f alpha:1.0f].CGColor;
    }
    
    switch (textField.tag) {
        case 1:
        {
            if(self.callBackForFromPrice)
            {  self.callBackForFromPrice(self.textField.text); }
        }
            break;
        case 2:
        {
            if(self.callBackFortoPrice)
            {  self.callBackFortoPrice(self.textField.text); }

        }
        default:
            break;
    }

    [self.textField resignFirstResponder];
}



- (IBAction)currencyButtonAction:(id)sender {
    
    if (filterCurrencyPopUupView.hidden){
        filterCurrencyPopUupView.hidden = NO;
       filterCurrencyPopUupView.frame = CGRectMake(self.frame.origin.x + self.frame.size.width - 160, self.frame.origin.y + 50,150 ,100 );

        filterCurrencyPopUupView.layer.borderWidth = 1 ;
        filterCurrencyPopUupView.layer.borderColor =[UIColor grayColor].CGColor;
        
        UITableView *filterPopupTableView = self.filterVC.filterCurrencyTableView;
        
        filterPopupTableView.frame = CGRectMake(0, 0, 150, 100);
        
        filterPopupTableView.delegate = self;
        filterPopupTableView.dataSource = self;
        
        [filterCurrencyPopUupView addSubview: filterPopupTableView ];
        [self.filterVC.tableView addSubview:filterCurrencyPopUupView];
    }
    else{
        filterCurrencyPopUupView.hidden = YES;
        [filterCurrencyPopUupView removeFromSuperview];
    }
//    CurrencySelectVC *currencySelectVC = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:mCurrencyStoryboardID];
  //  currencySelectVC.previousSelection = self.labelForCurrency.text ;
//    [currencySelectVC setDelegate:self];
//
//    [ self.navigationController pushViewController :currencySelectVC animated:YES];
//
}


#pragma mark - TableViewDelegate Methods
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return filtercurrencyData.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
FilterCurrencyDropCell *cell =[tableView dequeueReusableCellWithIdentifier:@"FilterCurrencyDropCell"];
    cell.filterCurrencyLabel.text = filtercurrencyData[indexPath.row];
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
//    self.labelForCurrency.text = filtercurrencyData[indexPath.row];
//    self.filterVC.filterSetCurrency = self.labelForCurrency.text;
//    self.listing.currency = filtercurrencyData[indexPath.row];
//    filterCurrencyPopUupView.hidden = YES;
//    [filterCurrencyPopUupView removeFromSuperview];
    
}


#pragma mark - CurrencyDelegate Method
-(void) country:(CurrencySelectVC *)country didChangeValue:(id)value{
    [country setDelegate:nil];
    NSDictionary *countryDict = value;
    self.currencyCode =[NSString stringWithFormat:@"EUR"];
    self.labelForCurrency.text = self.currencyCode;
    self.currencySymbol = [NSString stringWithFormat:@"%@",[countryDict objectForKey:CURRENCY_SYMBOL]];
    if(_callbackForCurrency){
        self.callbackForCurrency(self.currencyCode , self.currencySymbol);
    }
}
- (IBAction)buttonDinAction:(UIButton *)sender {
    
    self.labelDIN.textColor = mBaseColor;
    self.labelForCurrency.textColor = [UIColor blackColor];
    if(_callbackForCurrency){
        self.callbackForCurrency(self.labelDIN.text , self.currencySymbol);
    }
    
}
- (IBAction)buttonEurAction:(UIButton *)sender {
    self.labelForCurrency.textColor = mBaseColor;
    self.labelDIN.textColor = [UIColor blackColor];
    if(_callbackForCurrency){
        self.callbackForCurrency(self.labelForCurrency.text , self.currencySymbol);
    }
}
@end
