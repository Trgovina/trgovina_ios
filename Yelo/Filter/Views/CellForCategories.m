//
//  CellForCategories.m

//
//  Created by Rahul Sharma on 02/01/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import "CellForCategories.h"
#import "CollectionCategoriesCell.h"
#import "SubCategoryTableViewController.h"
#import "SubCategory.h"


#define mCategoryNameResponseKey       @"name"
#define mCategoryActiveImageKey        @"activeimage"

@implementation CellForCategories
{
    NSArray *arrayOfImages;
    NSMutableString *selectedCategory;
    NSURL *imageUrl;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    
    _collectionView.delegate=self;
    _collectionView.dataSource=self;
    
}


/*--------------------------------------*/
#pragma mark -
#pragma mark - CollectionView DataSource Method
/*--------------------------------------*/

- (NSInteger)collectionView:(UICollectionView *)view numberOfItemsInSection:(NSInteger)section {
    
//    if(self.arrayOfCategoryList.count % 2)
//    { return self.arrayOfCategoryList.count + 1 ;}
    
    return self.arrayOfCategoryList.count ;
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)cv cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    CollectionCategoriesCell *cell = [cv dequeueReusableCellWithReuseIdentifier:@"cellCategory" forIndexPath:indexPath];
    
    if(indexPath.row != _arrayOfCategoryList.count){
    [cell setValuesWithArray:self.arrayOfCategoryList[indexPath.row]];
    [cell checkSelectedCategory:self.arrayOfCategoryList[indexPath.row] forFilterListings:self.filterListingsRef];
    }
    else
    {
        cell.labelForCategory.text = @"";
        cell.imageView.image = nil;
        cell.tickMark.hidden = YES;
    }

    return cell;
}

/*-------------------------------------------*/
 #pragma mark -
 #pragma mark - CollectionView Delegate Method
 /*-------------------------------------------*/


 -(CGSize)collectionView:(UICollectionView *)collectionView layout:(nonnull UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    return CGSizeMake((_collectionView.frame.size.width/3),90);
}

-(void)collectionView:(UICollectionView *)cv didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    if(indexPath.row != _arrayOfCategoryList.count){
        
         CollectionCategoriesCell *currentCell = (CollectionCategoriesCell *)[cv cellForItemAtIndexPath:indexPath];
        if(currentCell.tickMark.hidden)
        {
        for (int i=0; i< _arrayOfCategoryList.count;i++ ) {
           
        NSIndexPath *ind =[NSIndexPath indexPathForRow:i inSection:0 ];
        CollectionCategoriesCell *cell = (CollectionCategoriesCell *)[cv cellForItemAtIndexPath:ind];
        if(i == indexPath.row)
        {
        cell.tickMark.hidden = NO;
        }
        else
        {
        cell.tickMark.hidden = YES;
        }
        }
        
        Category *category = self.arrayOfCategoryList[indexPath.row];
            
        self.filterListingsRef.category = category.name;
            self.filterListingsRef.categoryNodeId  = category.categoryNodeId;
            self.filterListingsRef.subcategoryNodeId = category.subCategoryNodeId;

        self.filterListingsRef.categoryImage = category.activeimage;
        [self.filterListingsRef.filtersDictionary setValue:category.name forKey:@"Category"];
          
        if (category.subCategoryCount){
        [[ProgressIndicator sharedInstance]showPIOnView:self.filterVC.view withMessage:LoadingIndicatorTitle];
       // NSDictionary *param = @{@"categoryName":category.name
            NSDictionary *param = @{@"categoryName":flStrForObj(category.name),
                                    @"categoryId":flStrForObj(category.categoryNodeId),
                              msubCategoryNodeId:flStrForObj(category.subCategoryNodeId),
                                    mLanguageCode : [Helper currentLanguage]
                                };
        [WebServiceHandler getSubCategory:param andDelegate:self];
        }
        else if (category.filterCount){
            self.filterListingsRef.subcategoryArray = nil;
            self.filterListingsRef.subcategory = nil;
            self.filterListingsRef.filtersArray = category.filterArray;
            [self.filterVC.tableView reloadData];
        }
        else{
           
            self.filterListingsRef.subcategoryArray = nil;
            self.filterListingsRef.subcategory = nil;
            self.filterListingsRef.filtersArray = nil;
            [self.filterVC.tableView reloadData];
        }
            
    }
        else
        {
            currentCell.tickMark.hidden = NO;
            self.filterListingsRef.category = @"";
            self.filterListingsRef.subcategoryName = @"";
            [self.filterListingsRef.filtersDictionary removeObjectForKey:@"Category"];
            [self.filterListingsRef.filtersDictionary removeObjectForKey:@"Subcategory"];
            [self.filterListingsRef.subFilterKeyValue removeAllObjects];
            self.filterListingsRef.subcategoryArray = nil;
            self.filterListingsRef.subcategory = nil;
            [self.filterVC.tableView reloadData];
            
        }

 }

}


-(void)didFinishLoadingRequest:(RequestType)requestType withResponse:(id)response error:(NSError *)error
{
    [[ProgressIndicator sharedInstance]hideProgressIndicator];
    if(error)
    {
        
    }
    
    if(requestType == RequestTypeSubCategory)
    {
        switch ([response[@"code"]integerValue]) {
            case 200:
            {
                self.filterListingsRef.subcategoryName = @"";
                [self.filterListingsRef.filtersDictionary removeObjectForKey:@"Subcategory"];
                [self.filterListingsRef.subFilterKeyValue removeAllObjects];

            self.filterListingsRef.subcategoryArray = [SubCategory arrayOfSubCategory:response[@"data"]];
            self.filterListingsRef.subcategory = nil;
             [self.filterVC.tableView reloadData];
            }
                break;
            case 204:
             self.filterListingsRef.subcategoryArray = nil;
             self.filterListingsRef.subcategory = nil;
             [self.filterVC.tableView reloadData];
                break;
            default:
                break;
        }
        
    }
    
}


@end
