//
//  SubcategoryFilterTableViewCell.m
//  CollegeStax
//
//  Created by 3Embed on 22/05/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

#import "SubcategoryFilterTableViewCell.h"


@implementation SubcategoryFilterTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}


-(void)setSubcategoryFor :(SubCategory *)subcategory selectedSubcategory :(NSString *)selectedSub
{
    self.subcategoryName.text = subcategory.subCategoryName;
    [self.subcategoryImage sd_setImageWithURL:[NSURL URLWithString:subcategory.image] placeholderImage:[UIImage imageNamed:@"default_filter_icon"]];
    if([selectedSub isEqualToString:subcategory.subCategoryName])
    {
        self.tickMark.hidden = NO;
        
    }
    else
    {
        self.tickMark.hidden = YES;
    }
}

@end
