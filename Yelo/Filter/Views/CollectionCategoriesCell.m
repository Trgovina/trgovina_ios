//
//  CollectionCategoriesCell.m

//
//  Created by Rahul Sharma on 02/01/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#define mCategoryNameResponseKey       @"name"
#define mCategoryActiveImageKey        @"activeimage"
#define mcategoryDeactiveImageKey      @"deactiveimage"

#import "CollectionCategoriesCell.h"

@implementation CollectionCategoriesCell

- (void)awakeFromNib {
    [super awakeFromNib];
  }

-(void)setValuesWithArray:(Category *)category
{
    
    self.labelForCategory.text = category.name;
    [self.imageView sd_setImageWithURL:[NSURL URLWithString:category.activeimage] placeholderImage:[UIImage imageNamed:@""]];
}

-(void)checkSelectedCategory:(Category *)category forFilterListings :(FilterListings *)filterListing
{
    if([filterListing.category isEqualToString:category.name ])
    {
        self.tickMark.hidden = NO;
    }
    else
    {
        self.tickMark.hidden = YES ;
    }
}
@end
