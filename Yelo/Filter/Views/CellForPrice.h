//
//  CellForPrice.h

//
//  Created by Rahul Sharma on 03/01/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FilterViewController.h"
#import "CurrencySelectVC.h"




typedef void (^fromPrice)(NSString *fromPrice);
typedef void (^toPrice)(NSString *toPrice);
typedef void (^currencyDetails)(NSString *currencyCode, NSString *currencySymbol);
@interface CellForPrice : UITableViewCell 
@property (strong, nonatomic) IBOutlet UILabel *labelForTitle;
@property (strong, nonatomic) IBOutlet UILabel *labelForCurrency;
@property (strong, nonatomic) IBOutlet UITextField *textField;

@property (nonatomic, strong)FilterViewController *filterVC ;
@property (nonatomic,strong) Listings *listing ;


@property (weak, nonatomic) IBOutlet UIImageView *dropDownImage;
@property (strong, nonatomic)NSString *currencyCode, *currencySymbol ;
@property (strong,nonatomic) UINavigationController *navigationController;
@property(nonatomic,copy)fromPrice callBackForFromPrice;
@property(nonatomic,copy)toPrice callBackFortoPrice;
@property (nonatomic,copy)currencyDetails callbackForCurrency ;

-(void)setObjectsForIndex :(NSIndexPath *)index minPrice:(NSString *)minPrice maxPrice:(NSString *)maxPrice currencyCode :(NSString *)currencyCode;
@property (weak, nonatomic) IBOutlet UIButton *currencyButtonOutlet;

- (IBAction)currencyButtonAction:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *labelDIN;
@property (weak, nonatomic) IBOutlet UIButton *buttonDinOutlet;
- (IBAction)buttonDinAction:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UIButton *buttonEurOutlet;
- (IBAction)buttonEurAction:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UIView *currencyBetweenView;

@end
