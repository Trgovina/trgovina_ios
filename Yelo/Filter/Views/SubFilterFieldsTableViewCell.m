//
//  SubFilterFieldsTableViewCell.m
//  CollegeStax
//
//  Created by 3Embed on 22/05/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

#import "SubFilterFieldsTableViewCell.h"
#import "SubCategoryTableViewCell.h"

@implementation SubFilterFieldsTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
 }

-(void)setFilterFieldsFor :(Filter *)filterFields
{
    self.subFilterFieldsName.text = filterFields.fieldName;
    self.filterId = filterFields.filterId;

    filterValues = [filterFields.values componentsSeparatedByString:@","];
    [self setUIonTheBasisOfType:filterFields.type];
    [self.listTableView reloadData];
    
}



-(void)setUIonTheBasisOfType:(NSUInteger )type
{
    //    * 1 : textbox
    //    * 2 : checkbox
    //    * 3 : slider
    //    * 4 : radio button
    //    * 5 : range
    //    * 6 : drop down
    //    * 7 : date
    switch (type) {
        case 1:
        case 3:{
            self.sliderView.hidden = YES;
            self.rangeView.hidden = YES;
            self.listTableView.hidden = YES;
            self.textBoxView.hidden = NO;
            self.filterListingObj.advanceFilterCellHeight = 80;
            self.textBoxTextField.placeholder = self.subFilterFieldsName.text;
            
            for (NSDictionary *dic in self.filterListingObj.subFilterKeyValue) {
              //  if([dic[@"fieldName"] isEqualToString:self.subFilterFieldsName.text] )
                if([dic[@"fieldName"] isEqualToString:self.filterId] )
                {
                    self.textBoxTextField.text = dic[@"value"];
                    break;
                }
            }
           }
            break;
        case 2:
        case 4:
        case 6:
            self.sliderView.hidden = YES;
            self.rangeView.hidden = YES;
            self.listTableView.hidden = NO;
            self.textBoxView.hidden = YES;
            CGFloat listHeight1 = (filterValues.count * 50) + 40;
            self.filterListingObj.advanceFilterCellHeight = listHeight1;
            break;
        case 5:
        case 7:
            self.sliderView.hidden = YES;
            self.rangeView.hidden = NO;
            self.listTableView.hidden = YES;
            self.textBoxView.hidden = YES;
             self.filterListingObj.advanceFilterCellHeight = 140;
            
            for (NSDictionary *dic in self.filterListingObj.subFilterKeyValue) {
                if([dic[@"fieldName"] isEqualToString:self.subFilterFieldsName.text] )
                {
                    self.rangeFromTextfield.text = dic[@"from"];
                    self.rangeToTextField.text = dic[@"to"];
                    fromRangeValue = self.rangeFromTextfield.text;
                    toRangeValue = self.rangeToTextField.text;
                    break;
                }
            }
            break;
        default:
            break;
    }
    
}



#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return filterValues.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    SubCategoryTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SubFilterValueCell" forIndexPath:indexPath];
    [cell setValuesForFilter:filterValues[indexPath.row] previousSelection:@""];
    [cell checkSelectedKeyValueFilter:self.filterListingObj.subFilterKeyValue forValue:filterValues[indexPath.row] fieldName:self.subFilterFieldsName.text];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    SubCategoryTableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    
    if(cell.tickMarkImage.hidden)
    {
      cell.tickMarkImage.hidden = NO;
      NSMutableDictionary *dict = [NSMutableDictionary new];
     
      //[dict setValue:self.subFilterFieldsName.text forKey:@"fieldName"];
        [dict setValue:self.subFilterFieldsName.text forKey:@"fieldName"];

      [dict setValue:filterValues[indexPath.row] forKey:@"value"];
      [dict setValue:@"equlTo" forKey:@"type"];
      [self.filterListingObj.subFilterKeyValue addObject:dict];
    }
    else
    {
        for (NSDictionary *dict in self.filterListingObj.subFilterKeyValue) {
            
            if([dict[@"value"] isEqualToString:filterValues[indexPath.row]])
            {
                [self.filterListingObj.subFilterKeyValue removeObject:dict];
                cell.tickMarkImage.hidden = YES;
                break;
            }
        }
    }
  
    
}


#pragma mark - TextField Delegate

#pragma mark - Textfield view data source

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    
}

- (IBAction)textFieldValueEditingChanged:(id)sender {
    
    UITextField *textField = (UITextField *) sender;
    switch (textField.tag) {
        case 0:{
            textBoxValue = textField.text;
            
            if(textBoxValue.length){
            for (NSDictionary *dic in self.filterListingObj.subFilterKeyValue) {
                if([dic[@"fieldName"] isEqualToString:self.filterId] )
                {
                    [self.filterListingObj.subFilterKeyValue removeObject:dic];
                    break;
                }
            }
            NSMutableDictionary *dict = [NSMutableDictionary new];
           // [dict setValue:self.subFilterFieldsName.text forKey:@"fieldName"];
                [dict setValue:self.filterId forKey:@"fieldName"];

            [dict setValue:textBoxValue forKey:@"value"];
            [dict setValue:@"equlTo" forKey:@"type"];
            [self.filterListingObj.subFilterKeyValue addObject:dict];
            }
        }
            break;
        case 2:
            fromRangeValue = textField.text;
            [self setRangeValues];
            break;
        case 1:
            toRangeValue = textField.text;
            [self setRangeValues];
           
            break;
            
        default:
            break;
    }
    
}

-(void)setRangeValues
{
    if(toRangeValue.length && fromRangeValue.length)
    {
        for (NSDictionary *dic in self.filterListingObj.subFilterKeyValue) {
            if([dic[@"fieldName"] isEqualToString:self.filterId] )
            {
                [self.filterListingObj.subFilterKeyValue removeObject:dic];
                break;
            }
        }
        
         NSString *value = [NSString stringWithFormat:@"%@ %@ - %@",self.subFilterFieldsName.text,fromRangeValue,toRangeValue];
        
        NSMutableDictionary *dict = [NSMutableDictionary new];
        //[dict setValue:self.subFilterFieldsName.text forKey:@"fieldName"];
        [dict setValue:self.filterId forKey:@"fieldName"];

        [dict setValue:fromRangeValue forKey:@"from"];
        [dict setValue:toRangeValue forKey:@"to"];
        [dict setValue:@"range" forKey:@"type"];
        [dict setValue:value forKey:@"value"];
        [self.filterListingObj.subFilterKeyValue addObject:dict];
    }
    
}


@end
