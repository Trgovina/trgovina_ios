
//
//  Created by Rahul Sharma on 31/12/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

/* Ajay Thakur */

#import "FilterViewController.h"
#import "CellForLocation.h"
#import "CellForCategories.h"
#import "CellForDistance.h"
#import "CellForSorting.h"
#import "CellForPrice.h"
#import "Helper.h"
#import "PinAddressController.h"
#import "SubcategoryFilterTableViewCell.h"
#import "SubFilterFieldsTableViewCell.h"


#define mDefaultColor   [UIColor colorWithRed:72/255.0f green:72/255.0f blue:72/255.0f alpha:1.0f]
@interface FilterViewController ()

@end

@implementation FilterViewController
{
    NSArray *arrayOfSections;
    NSArray *arrayOfCells;
    NSArray *arrayOfSortingBy;
    NSArray *arrayOfPostWithIn;
    UITapGestureRecognizer *tap;
    NSInteger i;
    NSString *price,*distance ;
    UITapGestureRecognizer *gestureRecognizer;

}

#pragma mark-
#pragma mark - ViewController LifeCycle

- (void)viewDidLoad {
    [super viewDidLoad];
   
    i=0;
    minPrce = self.filterListings.from;
    maxPrce = self.filterListings.to;
    arrayOfSections = [[NSArray alloc]initWithObjects:filterLocationSectionTitle,filterCategorySectionTitle,subcategoryFilter,advanceFilter,filterDistanceSectionTitle,filterSortBySectionTitle,filterPostedWithInSectionTitle,filterPriceSectionTitle, nil];
    
    arrayOfCells=[[NSArray alloc]initWithObjects:@"cellForLocation",@"cellForCategories",@"SubcategoryFilterCell",@"SubFilterFieldsCell",@"cellForSlider",@"cellForSort",@"cellForSort",@"cellForPrice",nil];
    
    arrayOfSortingBy=[[NSArray alloc]initWithObjects:sortingByNewestFirst,sortingByClosestFirst,sortingByPriceLowToHigh,sortingByPriceHighToLow,nil];
    
    arrayOfPostWithIn = [[NSArray alloc]initWithObjects:postedWithLast24h,postedWithLast7d,postedWithLast30d,postedWithAllProducts,nil];
    
    [_tableView setSeparatorStyle:UITableViewCellSeparatorStyleSingleLine];
    [_tableView setSeparatorColor:[UIColor colorWithRed:225/255.0 green:225/255.0 blue:225/255.0 alpha:1]];
    _tableView.delegate=self;
    _tableView.dataSource=self;
    self.automaticallyAdjustsScrollViewInsets = NO;
    self.navigationItem.title = NSLocalizedString(navTitleForFilterScreen, navTitleForFilterScreen);
    [self createNavLeftButtonandCreateNavRightButton];
    
    gestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard)];
    gestureRecognizer.enabled = NO;
    [self.tableView addGestureRecognizer:gestureRecognizer];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(veiwMoveAccKeyboardInFilter:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(veiwMoveToOriginalInFilter) name:UIKeyboardWillHideNotification object:nil];
    
    
    self.navigationController.navigationBar.hidden = NO;
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark-
#pragma mark - TableView DataSource Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return arrayOfSections.count;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    switch (section) {
        case 0:
        case 1:
            return 1;
        case 2:
            return self.filterListings.subcategoryArray.count;
            break;
        case 3:
            return self.filterListings.filtersArray.count;
        case 4:
            return 1;
        case 5:
        case 6:
            return 4;
            break;
        case 7:
            return 3;
            break;
        default:
            return 0;
    }
    
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.section) {
        case 0:{
            CellForLocation *cell=[tableView dequeueReusableCellWithIdentifier:[arrayOfCells objectAtIndex:indexPath.section]];
            if(self.filterListings.location.length)
            {
                
                cell.LabelForChangeLocationTitle.hidden = YES;
                cell.selectedLocLab.hidden = NO;
                cell.selectedLocLab.text = self.filterListings.location;
            }
            return cell;
        }
            break;
            
        case 1:{
            CellForCategories *cell=[tableView dequeueReusableCellWithIdentifier:[arrayOfCells objectAtIndex:indexPath.section]];
            cell.filterVC = self ;
            cell.filterListingsRef = self.filterListings;
            cell.arrayOfCategoryList = self.categoryArray;
            [cell.collectionView reloadData];
            return cell;
        }
            
        case 2:{
            SubcategoryFilterTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:[arrayOfCells objectAtIndex:indexPath.section]] ;
            [cell setSubcategoryFor:self.filterListings.subcategoryArray[indexPath.row] selectedSubcategory:self.filterListings.subcategoryName];
            return cell;
        }
            
        case 3:
        {
            SubFilterFieldsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:[arrayOfCells objectAtIndex:indexPath.section]];
            cell.filterListingObj = self.filterListings;
            [cell setFilterFieldsFor:self.filterListings.filtersArray[indexPath.row]];
            return cell;
        }
            
        case 4:{
            CellForDistance *cell=[tableView dequeueReusableCellWithIdentifier:[arrayOfCells objectAtIndex:indexPath.section]];
            cell.leadingConstraint = self.filterListings.leadingConstraint;
            [cell setValuesForSlider:[self.filterListings.distanceMax intValue]   andLeadingConstraint:self.filterListings.leadingConstraint];
            cell.callBackForDistance =^(int dist, NSInteger leadingConstraint)
            {
                distnce = dist ;
                self.filterListings.distanceMax = [NSString stringWithFormat:@"%ld",(long)distnce] ;
                self.filterListings.leadingConstraint = leadingConstraint ;
            };
            return cell;
        }
        case 5:{
            CellForSorting *cell=[tableView dequeueReusableCellWithIdentifier:[arrayOfCells objectAtIndex:indexPath.section]];
            if([[arrayOfSortingBy objectAtIndex:indexPath.row] isEqualToString:self.filterListings.sortBy])
            {
                cell.imageMark.hidden=NO;
                cell.labelForSortBy.textColor = mBaseColor;
            }
            else{
                cell.imageMark.hidden=YES;
                cell.labelForSortBy.textColor = mDefaultColor;
                
            }
            
            cell.labelForSortBy.text = NSLocalizedString([arrayOfSortingBy objectAtIndex:indexPath.row],[arrayOfSortingBy objectAtIndex:indexPath.row]);
            return cell;
        }
            
        case 6:{
            CellForSorting *cell=[tableView dequeueReusableCellWithIdentifier:[arrayOfCells objectAtIndex:indexPath.section]];
            if([[arrayOfPostWithIn objectAtIndex:indexPath.row] isEqualToString:self.filterListings.postedWithin])
            {
                cell.imageMark.hidden=NO;
                cell.labelForSortBy.textColor = mBaseColor;
            }
            else
            {
                cell.imageMark.hidden=YES;
                cell.labelForSortBy.textColor = mDefaultColor;
                if ((indexPath.row==3)&& (!self.filterListings.postedWithin.length))
                {
                    cell.imageMark.hidden=NO;
                    cell.labelForSortBy.textColor = mBaseColor;
                }
            }
            cell.labelForSortBy.text = NSLocalizedString([arrayOfPostWithIn objectAtIndex:indexPath.row], [arrayOfPostWithIn objectAtIndex:indexPath.row]);
            return cell;
        }
            
        case 7:{
            CellForPrice *cell=[tableView dequeueReusableCellWithIdentifier:[arrayOfCells objectAtIndex:indexPath.section]];
            cell.filterVC = self;
          //  cell.listing = self.listings ;
            cell.navigationController = self.navigationController;
            [cell setObjectsForIndex:indexPath minPrice:self.filterListings.from maxPrice:self.filterListings.to currencyCode:self.filterListings.currencyCode];
            
            cell.callBackForFromPrice =^(NSString *fromPrice)
            {
                minPrce = fromPrice;
            };
            cell.callBackFortoPrice = ^(NSString *toPrice)
            {
                maxPrce = toPrice;
            };
            
            cell.callbackForCurrency = ^(NSString *currCode, NSString *currSymbol)
            {
                self.filterListings.currencyCode = currCode;
                self.filterListings.currencySymbol = currCode;
                
            };
            return cell;
        }
            
        default:
        {
            UITableViewCell *cell1 = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@""];
            return cell1;
        }
            break;
            
    }
}

#pragma mark-
#pragma mark - TableView Delegate Methods

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.section) {
        case 1:
        {
            
            int count = (unsigned)self.categoryArray.count ;
            if(!count)
            {return 0; }
            
            if(count % 3)
            { count += 2;}
            return 100 *(count/3);
        }
            break;
        case 3:
        {
            return self.filterListings.advanceFilterCellHeight;
        }
        case 4:
        {
            return 90;
        }
            
        default:
            break;
    }
    return 50;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if(section == 2)
    {
        if(!self.filterListings.subcategoryArray.count)
        {
            return 0 ;
        }
    }
    else if (section == 3)
    {
        if (!self.filterListings.filtersArray.count)
        {
            return 0 ;
        }
    }
    
    return 50;
}

/**
 This is Header for sections.
 @return view.
 */
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *sectionView=nil;
    UIView *subView=nil;
    UIView *divider=nil;
    CGFloat width =tableView.frame.size.width;
    sectionView=[[UIView alloc]initWithFrame:CGRectMake(0, 0,width ,50)];
    sectionView.backgroundColor=[UIColor colorWithRed:248/255.0 green:248/255.0 blue:248/255.0 alpha:1];
    
    divider=[[UIView alloc]initWithFrame:CGRectMake(0, 0, width,0.5)];
    divider.backgroundColor=[UIColor colorWithRed:225/255.0 green:225/255.0 blue:225/255.0 alpha:1];
    [sectionView addSubview:divider];
    subView=[[UIView alloc]initWithFrame:CGRectMake(0, 0.5, tableView.frame.size.width,48)];
    subView.backgroundColor=[UIColor colorWithRed:248/255.0 green:248/255.0 blue:248/255.0 alpha:1];
    
    AlignmentOfLabel* lableIn=[[AlignmentOfLabel alloc]initWithFrame:CGRectMake(15, sectionView.center.y, sectionView.frame.size.width - 30, 15)];
    [Helper setToLabel:lableIn Text:[arrayOfSections objectAtIndex:section] WithFont:@"Roboto-medium" FSize:13 Color:[UIColor colorWithRed:153/255.0 green:153/255.0 blue:153/255.0 alpha:1]];
    [subView addSubview:lableIn];
    [sectionView addSubview:subView];
    divider=[[UIView alloc]initWithFrame:CGRectMake(0, 49.5, width,0.5)];
    divider.backgroundColor=[UIColor colorWithRed:225/255.0 green:225/255.0 blue:225/255.0 alpha:1];
    [sectionView addSubview:divider];
    return sectionView;
    
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.section) {
        case 0:
        {   PinAddressController *pinAddressVC = [self.storyboard instantiateViewControllerWithIdentifier:mPinAddressStoryboardID];
            pinAddressVC.navigationItem.title = NSLocalizedString(navTitleForChangeLocation, navTitleForChangeLocation) ;
            pinAddressVC.lat = [self.filterListings.latitude doubleValue];
            pinAddressVC.longittude = [self.filterListings.longitude doubleValue];
            pinAddressVC.isFromFilters = YES ;
            pinAddressVC.callBackForLocation=^(NSDictionary *locDict)
            {
                CellForLocation *cell = [tableView cellForRowAtIndexPath:indexPath];
                cell.selectedLocLab.hidden = NO;
                cell.LabelForChangeLocationTitle.hidden = YES;
                cell.selectedLocLab.text = locDict[@"address"];
                
                self.filterListings.latitude = locDict[@"lat"];
                self.filterListings.longitude = locDict[@"long"];
                self.filterListings.location = locDict[@"address"];
            };
            [self.navigationController pushViewController:pinAddressVC animated:YES];
        }
            break;
        case 2:
        {
            SubcategoryFilterTableViewCell *currentCell=[tableView cellForRowAtIndexPath:indexPath];
            
            if(currentCell.tickMark.hidden){
                
                [self.filterListings.subFilterKeyValue removeAllObjects];
                for(i=0;i<self.filterListings.subcategoryArray.count;i++)
                {
                    NSIndexPath *ind =[NSIndexPath indexPathForRow:i inSection:2 ];
                    SubcategoryFilterTableViewCell *cell=[tableView cellForRowAtIndexPath:ind];
                    if(i == indexPath.row)
                    {
                        cell.tickMark.hidden = NO;
                    }
                    else
                    {
                        cell.tickMark.hidden = YES;
                    }
                }
                self.filterListings.subcategory = self.filterListings.subcategoryArray[indexPath.row];
                SubCategory *subcategoryObj = self.filterListings.subcategory;
                self.filterListings.subcategoryName = subcategoryObj.subCategoryName;
              //  self.filterListings.subcategoryNodeId = subcategoryObj.subCategoryNodeId;

                self.filterListings.subcategoryImage = subcategoryObj.image;
                [self.filterListings.filtersDictionary setValue:subcategoryObj.subCategoryName forKey:@"Subcategory"];
                self.filterListings.filtersArray = self.filterListings.subcategory.filterArray;
            }
            //         else
            //            {
            //             currentCell.tickMark.hidden = YES;
            //             [self.filterListings.subFilterKeyValue removeAllObjects];
            //            }
            [self.tableView reloadData];
        }
            break;
        case 3:{
            SubcategoryFilterTableViewCell *currentCell=[tableView cellForRowAtIndexPath:indexPath];
            
            if(currentCell.tickMark.hidden){
                
                [self.filterListings.subFilterKeyValue removeAllObjects];
                for(i=0;i<self.filterListings.filtersArray.count;i++)
                {
                    NSIndexPath *ind =[NSIndexPath indexPathForRow:i inSection:2 ];
                    SubcategoryFilterTableViewCell *cell=[tableView cellForRowAtIndexPath:ind];
                    if(i == indexPath.row)
                    {
                        cell.tickMark.hidden = NO;
                    }
                    else
                    {
                        cell.tickMark.hidden = YES;
                    }
                }
                self.filterListings.subcategory = self.filterListings.filtersArray[indexPath.row];
                SubCategory *subcategoryObj = self.filterListings.subcategory;
                self.filterListings.subcategoryName = subcategoryObj.subCategoryName;
                //  self.filterListings.subcategoryNodeId = subcategoryObj.subCategoryNodeId;
                
                self.filterListings.subcategoryImage = subcategoryObj.image;
                [self.filterListings.filtersDictionary setValue:subcategoryObj.subCategoryName forKey:@"Subcategory"];
                self.filterListings.filtersArray = self.filterListings.subcategory.filterArray;
            }
            //         else
            //            {
            //             currentCell.tickMark.hidden = YES;
            //             [self.filterListings.subFilterKeyValue removeAllObjects];
            //            }
            [self.tableView reloadData];
        }
            break;
        case 5:
        {
            CellForSorting *cell=[tableView cellForRowAtIndexPath:indexPath];
            for(i=0;i<arrayOfSortingBy.count;i++)
            {
                NSIndexPath *ind =[NSIndexPath indexPathForRow:i inSection:5 ];
                CellForSorting *cell=[tableView cellForRowAtIndexPath:ind];
                if(i!=indexPath.row)
                {
                    cell.imageMark.hidden=YES;
                    cell.labelForSortBy.textColor = mDefaultColor;
                }
            }
            if(cell.imageMark.hidden){
                cell.imageMark.hidden=NO;
                cell.labelForSortBy.textColor = mBaseColor;
                
                self.filterListings.sortBy = [arrayOfSortingBy objectAtIndex:indexPath.row];
            }
            else
            {
                self.filterListings.sortBy = @"";
                cell.imageMark.hidden=YES;
                cell.labelForSortBy.textColor = mDefaultColor;
            }
        }
            break;
        case 6:
        {
            CellForSorting *cell=[tableView cellForRowAtIndexPath:indexPath];
            for(i=0;i<arrayOfPostWithIn.count;i++)
            {
                NSIndexPath *ind =[NSIndexPath indexPathForRow:i inSection:6 ];
                CellForSorting *cell=[tableView cellForRowAtIndexPath:ind];
                if(i!=indexPath.row){
                    cell.imageMark.hidden=YES;
                    cell.labelForSortBy.textColor = mDefaultColor;
                    self.filterListings.postedWithin = @"";
                }
                
            }
            if(cell.imageMark.hidden){
                cell.labelForSortBy.textColor = mBaseColor;
                cell.imageMark.hidden=NO;
                if(indexPath.row!=3)
                    self.filterListings.postedWithin =[arrayOfPostWithIn objectAtIndex:indexPath.row];
            }
            
        }
        default:
            break;
    }
    
}

-(void)requestForFilter {
    NSInteger max = [maxPrce integerValue];
    NSInteger min = [minPrce integerValue];
    
    if((maxPrce.length >0 && minPrce.length >0) && min > max)
    {
        [Helper showAlertWithTitle:nil Message:NSLocalizedString(minPriceAlertMessage, minPriceAlertMessage) viewController:self];
    }
    else
    {
        if(maxPrce.length >0 || minPrce.length >0)
        {
            if((minPrce.length > 0) && !(maxPrce.length > 0))
            {
                NSString *fromCost = [NSString stringWithFormat:@"%@ %@ ",self.filterListings.currencySymbol,minPrce];
                price = [[NSString stringWithFormat:@"%@",@"From "] stringByAppendingString:[NSString stringWithFormat:@"%@",fromCost]];
            }
            else if(!(minPrce.length > 0) && (maxPrce.length > 0))
            {
                NSString *fromCost = [NSString stringWithFormat:@"%@ %@ ",self.filterListings.currencySymbol,maxPrce];
                price = [[NSString stringWithFormat:@"%@",@"To "] stringByAppendingString:[NSString stringWithFormat:@"%@",fromCost]];
            }
            else
            {
                NSString *fromCost = [NSString stringWithFormat:@"%@ %@ ",self.filterListings.currencySymbol, minPrce];
                NSString *toCost  =  [NSString stringWithFormat:@"%@ %@ ",self.filterListings.currencySymbol,maxPrce];
                price = [[[NSString stringWithFormat:@"%@",fromCost]stringByAppendingString:@" - "]stringByAppendingString:toCost]  ;
            }
        }
        self.filterListings.from = minPrce;
        self.filterListings.to = maxPrce;
        self.filterListings.price = price;
        self.filterListings.distanceFilter = [NSString stringWithFormat:@"%ld Km",(long)distnce];
        
        [self.delegate applyfilterForSelectedLists];
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}



#pragma mark Apply Filters

- (IBAction)applyFiltersButtonAction:(id)sender {
    if ([CLLocationManager locationServicesEnabled]) {
        if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusDenied) {
            [self actionWhenLocationDisabled];
        }
        else {
            [self requestForFilter];
        }
    }
    else {
        [self actionWhenLocationDisabled];
    }
}


-(void)actionWhenLocationDisabled {
    UIAlertController *alertForLocation = [UIAlertController alertControllerWithTitle:NSLocalizedString(allowLocationAlertTitle, allowLocationAlertTitle) message:NSLocalizedString(allowLocationAlertMessage, allowLocationAlertMessage) preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *actionForOk = [UIAlertAction actionWithTitle:NSLocalizedString(alertOk, alertOk) style:UIAlertActionStyleDefault handler:^(UIAlertAction *Handle){
    }];
    
    UIAlertAction *actionForsettings = [UIAlertAction actionWithTitle:NSLocalizedString(allowLocationSettingsActionTitle, allowLocationSettingsActionTitle) style:UIAlertActionStyleDefault handler:^(UIAlertAction *Handle){
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
    }];
    
    [alertForLocation addAction:actionForsettings];
    [alertForLocation addAction:actionForOk];
    [self presentViewController:alertForLocation animated:YES completion:nil];
}




- (void)createNavLeftButtonandCreateNavRightButton {
    [self.navCancelButtonOutlet addTarget:self action:@selector(navBarLeftButtonAction) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *navLeft = [[UIBarButtonItem alloc]initWithCustomView:self.navCancelButtonOutlet];
    self.navigationItem.leftBarButtonItem = navLeft;
    
    [self.navResetButtonOutlet addTarget:self action:@selector(navBarRightButtonAction) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *navRight = [[UIBarButtonItem alloc]initWithCustomView:self.navResetButtonOutlet];
    self.navigationItem.rightBarButtonItem = navRight;
}

- (void)navBarRightButtonAction {
    [self.view endEditing:YES];
    minPrce = @"";
    maxPrce = @"";
    distnce = 0;
    self.filterListings.leadingConstraint = 40;
    NSIndexPath *index = [NSIndexPath indexPathForRow:0 inSection:0];
    CellForLocation *cell = [self.tableView cellForRowAtIndexPath:index];
    cell.LabelForChangeLocationTitle.hidden = NO;
    cell.selectedLocLab.hidden = YES;
    [self.filterListings resetFilterListings];
    [self.tableView reloadData];
}



- (void)navBarLeftButtonAction{
   
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark - Move View

/**
 This method will move view by evaluating keyboard height.
 
 @param notification post by Notificationcentre.
 */
-(void)veiwMoveAccKeyboardInFilter :(NSNotification *)notification {
    NSDictionary* info = [notification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0,kbSize.height, 0.0);
    self.tableView.contentInset = contentInsets;
    self.tableView.scrollIndicatorInsets = contentInsets;
    gestureRecognizer.enabled = YES;
    
}

/**
 This method will move view back to original frame.
 */
-(void)veiwMoveToOriginalInFilter
{
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    self.tableView.contentInset = contentInsets;
    self.tableView.scrollIndicatorInsets = contentInsets;
    
}


-(void)hideKeyboard
{
    [self.view endEditing:YES];
    gestureRecognizer.enabled = NO ;
}



@end

