//
//  SingletonData.h
//  Trgovina
//
//  Created by 3Embed on 25/07/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SingletonData : NSObject

@property (nonatomic,strong)NSArray *ChatSuggestionMessages;

+ (id)sharedInstance;
-(void)initWithChatSuggestions :(NSArray *)chats;
@end
