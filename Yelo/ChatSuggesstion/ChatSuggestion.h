//
//  ChatSuggestion.h
//  Trgovina
//
//  Created by 3Embed on 25/07/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ChatSuggestion : NSObject

@property(nonatomic,copy)NSString *ID;
@property(nonatomic,copy)NSString *message;
@property(nonatomic,copy)NSString *timestamp;

- (instancetype)initWithDictionary:(NSDictionary *)response;
+(NSArray *) arrayOfSuggestions :(NSDictionary *)responseData ;
@end
