//
//  ChatSuggestion.m
//  Trgovina
//
//  Created by 3Embed on 25/07/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

#import "ChatSuggestion.h"

@implementation ChatSuggestion

- (instancetype)initWithDictionary:(NSDictionary *)response;
{
    self = [super init];
    if (!self) { return nil; }
    
    self.ID = flStrForObj(response[@"_id"]);
     self.message = flStrForObj(response[@"message"]);
     self.timestamp = flStrForObj(response[@"timestamp"]);
    
    return self;
}



+(NSArray *) arrayOfSuggestions :(NSDictionary *)responseData
{
    NSMutableArray *chatSuggestions = [[NSMutableArray alloc]init];
    for(NSDictionary *chatDict in responseData) {
        
        ChatSuggestion *chat = [[ChatSuggestion alloc]initWithDictionary:chatDict] ;
        [chatSuggestions addObject:chat];
    }
    return chatSuggestions;
}
@end
