//
//  SingletonData.m
//  Trgovina
//
//  Created by 3Embed on 25/07/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

#import "SingletonData.h"

@implementation SingletonData

static SingletonData *instance = nil;

+(id)sharedInstance
{
    if(instance == nil)
    {
        instance= [SingletonData new];
        
    }
    return instance;
}

-(void)initWithChatSuggestions :(NSArray *)chats
{
    self.ChatSuggestionMessages = chats;
}

@end
