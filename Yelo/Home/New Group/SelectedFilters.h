//
//  SelectedFilters.h
//  CollegeStax
//
//  Created by 3Embed on 21/05/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SelectedFilters : NSObject

@property(nonatomic,copy)NSString *type;
@property(nonatomic,copy)NSString *name;
@property(nonatomic,copy)NSString *image;

- (instancetype)initWithDictionary:(NSDictionary *)response;
+(NSMutableArray *) arrayOfSeletedFilters :(NSArray *)responseData ;

@end
