//
//  ListingCollectionViewCell.h
//  Created by Rahul Sharma on 4/12/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//


@interface ListingCollectionViewCell : UICollectionViewCell
@property (strong, nonatomic) IBOutlet UIView *featuredView;
@property (weak, nonatomic) IBOutlet UIImageView *postedImageOutlet;

@property (weak, nonatomic) IBOutlet UILabel *price;
@property (weak, nonatomic) IBOutlet UILabel *productName;
@property (weak, nonatomic) IBOutlet UILabel *swapFor;
@property (weak, nonatomic) IBOutlet UIImageView *swapIcon;

-(void)setProducts:(ProductDetails *)product;
@property (weak, nonatomic) IBOutlet UIView *priceNameView;

@end
