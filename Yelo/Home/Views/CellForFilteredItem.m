//
//  CellForFilteredItem.m
//
//  Created by Rahul Sharma on 06/01/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import "CellForFilteredItem.h"

@implementation CellForFilteredItem

-(void)awakeFromNib
{
    [super awakeFromNib];
    
  self.layer.borderColor = mBaseColor2.CGColor ;
}

-(void)setValueForSelectedFilters :(SelectedFilters *)filter
{
    self.filterName.text = filter.name;
    [self.filterImage sd_setImageWithURL:[NSURL URLWithString:filter.image] placeholderImage:[UIImage imageNamed:@"filterImage"]];
}

@end
