//
//  LikeViewController.m

//
//  Created by Rahul Sharma on 4/19/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import "LikeViewController.h"
#import "WebServiceConstants.h"
#import "WebServiceHandler.h"
#import "ProfileViewController.h"
#import "TinderGenericUtility.h"
#import "ProgressIndicator.h"
#import "UIImageView+WebCache.h"
#import "UIImage+GIF.h"
#import "FontDetailsClass.h"
#import "Helper.h"
#import "UIScrollView+SVInfiniteScrolling.h"

@interface LikeViewController ()<WebServiceHandlerDelegate>
{
    NSMutableArray *followersresponseData;
    NSMutableArray *followingresponseData;
    NSMutableArray *likesData;
    NSMutableArray *arrayOfusername;
    NSMutableArray *arrayOffullname;
    NSMutableArray *arrayOfProfilePicUrl;
    NSMutableArray *arrayOfFollowingStaus;
    NSMutableArray *arrayOfMemberPrivateStatus;
    UIRefreshControl *refreshControl;
    bool infiniteRefreshAdded;
    int likesDataPagingIndex ;
}
@end

@implementation LikeViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    likesData = [NSMutableArray new];
    
    infiniteRefreshAdded = YES;
    self.postType = @"0" ;
    //customizing nav bar.
    [self navigationBarCustomization];
    [self addRefreshControl];
    self.progressIndicatorView.hidden = NO;
    self.tableView.hidden = YES;
    //if user wants to see followerslist then followerslistapi will call and passing token as parameter.
    if([self.navigationTitle isEqualToString:NSLocalizedString(navTitleForFollowers, navTitleForFollowers)]) {
        [self requestForFollowersDetails];
    }
    //if user wants to see followinglist then followinglistapi will call and passing token as parameter.
    if ([self.navigationTitle isEqualToString:NSLocalizedString(navTitleForFollowing, navTitleForFollowing)]) {
        [self requestForFollowingDetails];
    }
    if ([self.navigationTitle isEqualToString:NSLocalizedString(navTitleForLikes, navTitleForLikes)]) {
        likesDataPagingIndex = 0;
        [self requestAllLikesOnPostWithIndex:likesDataPagingIndex];
    }
    [self updateFollowStatus];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    self.navigationController.navigationBar.translucent = NO;
    self.navigationController.navigationBar.shadowImage = nil;
}

-(void)updateFollowStatus {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateFollwoStatus:) name:@"updatedFollowStatus" object:nil];
}


-(void)updateFollwoStatus:(NSNotification *)noti {
    //check the postId and Its Index In array.
        NSString *userName = flStrForObj(noti.object[@"newUpdatedFollowData"][@"userName"]);
        NSString *foolowStatusRespectToUser = noti.object[@"newUpdatedFollowData"][@"newFollowStatus"];
    
    
        for (int i=0; i <arrayOfusername.count;i++) {
            if ([flStrForObj(arrayOfusername[i]) isEqualToString:userName]) {
                arrayOfFollowingStaus[i] = foolowStatusRespectToUser;
                NSIndexPath* rowToReload = [NSIndexPath indexPathForRow:i inSection:0];
                NSArray* rowsToReload = [NSArray arrayWithObjects:rowToReload, nil];
                [self.tableView reloadRowsAtIndexPaths:rowsToReload withRowAnimation:UITableViewRowAnimationNone];
                break;
            }
        }
}

-(void)addRefreshControl {
    refreshControl = [[UIRefreshControl alloc]init];
    [self.tableView addSubview:refreshControl];
    [refreshControl addTarget:self action:@selector(refreshTable:) forControlEvents:UIControlEventValueChanged];
}
-(void)refreshTable:(id)sender {
    
    if ([self.navigationTitle isEqualToString:NSLocalizedString(navTitleForLikes, navTitleForLikes)]) {
        [self requestAllLikesOnPostWithIndex:0];
    }
    if ([self.navigationTitle isEqualToString:NSLocalizedString(navTitleForFollowing, navTitleForFollowing)])  {
        [self requestForFollowingDetails];
    }
    if([self.navigationTitle isEqualToString:NSLocalizedString(navTitleForFollowers, navTitleForFollowers)]) {
        [self requestForFollowersDetails];
    }
    [refreshControl endRefreshing];
}




#pragma mark - Request ForPost

-(void)requestAllLikesOnPostWithIndex :(NSInteger )offsetIndex {
    NSDictionary *requestDict = @{
                                  mauthToken:flStrForObj([Helper userToken]),
                                  mpostid:flStrForObj(self.postId),
                                  mposttype:self.postType,
                                  moffset:flStrForObj([NSNumber numberWithInteger:30 * likesDataPagingIndex]),
                                  mlimit:flStrForObj([NSNumber numberWithInteger:30])
                                  };
    [WebServiceHandler getAllLikesOnPost:requestDict andDelegate:self];
}

-(void)requestForFollowersDetails {
    //user can check his/her own  following list or he can check others.
    if (self.getdetailsDetailsOfUserName && ![self.getdetailsDetailsOfUserName isEqualToString:[Helper userName]]) {
        NSDictionary *requestDict = @{mauthToken : flStrForObj([Helper userToken]),
                                      mmemberName :self.getdetailsDetailsOfUserName
                                      };
        [WebServiceHandler getMemberFollowersList:requestDict andDelegate:self];
    }
    else {
        NSDictionary *requestDict = @{mauthToken : flStrForObj([Helper userToken]),
                                      };
        [WebServiceHandler getFollowersList:requestDict andDelegate:self];
    }
}

-(void)requestForFollowingDetails {
    //user can check his/her own  followers list or he can check others.
    if (self.getdetailsDetailsOfUserName && ![self.getdetailsDetailsOfUserName isEqualToString:[Helper userName]]) {
        NSDictionary *requestDict = @{mauthToken : flStrForObj([Helper userToken]),
                                      mmemberName :self.getdetailsDetailsOfUserName
                                      };
        [WebServiceHandler getMemberFollowingList:requestDict andDelegate:self];
    }
    else {
        NSDictionary *requestDict = @{mauthToken : flStrForObj([Helper userToken]),
                                      };
        [WebServiceHandler getFollowingList:requestDict andDelegate:self];
    }
}

/*-------------------------------------------*/
#pragma mark -
#pragma mark - viewDidLoad methods defination
/*-------------------------------------------*/

-(void)navigationBarCustomization {
    self.navigationItem.title = self.navigationTitle;
    [self createNavLeftButton];
}

/*-------------------------------------------*/
#pragma mark
#pragma mark - navigation bar buttons
/*-------------------------------------------*/

- (void)createNavLeftButton {
    UIButton *navCancelButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [navCancelButton rotateButton];
    [navCancelButton setImage:[UIImage imageNamed:mNavigationBackButtonImageName]forState:UIControlStateNormal];
    [navCancelButton setImage:[UIImage imageNamed:mNavigationBackButtonImageName] forState:UIControlStateSelected];
    [navCancelButton addTarget:self action:@selector(backButtonClicked)  forControlEvents:UIControlEventTouchUpInside];
    [navCancelButton setFrame:CGRectMake(0,0,40,40)];
    // Create a container bar button
    UIBarButtonItem *containingcancelButton = [[UIBarButtonItem alloc] initWithCustomView:navCancelButton];
    // UIBarButtonItem *homeButton = [[UIBarButtonItem alloc] initWithCustomView:segmentView];
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    
    negativeSpacer.width = -14;// it was -6 in iOS 6
    [self.navigationItem setLeftBarButtonItems:[NSArray arrayWithObjects:negativeSpacer,containingcancelButton, nil] animated:NO];
}

- (void)backButtonClicked {
    [self.navigationController popViewControllerAnimated:YES];
}


-(void)sendNewFollowStatusThroughNotification:(NSString *)userName andNewStatus:(NSString *)newFollowStatus {
    NSDictionary *newFollowDict = @{@"newFollowStatus"     :flStrForObj(newFollowStatus),
                                    @"userName"            :flStrForObj(userName),
                                  };
    [[NSNotificationCenter defaultCenter] postNotificationName:@"updatedFollowStatus" object:[NSDictionary dictionaryWithObject:newFollowDict forKey:@"newUpdatedFollowData"]];
}

/*---------------------------------------------------------*/
#pragma mark
#pragma mark -tableview delegates and datasource methods.
/*---------------------------------------------------------*/

/**
 *  tableView Delegates
 *  @return numberOfRowsInSection
 */

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if ([self.navigationTitle isEqualToString:NSLocalizedString(navTitleForFollowers, navTitleForFollowers)]) {
        return  followersresponseData.count;
    }
    else if ([self.navigationTitle isEqualToString:NSLocalizedString(navTitleForFollowing, navTitleForFollowing)]) {
        return followingresponseData.count;
    }
    else if ([self.navigationTitle isEqualToString:NSLocalizedString(navTitleForLikes, navTitleForLikes)]) {
        return likesData.count;
    }
    else
        return 0;
}

/*
 *  @return customized cell
 */
-(void)updateFollowButtonTitle :(NSInteger )row and:(id)sender {
    
    
    UIButton *reeceivedButton = (UIButton *)sender;
    
    reeceivedButton .layer.cornerRadius = 5;
    reeceivedButton .layer.borderWidth = 1;
    
    
    //  if follow status is 0 ---> title as "Requested"
    //  if follow status is 1 ---> title as "Following"
    //  if follow status is nil ---> title as "Follow"
    
   
      
    
    
    if ([arrayOfFollowingStaus[row]  isEqualToString:@"0"]) {
        [reeceivedButton  setTitle:@" REQUESTED" forState:UIControlStateNormal];
        
        [reeceivedButton  setTitleColor:[UIColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:1.0] forState:UIControlStateNormal];
        [reeceivedButton setImage:[UIImage imageNamed:@"edit_profile_two_timing_icon"] forState:UIControlStateNormal];
        reeceivedButton.backgroundColor = [UIColor colorWithRed:0.7804 green:0.7804 blue:0.7804 alpha:1.0];
        reeceivedButton .layer.borderColor = [UIColor clearColor].CGColor;
    }
    else if(([arrayOfFollowingStaus[row]  isEqualToString:@"1"])) {
        [reeceivedButton makeButtonAsFollowing];
    }
    else {
        
        [reeceivedButton makeButtonAsFollow];
    }
    
    
    
    
    if ([arrayOfusername[row]  isEqualToString:flStrForObj([Helper userName])]) {
        reeceivedButton.hidden = YES;
    }
    else {
        reeceivedButton.hidden = NO;
    }
    
    reeceivedButton.tag = 1000 + row;
    [reeceivedButton addTarget:self
                                action:@selector(cellFollowButtonAction:)
                      forControlEvents:UIControlEventTouchUpInside];
}

-(void)addProfilePic:(NSInteger )row profileImage:(UIImageView *)profileImageView{
    [profileImageView sd_setImageWithURL:[NSURL URLWithString:[arrayOfProfilePicUrl objectAtIndex:row]] placeholderImage:[UIImage imageNamed:@"defaultpp.png"]];
     [self.view layoutIfNeeded];
    profileImageView.layer.cornerRadius = profileImageView.frame.size.height/2;
    profileImageView.clipsToBounds = YES;
}

-(void)addUserDeatils:(NSInteger )row  usernameLabel:(UILabel *)usernameLabel fullnameLabel:(UILabel *)fullnameLabel{
    fullnameLabel.text = arrayOffullname[row];
    usernameLabel.text = arrayOfusername[row];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
      LikeTableViewCell *cell ;
    
    cell = [tableView dequeueReusableCellWithIdentifier:@"commentsCell"
                                                                          forIndexPath:indexPath];
    [self addUserDeatils:indexPath.row usernameLabel:cell.userNameLabelOutlet fullnameLabel:cell.NameLabelOutlet];
    
    [self updateFollowButtonTitle:indexPath.row and:cell.followButtonOutlet];
    [cell layoutIfNeeded];
    [self addProfilePic:indexPath.row profileImage:cell.profileImageViewOutlet];
    return cell;
}

-(void)cellFollowButtonAction:(id)sender {
    UIButton *selectedButton = (UIButton *)sender;
        if ([selectedButton.titleLabel.text isEqualToString:NSLocalizedString(followingButtonTitle, followingButtonTitle)]) {
            UIImageView *userImageView =[[UIImageView alloc] init];
            [userImageView sd_setImageWithURL:[NSURL URLWithString:flStrForObj(flStrForObj(arrayOfProfilePicUrl[selectedButton.tag%1000]))] placeholderImage:[UIImage imageNamed: @"defaultpp.png"]];
            [Helper showUnFollowAlert:userImageView.image and:flStrForObj(arrayOfusername[selectedButton.tag%1000]) viewControllerReference:self onComplition:^(BOOL isUnfollow)
             {
                 if(isUnfollow){
                     [self unfollowAction:sender];
                 }
             }];
        }
        else {
            
           
            arrayOfFollowingStaus[selectedButton.tag%1000] = @"1";
             [self sendNewFollowStatusThroughNotification:flStrForObj(arrayOfusername[selectedButton.tag%1000]) andNewStatus:@"1"];
            
            [selectedButton makeButtonAsFollowing];
            //passing parameters.
            NSDictionary *requestDict = @{muserNameTofollow     :arrayOfusername[selectedButton.tag %1000],
                                          mauthToken            :flStrForObj([Helper userToken]),
                                          };
            //requesting the service and passing parametrs.
            [WebServiceHandler follow:requestDict andDelegate:self];
        }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if([self.navigationTitle isEqualToString:NSLocalizedString(navTitleForFollowers, navTitleForFollowers)]) {
        ProfileViewController *newView = [self.storyboard instantiateViewControllerWithIdentifier:@"userProfileStoryBoardId"];
        newView.memberName = arrayOfusername[indexPath.row];
        newView.isMemberProfile = YES;
        newView.productDetails = YES;
        [self.navigationController pushViewController:newView animated:YES];
    }
    else if([self.navigationTitle isEqualToString:NSLocalizedString(navTitleForFollowing, navTitleForFollowing)]){
        ProfileViewController *newView = [self.storyboard instantiateViewControllerWithIdentifier:@"userProfileStoryBoardId"];
        newView.memberName = arrayOfusername[indexPath.row];
        newView.isMemberProfile = YES;
        newView.productDetails = YES;
        [self.navigationController pushViewController:newView animated:YES];
    }
    else if([self.navigationTitle isEqualToString:NSLocalizedString(navTitleForLikes, navTitleForLikes)]){
        ProfileViewController *newView = [self.storyboard instantiateViewControllerWithIdentifier:@"userProfileStoryBoardId"];
        newView.memberName = flStrForObj(likesData[indexPath.row][@"username"]);
        newView.isMemberProfile = YES;
        newView.productDetails = YES;
        [self.navigationController pushViewController:newView animated:YES];
    }
    else {
    }
 }


- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if([self.navigationTitle isEqualToString:NSLocalizedString(navTitleForLikes, navTitleForLikes)])
    {
        if(likesData.count >20) {
            if (indexPath.row == [likesData count] - 1 ) {
                ++likesDataPagingIndex;
                [self requestAllLikesOnPostWithIndex:likesDataPagingIndex];
            }
        }
    }
}


/*---------------------------------*/
#pragma mark -
#pragma mark - WebServiceDelegate
/*---------------------------------*/

//handling response
- (void) didFinishLoadingRequest:(RequestType)requestType withResponse:(id)response error:(NSError*)error {
    
    [[ProgressIndicator sharedInstance] hideProgressIndicator];
    [self stopAnimation];
    self.progressIndicatorView.hidden = YES;
    self.tableView.hidden = NO;
    
    if (error) {
        [Helper showAlertWithTitle:NSLocalizedString(alertError, alertError) Message:NSLocalizedString(mCommonServerErrorMessage, mCommonServerErrorMessage) viewController:self];
        return;
    }
    NSDictionary *responseDict = (NSDictionary*)response;
    
    //response for requesttype :: RequestTypeGetFollowingList.
    if (requestType == RequestTypeGetFollowingList ) {
        switch ([responseDict[@"code"] integerValue]) {
                //response code 200 for success.
            case 200: {
                [self handlingSuccessResponseOfFollowingList:responseDict];
            }
                break;
            default:
                 [self showingMessageForTableViewBackgroundAndType:2 ];
                break;
        }
    }
    
    //response for requesttype :: RequestTypeGetFollowersList.
    if (requestType == RequestTypeGetFollowersList ) {
        
        switch ([responseDict[@"code"] integerValue]) {
                //response code 200 for success.
            case 200 :{
                [self handlingSuccessResponseOfFollowersList:responseDict];
            }
                break;
            default:
                [self showingMessageForTableViewBackgroundAndType:1];
                break;
        }
    }
    //response for requesttype :: RequestTypeGetFollowersList.
    if (requestType == RequestTypeGetMemberFollowersList ) {
        
        switch ([responseDict[@"code"] integerValue]) {
                //response code 200 for success.
            case 200 :{
                [self handlingSuccessResponseOfMemberFollowersList:responseDict];
            }
                break;
            default:
                 [self showingMessageForTableViewBackgroundAndType:1 ];
                break;
        }
    }
    //response for requesttype :: RequestTypeGetFollowingList.
    if ( requestType == RequestTypeGetMemberFollowingList) {
        switch ([responseDict[@"code"] integerValue]) {
                //response code 200 for success.
            case 200: {
                [self handlingSuccessResponseOfMemberFollowingList:responseDict];
            }
                break;
            default:
                 [self showingMessageForTableViewBackgroundAndType:2 ];
                break;
        }
    }

    if (requestType == RequestTypeGetAllLikesOnPost) {
        [refreshControl endRefreshing];
        switch ([responseDict[@"code"] integerValue]) {
            case 200:
            {
                [self handlingSuccessResponseOfLikesList:responseDict[@"data"]];

            }
                break;
            default:
                if (likesData.count == 0)
                {
                [self showingMessageForTableViewBackgroundAndType:0];
                [self.tableView reloadData];
                }
                break;
        }
    }
}


-(void)handlingSuccessResponseOfLikesList:(NSMutableArray *)likesListData {
    
    if (likesListData.count) {
        
        self.tableView.backgroundView = nil ;
        if(likesDataPagingIndex == 0) {
            arrayOfusername =[[NSMutableArray alloc] init];
            arrayOffullname = [[NSMutableArray alloc] init];
            arrayOfProfilePicUrl =  [[NSMutableArray alloc] init];
            arrayOfFollowingStaus = [[NSMutableArray alloc] init];
            arrayOfMemberPrivateStatus = [[NSMutableArray alloc] init];
            [likesData removeAllObjects];
        }
            [likesData  addObjectsFromArray:likesListData];
        /*
         *  separating userName,fullname and thumbnailimageUrl from followingresponseData array and intialinzing in separate arrays.
         */
        for(int i = 0; i< likesData.count;i++) {
            NSString *userName = flStrForObj(likesData[i][@"username"]);
            NSString *fullName = flStrForObj( likesData[i][@"fullname"]);
            NSString *profilePicUrl = flStrForObj(likesData[i][@"profilePicUrl"]);
            NSString *followingstatus =  [NSString stringWithFormat:@"%@", likesData[i][@"userFollowRequestStatus"]];
            
            NSString *memberPrivateStatus = [NSString stringWithFormat:@"%@", likesData[i][@"memberPrivateFlag"]];
            
            //adding user names  to the array.
            [arrayOfusername addObject:userName];
            [arrayOffullname addObject:fullName];
            [arrayOfProfilePicUrl addObject:profilePicUrl];
            [arrayOfFollowingStaus addObject:followingstatus];
            [arrayOfMemberPrivateStatus addObject:memberPrivateStatus];
        }
        
        [self.tableView reloadData];
    }
}

//getting response of followersList api and converting into arrays(arrayOfFollowersusername,arrayOfFollowersname to populate the data in tableview).

-(void)handlingSuccessResponseOfFollowersList:(NSDictionary *)FollowersListData {
    if (FollowersListData) {
        //arrayOfFollowersusername(contains only Followersusername),arrayOfFollowersname(contains only name),followersresponseData(contains array of Followersusername and name),FollowersListData(dictonary contains data of success response).
        
        self.tableView.backgroundView = nil;
        arrayOfusername =[[NSMutableArray alloc] init];
        arrayOffullname = [[NSMutableArray alloc] init];
        arrayOfProfilePicUrl =  [[NSMutableArray alloc] init];
        arrayOfFollowingStaus = [[NSMutableArray alloc] init];
        followersresponseData =[[NSMutableArray alloc] init];
        arrayOfMemberPrivateStatus = [[NSMutableArray alloc] init];
        
        
        followersresponseData = FollowersListData[@"followers"];
        
        
        /*
         *  separating userName,fullname and thumbnailimageUrl from followersresponseData array and intialinzing in separate arrays.
         */
        for(int i = 0; i< followersresponseData.count;i++) {
            
            NSString *userName = followersresponseData[i][@"username"];
            NSString *fullName = flStrForObj( followersresponseData[i][@"fullname"]);
            NSString *profilePicUrl = flStrForObj(followersresponseData[i][@"profilePicUrl"]);
//            NSString *followingstatus =  [NSString stringWithFormat:@"%@", followersresponseData[i][@"FollowedBack"]];
            
            NSString *followingstatus =  [NSString stringWithFormat:@"%@", followersresponseData[i][@"userFollowRequestStatus"]];
            
             NSString *memberPrivateStatus = [NSString stringWithFormat:@"%@", followersresponseData[i][@"memberPrivateFlag"]];
            
            //adding user names  to the array.
            [arrayOfusername addObject:userName];
            [arrayOffullname addObject:fullName];
            [arrayOfProfilePicUrl addObject:profilePicUrl];
            [arrayOfFollowingStaus addObject:followingstatus];
            [arrayOfMemberPrivateStatus addObject:memberPrivateStatus];

        }
    }
    [self.tableView reloadData];
}

//getting response of followingList api and converting into arrays(arrayOfFollowingusername,arrayOfFollowingname to populate the data in tableview).
-(void)handlingSuccessResponseOfFollowingList:(NSDictionary *)FollowingListData {
    if (FollowingListData) {
        //arrayOfFollowingusername(contains only Followersusername),arrayOfFollowingname(contains only name),followingresponseData(contains array of Followingusername and name),FollowingListData(dictonary contains data of success response).
        self.tableView.backgroundView = nil;
        arrayOfusername =[[NSMutableArray alloc] init];
        arrayOffullname = [[NSMutableArray alloc] init];
        arrayOfProfilePicUrl =  [[NSMutableArray alloc] init];
        followingresponseData =[[NSMutableArray alloc] init];
         arrayOfFollowingStaus = [[NSMutableArray alloc] init];
        arrayOfMemberPrivateStatus = [[NSMutableArray alloc] init];
        followingresponseData = FollowingListData[@"result"];
    
        /*
         *  separating userName,fullname and thumbnailimageUrl from followingresponseData array and intialinzing in separate arrays.
         */
        for(int i = 0; i< followingresponseData.count;i++) {
            NSString *userName =flStrForObj( followingresponseData[i][@"username"]);
            NSString *fullName = flStrForObj( followingresponseData[i][@"fullName"]);
            NSString *profilePicUrl = flStrForObj(followingresponseData[i][@"profilePicUrl"]);
            NSString *followingstatus =  [NSString stringWithFormat:@"%@", followingresponseData[i][@"userFollowRequestStatus"]];
            
            NSString *memberPrivateStatus = [NSString stringWithFormat:@"%@", followingresponseData[i][@"memberPrivateFlag"]];
         
           // NSString *followingstatus =  [NSString stringWithFormat:@"%@",@"1"];
            [arrayOfFollowingStaus addObject:followingstatus];
            
         //adding user names  to the array.
            [arrayOfusername addObject:userName];
            [arrayOffullname addObject:fullName];
            [arrayOfProfilePicUrl addObject:profilePicUrl];
            [arrayOfMemberPrivateStatus addObject:memberPrivateStatus];
        }
    }
    else
    {
        [self showingMessageForTableViewBackgroundAndType:2];
    }
    [self.tableView reloadData];
}
//getting response of followingList api and converting into arrays(arrayOfFollowingusername,arrayOfFollowingname to populate the data in tableview).
-(void)handlingSuccessResponseOfMemberFollowingList:(NSDictionary *)FollowingListData {
  
    if (FollowingListData) {
        
        self.tableView.backgroundView = nil ;
        //arrayOfFollowingusername(contains only Followersusername),arrayOfFollowingname(contains only name),followingresponseData(contains array of Followingusername and name),FollowingListData(dictonary contains data of success response).
        arrayOfusername =[[NSMutableArray alloc] init];
        arrayOffullname = [[NSMutableArray alloc] init];
        arrayOfProfilePicUrl =  [[NSMutableArray alloc] init];
        arrayOfFollowingStaus = [[NSMutableArray alloc] init];
        arrayOfMemberPrivateStatus = [[NSMutableArray alloc] init];
        followingresponseData =[[NSMutableArray alloc] init];
        followingresponseData = FollowingListData[@"following"];
        /*
         *  separating userName,fullname and thumbnailimageUrl from followingresponseData array and intialinzing in separate arrays.
         */
        for(int i = 0; i< followingresponseData.count;i++) {
            NSString *userName =flStrForObj( followingresponseData[i][@"username"]);
            NSString *fullName = flStrForObj( followingresponseData[i][@"fullname"]);
            NSString *profilePicUrl = flStrForObj(followingresponseData[i][@"profilePicUrl"]);
       
            NSString *followingstatus =  [NSString stringWithFormat:@"%@", followingresponseData[i][@"userFollowRequestStatus"]];
            
            NSString *memberPrivateStatus = [NSString stringWithFormat:@"%@", followingresponseData[i][@"memberPrivateFlag"]];
            
            [arrayOfFollowingStaus addObject:followingstatus];
       
            //adding user names  to the array.
            [arrayOfusername addObject:userName];
            [arrayOffullname addObject:fullName];
            [arrayOfProfilePicUrl addObject:profilePicUrl];
            [arrayOfMemberPrivateStatus addObject:memberPrivateStatus];
        }
    }
    else
    {
        [self showingMessageForTableViewBackgroundAndType:2];
    }
    [self.tableView reloadData];
}

//getting response of followingList api and converting into arrays(arrayOfFollowingusername,arrayOfFollowingname to populate the data in tableview).

-(void)handlingSuccessResponseOfMemberFollowersList:(NSDictionary *)FollowersListData {

    if (FollowersListData) {
        //arrayOfFollowingusername(contains only Followersusername),arrayOfFollowingname(contains only name),followingresponseData(contains array of Followingusername and name),FollowingListData(dictonary contains data of success response).
        self.tableView.backgroundView = nil ;
        arrayOfusername =[[NSMutableArray alloc] init];
        arrayOffullname = [[NSMutableArray alloc] init];
        arrayOfProfilePicUrl =  [[NSMutableArray alloc] init];
        followersresponseData =[[NSMutableArray alloc] init];
         arrayOfFollowingStaus = [[NSMutableArray alloc] init];
        arrayOfMemberPrivateStatus =[[NSMutableArray alloc] init];
        followersresponseData = FollowersListData[@"memberFollowers"];
        
        /*
         *  separating userName,fullname and thumbnailimageUrl from followingresponseData array and intialinzing in separate arrays.
         */
        for(int i = 0; i< followersresponseData.count;i++) {
            NSString *userName = flStrForObj(followersresponseData[i][@"username"]);
            NSString *fullName = flStrForObj( followersresponseData[i][@"fullname"]);
            NSString *profilePicUrl = flStrForObj(followersresponseData[i][@"profilePicUrl"]);
            
            NSString *followingstatus =  [NSString stringWithFormat:@"%@", followersresponseData[i][@"userFollowRequestStatus"]];
            NSString *memberPrivateStatus = [NSString stringWithFormat:@"%@", followersresponseData[i][@"memberPrivateFlag"]];
            
            [arrayOfFollowingStaus addObject:followingstatus];

            //adding user names  to the array.
            [arrayOfusername addObject:userName];
            [arrayOffullname addObject:fullName];
            [arrayOfProfilePicUrl addObject:profilePicUrl];
            [arrayOfMemberPrivateStatus addObject:memberPrivateStatus];
        }
    }
    else
    {
        [self showingMessageForTableViewBackgroundAndType:1];
    }
    [self.tableView reloadData];
}

-(void)unfollowAction:(id)sender {
    UIButton *selectedButton = (UIButton *)sender;
    [selectedButton makeButtonAsFollow];
    arrayOfFollowingStaus[selectedButton.tag%1000] = @"2";
    
    [self sendNewFollowStatusThroughNotification:flStrForObj(arrayOfusername[selectedButton.tag%1000]) andNewStatus:@"2"];
    
    
    
    //passing parameters.
    NSDictionary *requestDict = @{muserNameToUnFollow: arrayOfusername[selectedButton.tag %1000],
                                  mauthToken            :flStrForObj([Helper userToken]),
                                  };
    //requesting the service and passing parametrs.
    [WebServiceHandler unFollow:requestDict andDelegate:self];
}

- (void)stopAnimation {
    __weak LikeViewController *weakSelf = self;
    int64_t delayInSeconds = 2.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        [weakSelf.tableView.infiniteScrollingView stopAnimating];
    });
}

#pragma  mark-
#pragma mark- BackgroundMessage

/**
 This method is invoked to show background with some text and image for TableView.
 In case if data list is empty.

 @param type Integer value for tags( likes, followers, following).
 */
-(void)showingMessageForTableViewBackgroundAndType :(NSInteger )type
{
    self.backgroundView.frame = CGRectMake(0, 0, self.tableView.bounds.size.width, self.tableView.bounds.size.height);
    switch (type) {
        case 0:
        {
            self.backgroundImageToShow.image = [UIImage imageNamed:@"empty_favourite"];
            self.labelForBackgroundMessage.text = NSLocalizedString(emptyLikesListLabelText, emptyLikesListLabelText);
        }
            break;
        case 1:
        {
            self.backgroundImageToShow.image = [UIImage imageNamed:@"empty followers"];
            self.labelForBackgroundMessage.text = NSLocalizedString(emptyFollowersListLabelText, emptyFollowersListLabelText);
        }
            break;
        case 2:
        {
            self.backgroundImageToShow.image = [UIImage imageNamed:@"empty following"];
            self.labelForBackgroundMessage.text = NSLocalizedString(emptyFollowingsListLabelText, emptyFollowingsListLabelText);
        }
            break ;
        default:
            break;
    }
    
    self.tableView.backgroundView = self.backgroundView;
}


@end
