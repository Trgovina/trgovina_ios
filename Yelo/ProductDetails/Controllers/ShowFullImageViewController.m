//
//  ShowFullImageViewController.m

//
//  Created by Rahul Sharma on 21/04/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import "ShowFullImageViewController.h"
#import "FontDetailsClass.h"
#import "TMImageZoom.h"

@interface ShowFullImageViewController () <UIScrollViewDelegate,UIGestureRecognizerDelegate>
{
    UIButton *navRight;
    UISwipeGestureRecognizer *swipeDown;
}
@end

@implementation ShowFullImageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self.navigationController.navigationBar setBackgroundColor:[UIColor clearColor]];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    self.navigationController.navigationBar.translucent = YES;
    
    UIButton *navLeft = [CommonMethods createNavButtonsWithselectedState:@"cross_showFullImage" normalState:@"cross_showFullImage"];
    navLeft.layer.masksToBounds = NO;
    navLeft.layer.shadowOffset = CGSizeMake(0, -2);
    navLeft.layer.shadowRadius = 2;
    navLeft.layer.shadowOpacity = 0.2;
    UIBarButtonItem *navLeftButton = [[UIBarButtonItem alloc]initWithCustomView:navLeft];
    [navLeft setFrame:CGRectMake(0,0,30,30)];
    self.navigationItem.leftBarButtonItem = navLeftButton;
    [navLeft addTarget:self action:@selector(closeButtonAction) forControlEvents:UIControlEventTouchUpInside];
    [self.navigationItem setLeftBarButtonItems:@[[CommonMethods getNegativeSpacer],navLeftButton]];
    
    navRight = [UIButton buttonWithType:UIButtonTypeCustom];
    NSString *titleForNuber = [NSString stringWithFormat:@"1/%lu",(unsigned long)_imageCount];
    [navRight setTitle:titleForNuber forState:UIControlStateNormal];
    navRight.layer.masksToBounds = NO;
    navRight.layer.shadowOffset = CGSizeMake(0, -2);
    navRight.layer.shadowRadius = 2;
    navRight.layer.shadowOpacity = 0.2;
    UIBarButtonItem *navRightButton = [[UIBarButtonItem alloc]initWithCustomView:navRight];
    [navRight setFrame:CGRectMake(0,0,30,30)];
    self.navigationItem.rightBarButtonItem = navRightButton;
    [self.navigationItem setRightBarButtonItems:@[[CommonMethods getNegativeSpacer],navRightButton]];
    [self createScrollViewWithImageView];
    swipeDown = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipeDown:)];
    [swipeDown setDirection: UISwipeGestureRecognizerDirectionDown];
    [self.view addGestureRecognizer:swipeDown ];
    scrollView.contentOffset = CGPointMake(self.imageTag *scrollView.frame.size.width, 0);
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(BOOL)prefersStatusBarHidden
{
    return YES;
}

#pragma mark-
#pragma mark - SwipeGesture
                                           
-(void)handleSwipeDown:(UISwipeGestureRecognizer *)swipe
 {
                                               
   [self dismissViewControllerAnimated:YES completion:nil];
 }
                                           
/**
 Navigation Close Button Action.
 */
-(void)closeButtonAction
{
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}

-(void)scrollViewDidScroll:(UIScrollView *)tempscrollView {
    if ([tempscrollView isEqual:scrollView]) {
        CGPoint offset = scrollView.contentOffset;
        if(offset.x == 0 ) {
            NSString *titleForNuber = [NSString stringWithFormat:@"1/%lu",(unsigned long)_imageCount];
            [navRight setTitle:titleForNuber forState:UIControlStateNormal];
        }
        else if( offset.x <=CGRectGetWidth(self.view.frame)) {
            NSString *titleForNuber = [NSString stringWithFormat:@"2/%lu",(unsigned long)_imageCount];
            [navRight setTitle:titleForNuber forState:UIControlStateNormal];
        }
        else if( offset.x <=CGRectGetWidth(self.view.frame)*2) {
            NSString *titleForNuber = [NSString stringWithFormat:@"3/%lu",(unsigned long)_imageCount];
            [navRight setTitle:titleForNuber forState:UIControlStateNormal];
        }
        else if( offset.x <=CGRectGetWidth(self.view.frame)*3) {
            NSString *titleForNuber = [NSString stringWithFormat:@"4/%lu",(unsigned long)_imageCount];
            [navRight setTitle:titleForNuber forState:UIControlStateNormal];
        }
        else if( offset.x <=CGRectGetWidth(self.view.frame)*4) {
            NSString *titleForNuber = [NSString stringWithFormat:@"5/%lu",(unsigned long)_imageCount];
            [navRight setTitle:titleForNuber forState:UIControlStateNormal];
        }
        
        // Set offset to adjusted value
        scrollView.contentOffset = offset;
    }
}

-(void)createScrollViewWithImageView
{
    scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0,0, self.view.frame.size.width, self.view.frame.size.height)];
    scrollView.translatesAutoresizingMaskIntoConstraints=NO;
    scrollView.delegate = self;
    scrollView.showsVerticalScrollIndicator = NO;
    scrollView.showsHorizontalScrollIndicator = NO;
    [scrollView setPagingEnabled:YES];
    [scrollView setAlwaysBounceVertical:NO];
    [scrollView setAlwaysBounceHorizontal:NO];
    scrollView.delegate=self;
    [scrollView layoutIfNeeded];
    scrollView.bounces = NO;
   
    for (int i = 0; i < self.imageCount; i++)
    {
        xOrigin = i * scrollView.frame.size.width;
        imageView = [[UIImageView alloc] initWithFrame:CGRectMake(xOrigin,0, self.view.frame.size.width, self.view.frame.size.height)]
        ;
         imageView.contentMode = UIViewContentModeScaleAspectFit;
        imageView.backgroundColor = [UIColor blackColor];
        [imageView sd_setImageWithURL:[NSURL URLWithString:[self.arrayOfImagesURL objectAtIndex:i]]placeholderImage:[UIImage imageNamed:@"itemProdDefault"]];
        
                if(i==0){
                    UITapGestureRecognizer *tapForImage = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showFullImage:)];
                    tapForImage.delegate =self;
                     imageView.userInteractionEnabled = YES;
                    tapForImage.numberOfTapsRequired = 1 ;
                    [imageView addGestureRecognizer:tapForImage];
                        }
        UIPinchGestureRecognizer *pinchZoom = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(pinch:)];
        [imageView addGestureRecognizer:pinchZoom];
        imageView.userInteractionEnabled = YES;
        [scrollView addSubview:imageView];
    }
    [scrollView setContentSize:CGSizeMake(scrollView.frame.size.width * self.imageCount,100)];
    [self.view addSubview:scrollView];
}

-(void)showFullImage:(UITapGestureRecognizer *)tap
 {
        UIImageView *tappedImageView = (UIImageView *)tap.view;
        
        NSArray *namesOfTaggedItems = [[NSArray alloc]init];
        NSArray *positionsOfNames = [[NSArray alloc]init];
        if(flStrForObj(self.TaggedProducts).length > 0){
            namesOfTaggedItems = [self.TaggedProducts componentsSeparatedByString:@","];
            positionsOfNames = [self.taggedCoordinates componentsSeparatedByString:@",,"];
            // if there is no one tagged then from response by defaultly we are getting undefined so handling that
            
            if (!tappedImageView.subviews.count) {
                for( int i = 0; i < namesOfTaggedItems.count; i++ ) {
                    UIButton *customButton;
                    customButton = [UIButton buttonWithType: UIButtonTypeCustom];
                    [customButton setBackgroundColor: [UIColor clearColor]];
                    [customButton setTitleColor:[UIColor blackColor] forState:
                     UIControlStateHighlighted];
                    //sets background image for normal state
                    customButton.userInteractionEnabled = NO;
                    [customButton setBackgroundImage:[UIImage imageNamed:
                                                      @"tag_people_tittle_btn"]
                                            forState:UIControlStateNormal];
                    
                    [customButton setBackgroundImage:[UIImage imageNamed:@"tag_people_tittle_btn"] forState:UIControlStateHighlighted];
                    [customButton setTitle:[namesOfTaggedItems objectAtIndex:i] forState:UIControlStateNormal];
                    
                    CGPoint fromPoint = CGPointFromString([positionsOfNames objectAtIndex:i]);
                    CGSize stringsize = [customButton.titleLabel.text sizeWithAttributes:@{NSFontAttributeName:[UIFont fontWithName:RobotoMedium size:13]}];
                    [customButton setFrame:CGRectMake(fromPoint.x,fromPoint.y, stringsize.width + 50, 35)];
                    //checking the poistion is going out of imaege  or not.
                    //if button poistion is out of image then need to alifgn properly.
                    if (fromPoint.x + customButton.frame.size.width > tappedImageView.frame.size.width || fromPoint.y + customButton.frame.size.height > tappedImageView.frame.size.height) {
                       // NSLog(@"custom button is out of view");
                        if (fromPoint.x + customButton.frame.size.width > tappedImageView.frame.size.width) {
                          //  NSLog(@"custom button is out of view along horizontal");
                            [customButton setFrame:CGRectMake(fromPoint.x - ((fromPoint.x + customButton.frame.size.width) - tappedImageView.frame.size.width),fromPoint.y, stringsize.width + 50, 35)];
                        }
                        else {
                           // NSLog(@"custom button is aligned properly along horizontal");
                        }
                        if( fromPoint.y + customButton.frame.size.height > tappedImageView.frame.size.height) {
                           // NSLog(@"custom button is out of view along vertical");
                            //[customButton setFrame:CGRectMake(fromPoint.x,fromPoint.y, stringsize.width + 50, 35)];
                            
                            [customButton setFrame:CGRectMake(fromPoint.x,fromPoint.y - ((fromPoint.y + customButton.frame.size.height) - tappedImageView.frame.size.height), stringsize.width + 50, 35)];
                        }
                        else {
                           // NSLog(@"custom button is aligned properly along vertical");
                        }
                        
                        if (fromPoint.x + customButton.frame.size.width > tappedImageView.frame.size.width && fromPoint.y + customButton.frame.size.height > tappedImageView.frame.size.height) {
                            [customButton setFrame:CGRectMake(fromPoint.x - ((fromPoint.x + customButton.frame.size.width) - tappedImageView.frame.size.width),fromPoint.y - ((fromPoint.y + customButton.frame.size.height) - tappedImageView.frame.size.height), stringsize.width + 50, 35)];
                            
                           // NSLog(@"custom button is out of view along both horizontal and vertical");
                        }
                    }
                    else {
                        [customButton setFrame:CGRectMake(fromPoint.x,fromPoint.y, stringsize.width + 50, 35)];
                    }
                    [customButton setTitleEdgeInsets:UIEdgeInsetsMake(5.0f, 0.0f, 0.0f, 0.0f)];
                    customButton.tag = 12345 + i;
                                       [UIView transitionWithView:tappedImageView
                                      duration:0
                                       options:UIViewAnimationOptionTransitionCrossDissolve
                                    animations:^{
                                        [tappedImageView addSubview:customButton];
                                        [tappedImageView bringSubviewToFront:customButton];
                                        [self.view layoutIfNeeded];
                                    }
                                    completion:NULL];
                }
            }
            else {
                for (UIButton *eachButton in tappedImageView.subviews) {
                    [UIView transitionWithView:tappedImageView
                                      duration:0
                                       options:UIViewAnimationOptionTransitionCrossDissolve
                                    animations:^{
                                        [eachButton removeFromSuperview];
                                        [self.view layoutIfNeeded];
                                    }
                                    completion:NULL];
                }
            }
        }
    }



- (void)pinch:(UIPinchGestureRecognizer *)gesture {
    
    [[TMImageZoom shared] gestureStateChanged:gesture withZoomImageView:(UIImageView *)gesture.view];
}




@end
