//  ProductDetailsViewController
//  Created by Ajay Thakur on 16/09/17.
//  Copyright © 2016 Rahul Sharma. All rights reserved.

#import <UIKit/UIKit.h>
#import "ProductDetails.h"
#import "PageControl.h"
@import GoogleMobileAds;
@protocol ProductDetailsDelegate <NSObject>
@required

/**
 Delegate Method to remove the product in Home screen if deleted by any user.

 @param indexpath indexPath for tracking product in homeScreen.
 */
-(void)productIsRemovedForIndex :(NSIndexPath *)indexpath ;

/**
 Delegate Method to maintain the click count for eventBase.

 @param updatedProduct product object to get the refrence.
 @param indexpath indexPath for tracking product in homeScreen.
 */
-(void)updateProductForClickCount:(ProductDetails *)updatedProduct  forIndex : (NSIndexPath *)indexpath  ;

@end

@interface ProductDetailsViewController : UIViewController <GADBannerViewDelegate >

#pragma mark -
#pragma mark - Non IB Properties -


@property BOOL dataFromHomeScreen ,isNewsFeedSocial ,noAnimation ;
@property(nonatomic, strong)UIImage *imageFromHome;
@property NSInteger movetoRowNumber;
@property NSString *postId,*currentCity ,*countryShortName,*currentLattitude,*currentLongitude,*activityUser,*postType,*navigationBarTitle  ;
@property(nonatomic, strong)NSIndexPath *indexPath;
@property double lat,log;
@property int currentIndex;



/**
 Object for ProductDetailsDelegate.
 */
@property (nonatomic,strong)id <ProductDetailsDelegate> productDelegate ;


/**
 Product Model object to get the refrence.
 */
@property (nonatomic, strong)ProductDetails *product ;

#pragma mark -
#pragma mark - UIView Outlets -

/**
 Banner view object for showing banner ads.
 */
@property (nonatomic, strong)GADBannerView *bannerView ;

/**
 UIView outlet Container for makeOffer Button.
 */
@property (strong, nonatomic) IBOutlet UIView *viewForMakeOffer;

/**
 UITableView Outlet for showing the properties, details of product.
 */
@property (weak, nonatomic) IBOutlet UITableView *tableView;

/**
 UIView Outlet.Actually a container for floating chat button.
 */
@property (weak, nonatomic) IBOutlet UIView *floatingButtonView;

#pragma mark -
#pragma mark - Label Outlets -


/**
 UILabel outlet for showing the price of product.
 */
@property (strong, nonatomic) IBOutlet UILabel *priceOutlet;

/**
 UIlabel outlet to show product is negotable or not.
 */
@property (strong, nonatomic) IBOutlet UILabel *offerNegotLabel;

/**
 UILabel outlet for product currency.
 */
@property (strong, nonatomic) IBOutlet UILabel *offercurrencyLabel;

#pragma mark -
#pragma mark - Button Outlets -

/**
 Make offer Button Outlet.To create an offer on product.
 */
@property (strong, nonatomic) IBOutlet UIButton *makeOfferOutlet;

/**
 Direct chat button outlet.
 */
@property (weak, nonatomic) IBOutlet UIButton *floatingButtonOutlet;


/**
 Edit Product Button outlet.
 */
@property (weak, nonatomic) IBOutlet UIButton *editButtonOutlet;

#pragma mark -
#pragma mark - Button Actions -

/**
 Like product Button. This button action provide option for user
 to like and unlike the product.
 
 @param sender likeButton Outlet in Like tableview cell.
 */
- (IBAction)likeButtonAction:(id)sender;

/**
 This action provides the list of all users who has liked
 this product earlier.
 
 @param sender UIButton Outlet.
 */
- (IBAction)numberOfLikesButtonAction:(id)sender;

/**
 Show actionsheet with sharing options.
 
 @param sender shareButton Outlet.
 */
- (IBAction)shareButtonAction:(id)sender;

/**
 This action method redirect user to Add Review for product.
 
 @param sender UIButton Outlet.
 */
- (IBAction)viewAllCommentsButtonAction:(id)sender;

/**
 Show profile of user who has posted the product.
 
 @param sender UIButton Outlet.
 */
- (IBAction)showProfileButton:(id)sender;

/**
 Follow Button Action. By clicking Follow button user can
 follow and unfollow members.
 
 @param sender UIButton Outlet.
 */
- (IBAction)followButtonAction:(id)sender;

/**
 Floating Chat Button Action method.
 On clicking Floating chat button user will redirect to chat directly.
 
 @param sender UIButton Outlet.
 */
- (IBAction)FloatingButtonAction:(id)sender;

/**
 Make Offer button Action method. This method will create offer on product.
 
 @param sender UIButton Outlet.
 */
- (IBAction)makeOfferButton:(id)sender;

- (IBAction)editButtonAction:(id)sender;
#pragma mark -
#pragma mark - NSConstraints Outlets -

/**
 NSLayout Top constraint. to set floating chat button intial position.
 */
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *floatingButtonTopConstraint;

@property (weak, nonatomic) IBOutlet UIView *swapOfferView;
- (IBAction)swapButtonAction:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *swapButtonOutlet;

/**
 This method return the currency symbol for given currency value.

 @param curr currencyValue.
 @return currencySymbol.
 */
-(NSString*)returnCurrency:(NSString *)curr;

/**
 Streching Header Outlets.
 */
@property (strong, nonatomic) IBOutlet UIScrollView *headerScrollView;
@property (strong, nonatomic) IBOutlet UIView *imgScrollBackView;
@property (strong, nonatomic) IBOutlet PageControl *pageControlView;

@end
