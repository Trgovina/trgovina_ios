//
//  LikersCollectionViewCell.h

//
//  Created by Rahul Sharma on 17/02/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LikersCollectionViewCell : UICollectionViewCell
@property (strong, nonatomic) IBOutlet UIImageView *userProfileImageView;

@end
