//
//  CellForProdSummry.m

//
//  Created by Rahul Sharma on 13/01/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import "CellForProdSummry.h"


@implementation CellForProdSummry

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


/**
 Update Fields in cell with data.

 @param product product details.
 */
-(void)updateFieldsForProductWithDataArray :(ProductDetails *)product
{
    self.productName.text = product.productName ;
    self.productType.text=  product.category ;
    self.subcategory.text = product.subcategory ;
    self.productType.text = [self.productType.text capitalizedString];
    NSString *timeStamp = [Helper convertEpochToNormalTimeInshort:product.postedOn];
    self.labelForTimeStamp.text = timeStamp;
    if(!(product.category.length && product.subcategory.length))
    {
        self.labelForTimeStamp.hidden = YES;
        self.dividerView.hidden = YES;
    }
    else
    {
        self.labelForTimeStamp.hidden = NO;
        self.dividerView.hidden = NO;
    }
    
}

@end
