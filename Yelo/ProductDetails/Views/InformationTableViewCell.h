//
//  InformationTableViewCell.h
//  CollegeStax
//
//  Created by 3Embed on 12/06/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InformationTableViewCell : UITableViewCell<UICollectionViewDelegate,UICollectionViewDataSource>

@property(weak,nonatomic)ProductDetails *product;
@property (weak, nonatomic) IBOutlet UICollectionView *propertiesCollectionView;

@end
