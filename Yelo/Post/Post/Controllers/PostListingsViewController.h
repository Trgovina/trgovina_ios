//
//  PostListingsViewController.h
//  Vendu
//
//  Created by Rahul Sharma on 19/03/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol updateCallBack <NSObject>

-(void)popToRootViewController :(BOOL)pop ;
@end;



/**
 Callback to maintain the images from Camera.
 
 @param arrayOfCameraImages array of captured images paths.
 */
typedef void (^CallBackForCameraScreen)(NSArray *arrayOfCameraImages);

@interface PostListingsViewController : UIViewController

@property (weak,nonatomic)id<updateCallBack>popDelegate;
@property BOOL editingPost, cameraFromTabBar;

@property (nonatomic,strong) ProductDetails *product;
@property (nonatomic,strong) Listings *listings;
@property (copy,nonatomic) CallBackForCameraScreen callBackForCamera ;

@property (nonatomic,strong) NSString *setCurrency;
@property (strong,nonatomic)NSMutableArray *arrayOfImagePaths;
@property (weak, nonatomic) IBOutlet UITableView *tableViewForListings;

@property (weak, nonatomic) IBOutlet UIButton *postButtonOutlet;
- (IBAction)postButtonAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

- (IBAction)tapGestureAction:(id)sender;

@property (strong, nonatomic) IBOutlet UITapGestureRecognizer *tapGestureOutlet;

@property (strong, nonatomic) IBOutlet UITableView *currencyTableView;


- (void) postToSocialMedia:(NSDictionary *)postDetails;

@end
