//
//  NegotiableTableViewCell.m
//  Vendu
//
//  Created by Rahul Sharma on 31/03/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

#import "NegotiableTableViewCell.h"
#import "SwapSuggessionViewController.h"

@implementation NegotiableTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

-(void)setStateForNegtiableSwitch :(BOOL)state
{
    self.negotiableSwitch.tag = 0;
    self.title.text = NSLocalizedString(productNegotiable, productNegotiable);
    [self.negotiableSwitch setOn: state];
    self.listing.negotiable = state;
}
-(void)setStateForWillingToExchange :(BOOL)state
{
    self.negotiableSwitch.tag = 1;
    self.title.text = @"Moguća zamjena";
    [self.negotiableSwitch setOn: state];
    self.listing.willingToExchange = state;
}

- (IBAction)switchAction:(id)sender {
    UISwitch *switchButton = (UISwitch *)sender;
    
    if(switchButton.tag == 0)
    {
        if(switchButton.on)
        {
            self.listing.negotiable = YES;
        }
        else
        {
            self.listing.negotiable = NO;
        }
        
    }
    else
    {
        if(switchButton.on)
        {
            self.listing.willingToExchange = YES;
            SwapSuggessionViewController *newVC = [self.refrenceVC.storyboard instantiateViewControllerWithIdentifier:@"swapSuggessionStoryboard"];
            newVC.listing = self.listing;
            newVC.refrenceVC = self.refrenceVC;
            UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:newVC];
            [self.refrenceVC presentViewController:navigationController animated:YES completion:nil];
        }
        else
        {
            self.listing.willingToExchange = NO;
        }
        
        
        NSIndexPath* rowToreload  = [NSIndexPath indexPathForRow:7 inSection:0];

        NSArray* rowsToReload = [NSArray arrayWithObjects:rowToreload, nil];
        
        [self.refrenceVC.tableViewForListings reloadRowsAtIndexPaths:rowsToReload withRowAnimation:UITableViewRowAnimationFade];
     
        
    }
    
    
}


@end

