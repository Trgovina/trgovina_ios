//
//  CurrencyPopUpCell.h
//  Trgovina
//
//  Created by apple on 21/11/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CurrencyPopUpCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *currencyPopUpLabel;



@end

NS_ASSUME_NONNULL_END
