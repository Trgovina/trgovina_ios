//
//  NegotiableTableViewCell.h
//  Vendu
//
//  Created by Rahul Sharma on 31/03/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PostListingsViewController.h"

@interface NegotiableTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet SwitchRTL *negotiableSwitch;

@property(nonatomic,strong)PostListingsViewController *refrenceVC;
@property (nonatomic,strong) Listings *listing ;

-(void)setStateForNegtiableSwitch :(BOOL)state;
-(void)setStateForWillingToExchange :(BOOL)state;
@property (weak, nonatomic) IBOutlet UILabel *title;

@end
