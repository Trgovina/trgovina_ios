//
//  ShareTableViewCell.m
//  Vendu
//
//  Created by Rahul Sharma on 19/03/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

#import "ShareTableViewCell.h"

@implementation ShareTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
     [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(fbNotification:)name:@"facebookCancel" object:nil];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setMediaTitle :(NSString *)title mediaIcon_Off :(NSString *)mediaIconOff andMediaIcon_On :(NSString *)mediaIconOn
{
    [self.shareMediaButtonOutletForTitle setTitle:title forState:UIControlStateNormal];
    [self.shareMediaButtonOutletForTitle setTitle:title forState:UIControlStateSelected];
    
     [self.shareMediaButtonOutletForTitle setImage:[UIImage imageNamed:mediaIconOff] forState:UIControlStateNormal];
     [self.shareMediaButtonOutletForTitle setImage:[UIImage imageNamed:mediaIconOn] forState:UIControlStateSelected];
    
 }

- (IBAction)switchAction:(id)sender {
    
    UISwitch * switchButton = (UISwitch *)sender;
    switch (switchButton.tag) {
        case 0:
            if (self.switchOutlet.on) {
                [Helper checkFbLoginforViewController:self.refrenceVC ];
                self.shareMediaButtonOutletForTitle.selected =YES;
                self.listing.isFacebookShare = YES ;
            }
            else {
                self.shareMediaButtonOutletForTitle.selected = NO;
                self.listing.isFacebookShare = NO ;
            }
            break;
        case 1:
            if (self.switchOutlet.on) {
                self.shareMediaButtonOutletForTitle.selected =YES;
                self.listing.isTwitterShare = YES ;
            }
            else
            {
                self.shareMediaButtonOutletForTitle.selected = NO;
                self.listing.isTwitterShare = NO ;
            }
            break;
        case 2:
            if (self.switchOutlet.on ) {
                
                if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"instagram:app"]])
                {
                    self.shareMediaButtonOutletForTitle.selected = YES;
                    self.listing.isInstagramShare = YES ;
                }
                else
                {
                    self.switchOutlet.on = NO;
                    self.shareMediaButtonOutletForTitle.selected = NO;
                    self.listing.isInstagramShare = NO ;
                    [Helper showAlertWithTitle:NSLocalizedString(alertMessage,alertMessage) Message:NSLocalizedString(instagramIsNotInstalled,instagramIsNotInstalled) viewController:self.refrenceVC];
                    
                }
            }
            else {
                self.shareMediaButtonOutletForTitle.selected = NO;
                self.listing.isInstagramShare = NO ;
            }
            
            break;
        default:
            break;
    }
    
}


-(void)fbNotification:(NSNotification *)noti {
    if(self.switchOutlet.tag == 0)
    {
    self.switchOutlet.on = NO;
    self.listing.isFacebookShare = NO ;
    self.shareMediaButtonOutletForTitle.selected = NO;
    }
}

@end
