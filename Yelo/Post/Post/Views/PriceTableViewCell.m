//
//  PriceTableViewCell.m
//  Vendu
//
//  Created by Rahul Sharma on 19/03/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

#import "PriceTableViewCell.h"
#import "CurrencySelectVC.h"
#import "CurrencyPopUpCell.h"

@interface PriceTableViewCell()<CurrencyDelegate , UITextFieldDelegate,UITableViewDataSource, UITableViewDelegate>
{
    UIView *currencyPopUupView;
    NSArray *currencyData;
}
@end

@implementation PriceTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.labelEUR.textColor=[UIColor blackColor];
    self.currency.textColor=mBaseColor;
    currencyData   = [NSArray arrayWithObjects:@"EUR",@"DIN", nil];
    currencyPopUupView = [[UIView alloc]init];
    currencyPopUupView.hidden = YES;
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setPrice :(NSString *)price  currency :(NSString *)currency
{
    self.currency.text = currency ;
    self.priceTextField.text = price ;
}

/*- (IBAction)currencyButtonAction:(id)sender {
    
    if (currencyPopUupView.hidden){
        currencyPopUupView.hidden = NO;
   currencyPopUupView.frame = CGRectMake(self.frame.origin.x + self.frame.size.width - 160, self.frame.origin.y + 50,150 ,100 );

    currencyPopUupView.layer.borderWidth = 1 ;
    currencyPopUupView.layer.borderColor =[UIColor grayColor].CGColor;
    
    UITableView *popupTableView = self.refrenceVC.currencyTableView;
    
    popupTableView.frame = CGRectMake(0, 0, 150, 100);
    
    popupTableView.delegate = self;
    popupTableView.dataSource = self;
    
    [currencyPopUupView addSubview: popupTableView ];
    [self.refrenceVC.tableViewForListings addSubview:currencyPopUupView];
    }
    else{
        currencyPopUupView.hidden = YES;
        [currencyPopUupView removeFromSuperview];
    }
    
//    CurrencySelectVC *currencySelectVC = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"CurrencySelectVC"];
//
//    [currencySelectVC setDelegate:self];
//    currencySelectVC.previousSelection = self.currency.text ;
//    [self.refrenceVC.navigationController pushViewController:currencySelectVC animated:YES];
}
*/
- (IBAction)priceTextFieldValueChangedAction:(id)sender {
    
    self.listing.price =  flStrForObj(self.priceTextField.text);
}


#pragma mark - CurrencyDelegate Method
-(void) country:(CurrencySelectVC *)country didChangeValue:(id)value{
    [country setDelegate:nil];
    NSDictionary *countryDict = value;
    self.currency.text=[NSString stringWithFormat:@" %@  %@",[countryDict objectForKey:CURRENCY_CODE],[countryDict objectForKey:CURRENCY_SYMBOL]];
   self.refrenceVC.setCurrency = self.currency.text;
    self.listing.currency = [NSString stringWithFormat:@"%@",[countryDict objectForKey:CURRENCY_CODE]];
}

#pragma mark - TextField Delegate -

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    self.refrenceVC.tapGestureOutlet.enabled = YES ;
    *self.isPriceTextFieldEnable = YES ;
}

-(void)textFieldDidEndEditing:(UITextField *)textField reason:(UITextFieldDidEndEditingReason)reason
{
    self.listing.price = self.priceTextField.text ;
     self.refrenceVC.tapGestureOutlet.enabled = NO ;
    *self.isPriceTextFieldEnable = NO ;
}



#pragma mark - TableView

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return currencyData.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{

    CurrencyPopUpCell *cell =[tableView dequeueReusableCellWithIdentifier:@"currencyPopupCell"];
    cell.currencyPopUpLabel.text = currencyData[indexPath.row];
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    self.currency.text = currencyData[indexPath.row];
    self.refrenceVC.setCurrency = self.currency.text;
    self.listing.currency = currencyData[indexPath.row];
    currencyPopUupView.hidden = YES;
    [currencyPopUupView removeFromSuperview];

}
- (IBAction)buttonEURAction:(UIButton *)sender {
        self.labelEUR.textColor = mBaseColor;
    self.currency.textColor = [UIColor blackColor];
    self.refrenceVC.setCurrency = self.labelEUR.text;
    self.listing.currency=self.labelEUR.text;

}
- (IBAction)buttonDINAction:(UIButton *)sender {
    self.labelEUR.textColor = [UIColor blackColor];
    self.currency.textColor = mBaseColor;
    self.refrenceVC.setCurrency = self.currency.text;
    self.listing.currency=self.currency.text;

}
@end
