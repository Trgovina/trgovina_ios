//
//  Listings.h
//  Vendu
//
//  Created by Rahul Sharma on 20/03/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SubCategory.h"

@interface Listings : NSObject

@property(nonatomic,copy) NSString *titleOfPost;
@property(nonatomic,copy) NSString *descriptionForPost;
@property(nonatomic,copy) NSString *category;
@property(nonatomic,copy) NSString *condition;
@property(nonatomic,copy) NSString *currency;
@property(nonatomic,copy) NSString *price;
@property(nonatomic)BOOL  negotiable;
@property(nonatomic)BOOL  willingToExchange;
@property(nonatomic,copy) NSString *address;
@property(nonatomic) double lattitude ;
@property(nonatomic) double longitude ;
@property(nonatomic)BOOL  isFacebookShare;
@property(nonatomic)BOOL  isTwitterShare;
@property(nonatomic)BOOL  isInstagramShare;
@property(nonatomic,copy) NSArray * arrayOfImagePaths;
@property(nonatomic,copy) NSString *cityName;
@property(nonatomic,copy) NSString *countrySName;
@property(nonatomic,copy) NSString *postId;

@property(nonatomic,copy) NSString *categoryId;

@property(nonatomic,copy) NSString *fieldName;
@property(nonatomic,copy) NSString *subId;
@property(nonatomic,copy) NSString *values;

@property(nonatomic,copy) NSString *categoryNodeId;
@property(nonatomic,copy) NSString *subCategoryNodeId;
@property(nonatomic,copy) NSString *subCategoryId;
@property(nonatomic,copy) NSString *languageCode;
@property(nonatomic,copy) NSString *subCategory;
@property(nonatomic,copy) NSDictionary *filter;

//EDIT
@property(nonatomic,copy) NSString *editCategoryNodeId;
@property(nonatomic,copy) NSString *editSubCategoryNodeId;
// EXCHANGE
@property(nonatomic,retain)NSMutableArray *swapPostArray;
@property(nonatomic,copy)NSString *swapDescription;
@property(nonatomic,weak) UIViewController *refrenceVC;

-(BOOL)isListingDone;

@end
