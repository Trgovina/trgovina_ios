//
//  AlbumsTableViewCell.h

//
//  Created by Rahul Sharma on 18/04/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AlbumsTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIImageView *coverImageViewForAlbums;
@property (strong, nonatomic) IBOutlet UILabel *labelForAlbumName;
@property (strong, nonatomic) IBOutlet UILabel *labelForPhotosCount;

@end
