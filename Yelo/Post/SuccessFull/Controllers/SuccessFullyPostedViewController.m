//
//  SuccessFullyPostedViewController.m

//
//  Created by Rahul Sharma on 10/07/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import "SuccessFullyPostedViewController.h"

@interface SuccessFullyPostedViewController ()<FBSDKSharingDelegate>


@end

@implementation SuccessFullyPostedViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    if(self.isFacebookSharing)
    {
        self.okButtonOutlet.enabled = NO;
        [self.components shortenWithCompletion:^(NSURL *_Nullable shortURL,
                                                 NSArray *_Nullable warnings,
                                                 NSError *_Nullable error) {
            // Handle shortURL or error.
            if (error) {
                NSLog(@"Error generating short link: %@", error.description);
                return;
            }
            NSURL *shortenURL =  shortURL;
            [self ShowFacebookSharingDialog:shortenURL.absoluteString];
            self.okButtonOutlet.enabled = YES;
        }];
    }
    else if (self.isTwitterSharing)
    {
        self.okButtonOutlet.enabled = NO;
        [self shareOntwitter];
    }
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if (_isTwitterSharingPending)
    {
        self.okButtonOutlet.enabled = NO;
        [self shareOntwitter];
    }
    
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    _isTwitterSharingPending = _isTwitterSharing;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(BOOL)prefersStatusBarHidden
{
    return YES ;
}

- (IBAction)okaButtonAction:(id)sender {
    
    if(self.postingDelegate)
    {
        [_postingDelegate postingListedSuccessfully];
    }
    
}



-(void)ShowFacebookSharingDialog :(NSString *)url
{
    dispatch_async(dispatch_get_main_queue(), ^{
        FBSDKShareLinkContent  *content = [[FBSDKShareLinkContent  alloc]init];
        content.contentURL = [NSURL URLWithString:url];
        FBSDKShareDialog* dialog = [[FBSDKShareDialog alloc] init];
        dialog.mode = FBSDKShareDialogModeAutomatic;
        dialog.shareContent = content;
        [dialog setDelegate:self];
        dialog.fromViewController = self;
        [dialog show];
        
    });
}

-(void)shareOntwitter
{
    _isTwitterSharing = NO;
    [self.components shortenWithCompletion:^(NSURL *_Nullable shortURL,
                                             NSArray *_Nullable warnings,
                                             NSError *_Nullable error) {
        // Handle shortURL or error.
        if (error) {
            NSLog(@"Error generating short link: %@", error.description);
            return;
        }
        NSURL *shortenURL =  shortURL;
        NSArray *items = @[shortenURL];
        // build an activity view controller
        UIActivityViewController *controller = [[UIActivityViewController alloc]initWithActivityItems:items applicationActivities:nil];
        // and present it
        [Helper presentActivityController:controller forViewController:self];
        self.okButtonOutlet.enabled = YES;
        
    }];
    
}

- (IBAction)closeButtonAction:(id)sender {
    
    if(self.postingDelegate)
    {
        [_postingDelegate postingListedSuccessfully];
    }
    
    
}


@end


