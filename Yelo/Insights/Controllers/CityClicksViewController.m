//
//  CityClicksViewController.m
//  Trgovina
//
//  Created by Rahul Sharma on 08/08/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import "CityClicksViewController.h"
#import "InsightsTableviewCell.h"


@interface CityClicksViewController ()<UITableViewDataSource , UITableViewDelegate , WebServiceHandlerDelegate>

@end

@implementation CityClicksViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.title = NSLocalizedString(clicks, clicks);
    
    UIButton *navLeft = [CommonMethods createNavButtonsWithselectedState:mNavigationBackButtonImageName normalState:mNavigationBackButtonImageName];
    UIBarButtonItem *navLeftButton = [[UIBarButtonItem alloc]initWithCustomView:navLeft];
    [navLeft setFrame:CGRectMake(0,0,20,20)];
    self.navigationItem.leftBarButtonItem = navLeftButton;
    [navLeft addTarget:self action:@selector(backButtonAction) forControlEvents:UIControlEventTouchUpInside];
    [self.navigationItem setLeftBarButtonItems:@[[CommonMethods getNegativeSpacer],navLeftButton]];
    
    ProgressIndicator *loginPI = [ProgressIndicator sharedInstance];
    [loginPI showPIOnView:self.view withMessage:NSLocalizedString(LoadingIndicatorTitle, LoadingIndicatorTitle)];
    
    NSDictionary *requestDict = @{mauthToken : [Helper userToken],
                                       mpostid : self.postId,
                                       mCountryShortName : self.countrySName                                        };
    
    [WebServiceHandler getInsightsCityWise:requestDict andDelegate:self];
    
    

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
  
}


-(void)backButtonAction
{
    [self.navigationController popViewControllerAnimated:YES];
}


#pragma mark -
#pragma mark - TableView Datasource and Delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{

    return dataArray.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    InsightsTableviewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"InsightsCityClicksCell"];
    cell.cityNameLabel.text = dataArray[indexPath.row][@"city"] ;
    cell.clicksLabel.text = [dataArray[indexPath.row][@"count"]stringValue] ;
    return cell ;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 60;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    self.sectionHeaderView.frame = CGRectMake(0, 0, self.view.frame.size.width,60 );
    self.labelForSectionHeader.text = [NSString stringWithFormat:@"%@%@",@"Clicks from ",self.countryFullName];
    return self.sectionHeaderView;
}


#pragma mark -
#pragma mark - Webservice delegate

-(void)didFinishLoadingRequest:(RequestType)requestType withResponse:(id)response error:(NSError *)error
{
    [[ProgressIndicator sharedInstance] hideProgressIndicator];
    
    if (error) {
        return ;
    }
    
    if(requestType == RequestToGetCityInsights)
    {
        switch ([response[0][@"code"] integerValue])
        {
            case 200:
            {
                dataArray = response[0][@"data"];
                [self.cityClicksTableview reloadData];
            }
                break;
            default:
                break;
        }
        
    }

    
}






@end
