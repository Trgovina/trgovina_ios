//
//  WebViewForDetailsVc.m

//
//  Created by Rahul_Sharma on 21/10/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import "WebViewForDetailsVc.h"

@interface WebViewForDetailsVc ()<UIWebViewDelegate>
{
    UIActivityIndicatorView *avForCollectionView;
}
@end

@implementation WebViewForDetailsVc

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    NSURL *url ;
    if (self.showTermsAndPolicy) {
        url = [NSURL URLWithString:TERMS_CONDITIONS_LINK];
        self.title = NSLocalizedString(navTitleForTerms, navTitleForTerms);
    }
    else if(self.forAdsUrl)
    {
        url = [NSURL URLWithString:self.adsurl] ;
    }
    else {
        url = [NSURL URLWithString:PRIVACY_LINK];
        self.title = NSLocalizedString(navTitleForPrivacy, navTitleForPrivacy);
    }

    
    NSURLRequest *requestURL = [NSURLRequest requestWithURL:url];
    [self.webViewOutlet loadRequest:requestURL];
    
    self.webViewOutlet.delegate =self;
    self.navigationItem.hidesBackButton = YES;
    [self createNavLeftButton];
    
    [self.webViewOutlet sizeToFit];
    
    [self addingActivityIndicatorToCollectionViewBackGround];
    
    self.navigationController.navigationBar.translucent = NO;
    self.navigationController.navigationBar.hidden = NO;
    
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    NSLog(@"Error : %@",error);
    [avForCollectionView stopAnimating];
    
}

-(void)webViewDidFinishLoad:(UIWebView *)webView {
    
    [avForCollectionView stopAnimating];
    
}
-(void)addingActivityIndicatorToCollectionViewBackGround {
    avForCollectionView = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    avForCollectionView.frame =CGRectMake(self.view.frame.size.width/2 -12.5, self.view.frame.size.height/2 -12.5, 25,25);
    avForCollectionView.tag  = 1;
    [self.view addSubview:avForCollectionView];
    [avForCollectionView startAnimating];
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)createNavLeftButton
{
    
    UIButton *navCancelButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [navCancelButton rotateButton];
    [navCancelButton setImage:[UIImage imageNamed:mNavigationBackButtonImageName]
                     forState:UIControlStateNormal];
    [navCancelButton setImage:[UIImage imageNamed:mNavigationBackButtonImageName]
                     forState:UIControlStateSelected];
    [navCancelButton addTarget:self
                        action:@selector(backButtonClicked)
              forControlEvents:UIControlEventTouchUpInside];
    [navCancelButton setFrame:CGRectMake(10.0f,0.0f,40,40)];
    
    // Create a container bar button
    UIBarButtonItem *containingcancelButton = [[UIBarButtonItem alloc] initWithCustomView:navCancelButton];

    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc]
                                       initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                       target:nil action:nil];
    negativeSpacer.width = -14;// it was -6 in iOS 6
    [self.navigationItem setLeftBarButtonItems:[NSArray arrayWithObjects:negativeSpacer,containingcancelButton, nil] animated:NO];
}

// hiding navigation bar and changing to previous controloler.

- (void)backButtonClicked
{
    if(self.forAdsUrl)
    {
        [self dismissViewControllerAnimated:NO completion:nil];
    }
    else
    {
   [self.navigationController popViewControllerAnimated:YES];
    }
}


@end
