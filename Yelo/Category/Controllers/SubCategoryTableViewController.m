//
//  SubCategoryTableViewController.m
//  CollegeStax
//
//  Created by 3Embed on 18/05/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

#import "SubCategoryTableViewController.h"
#import "SubCategoryTableViewCell.h"
#import "SubCategory.h"
#import "FilterFieldsViewController.h"
#import "Filter.h"
#import "PostListingsViewController.h"

@interface SubCategoryTableViewController ()<WebServiceHandlerDelegate>
{
    NSArray *subcategory;
}

@end

@implementation SubCategoryTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.backButtonOutlet setTitle:_categoryName forState:UIControlStateNormal];

    [self.backButtonOutlet setTitle:self.categoryName forState:UIControlStateNormal];
    [[ProgressIndicator sharedInstance]showPIOnView:self.view withMessage:LoadingIndicatorTitle];
    NSDictionary *param = @{@"categoryName":self.categoryObj.name,
                            @"categoryId":self.categoryObj.categoryNodeId,
                            mLanguageCode : [Helper currentLanguage],
                            };

    [WebServiceHandler getSubCategory:param andDelegate:self];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return subcategory.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    SubCategoryTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SubCategoryCell" forIndexPath:indexPath];
    cell.previousSelection = self.listing.subCategory;
    [cell setSubCategoryNames:subcategory[indexPath.row]];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    SubCategory *sub = subcategory[indexPath.row];
    
    if(sub.filterArray.count)
    {
    FilterFieldsViewController *filterFieldsVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SubcategoryFilterFieldsStoryboard"];
    filterFieldsVC.filterKeyValueDict = [NSMutableDictionary new];
    
        SubCategoryTableViewCell *subCategoryCell = [tableView cellForRowAtIndexPath:indexPath];
     if(!subCategoryCell.tickMarkImage.hidden)
     {
      [filterFieldsVC.filterKeyValueDict addEntriesFromDictionary:self.listing.filter];
      }
        filterFieldsVC.filter = sub.filterArray;
        filterFieldsVC.subcategoryObj = sub;
        filterFieldsVC.categoryObj = self.categoryObj;
    [self.navigationController pushViewController:filterFieldsVC animated:YES];
    }
    else
    {
        // For the case if there is no sub filters.
        for (UIViewController *viewController in [self.navigationController viewControllers]) {
            if([viewController isKindOfClass:[PostListingsViewController class]]){
                PostListingsViewController *newVC = (PostListingsViewController *)viewController;
                newVC.listings.category = self.categoryObj.name ;
                newVC.listings.subCategory = sub.subCategoryName;
                newVC.listings.categoryId = self.categoryObj.categoryNodeId;
                newVC.listings.categoryNodeId = self.categoryObj.catId;
                newVC.listings.subCategoryNodeId
                = sub.subCategoryNodeId ;
                newVC.listings.filter = nil;
                [newVC.tableViewForListings reloadData];
                [self.navigationController popToViewController:viewController animated:YES];
            }
        }
        
        
        
    }
    
}

#pragma mark - WebserviceHandler view data source

-(void)didFinishLoadingRequest:(RequestType)requestType withResponse:(id)response error:(NSError *)error
{
    [[ProgressIndicator sharedInstance]hideProgressIndicator];
    if(error)
    {
        
    }
    
    if(requestType == RequestTypeSubCategory)
    {
        switch ([response[@"code"]integerValue]) {
            case 200:
            {
                subcategory = [SubCategory arrayOfSubCategory:response[@"data"]];
                
                [self.tableViewOutlet reloadData];
            }
                break;
                
            default:
                break;
        }
        
    }
    
}

- (IBAction)backButtonButtonAction:(id)sender {
 [self.navigationController popViewControllerAnimated:YES];
}
@end


