//
//  SubCategoryTableViewCell.m
//  CollegeStax
//
//  Created by 3Embed on 18/05/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

#import "SubCategoryTableViewCell.h"

@implementation SubCategoryTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.valueLabel.text = @"";
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
// Set fields for subcategory.
-(void)setSubCategoryNames :(SubCategory *)subcategory
{
    if([subcategory.subCategoryName isEqualToString:_previousSelection])
    {
        self.tickMarkImage.hidden = NO;
    }
    else
    {
        self.tickMarkImage.hidden = YES;
    }
    
    self.titleLabel.text = subcategory.subCategoryName;
    [self.subCategoryImage sd_setImageWithURL:[NSURL URLWithString:subcategory.image] placeholderImage:[UIImage imageNamed:@"itemProdDefault"]];
}

// set Fields for filters.
-(void)setFieldsForFilter :(Filter *)filter previousSelection :(NSDictionary *)previousSet
{
    self.titleLabel.text = filter.fieldName;
    
    if([previousSet valueForKey:filter.fieldName])
    {
        @try {
             self.valueLabel.text = previousSet[filter.fieldName];
        } @catch (NSException *exception) {
           NSNumber *myNumber = previousSet[filter.fieldName];
           self.valueLabel.text = [myNumber stringValue];
            
        } @finally {
            
        }
       
        self.valueLabel.hidden = NO;
    }

    self.tickMarkImage.hidden = YES;
    if(filter.isMandatory){
    self.mandatory.hidden = NO;
    }
    else
    {
     self.mandatory.hidden = YES;
    }
}

// set Value for filters.
-(void)setValuesForFilter :(NSString *)value previousSelection :(NSString *)previousValue
{
    self.titleLabel.text = value;
    self.valueLabel.hidden = YES;
    
    if([value isEqualToString:previousValue])
    {
       self.tickMarkImage.hidden = NO;
    }
    else
    {
        self.tickMarkImage.hidden = YES;
    }
   
    
}

-(void)checkSelectedKeyValueFilter :(NSMutableArray *)keyValueArray forValue :(NSString *)value fieldName:(NSString *)fieldName
{
    for (NSDictionary *dict in keyValueArray) {
        
        if([dict[@"value"] isEqualToString:value] && [dict[@"fieldName"] isEqualToString:fieldName] )
        {
            self.tickMarkImage.hidden = NO;
            break;
        }

    }
    
}


@end
