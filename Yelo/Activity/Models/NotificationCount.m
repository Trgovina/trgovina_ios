//
//  Notification.m
//  Snapflipp
//
//  Created by Rahul Sharma on 29/11/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import "NotificationCount.h"

@implementation NotificationCount

static NotificationCount *sharedInstance ;

+(instancetype)sharedInstance
{
    if(sharedInstance == nil)
        sharedInstance = [NotificationCount new];
    return sharedInstance;
}


+(instancetype)initWithCount:(NSString *)labelCount
{
    NotificationCount *obj = [NotificationCount sharedInstance];
    obj.notificationsCount = flStrForObj(labelCount) ;
  return obj;
}

@end
