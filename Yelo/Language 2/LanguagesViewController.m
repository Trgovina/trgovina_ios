//
//  LanguagesViewController.m
//  MobiVenta
//
//  Created by Rahul Sharma on 11/01/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

#import "LanguagesViewController.h"
#import "LanguagesOptionTableCell.h"
#import "LanguageManager.h"
#import "AppDelegate.h"

@interface LanguagesViewController ()<UITableViewDelegate , UITableViewDataSource>
{
    NSArray *data;
    NSIndexPath * indxPathSelected ;
}

@end

@implementation LanguagesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    data = [LanguageManager languageStrings];
    [self.navigationItem setTitle:NSLocalizedString(language, language)];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return ELanguageCount;
}
    
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    LanguagesOptionTableCell *cell = [tableView dequeueReusableCellWithIdentifier:@"LanguageOptionCell"];
    
    if([LanguageManager currentLanguageString].length == 0)
    {
        cell.previousSelection = @"Serbian" ;
    }
    else
    {
     cell.previousSelection = [LanguageManager currentLanguageString];
    }
    
   [cell setLanguageName:data[indexPath.row] andIsSelected:indexPath.row == [LanguageManager currentLanguageIndex]];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
    {
        indxPathSelected = indexPath ;
        LanguagesOptionTableCell *cell=[tableView cellForRowAtIndexPath:indexPath];
        int i;
        for(i=0;i<ELanguageCount;i++)
        {
            NSIndexPath *ind =[NSIndexPath indexPathForRow:i inSection:0 ];
            LanguagesOptionTableCell *cell=[tableView cellForRowAtIndexPath:ind];
            if(i!=indexPath.row){
                cell.selectionImageView.hidden=YES;
            }
            
        }
        if(cell.selectionImageView.hidden){
            cell.selectionImageView.hidden=NO;
        }
    }

- (void)reloadRootViewController
{
   
    AppDelegate *delegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
     UIViewController* rootController = [storyboard  instantiateViewControllerWithIdentifier:@"TabBarStoryboardID"];
    delegate.window.rootViewController = rootController;
    
}

- (IBAction)doneButtonAction:(id)sender {
    
    UIAlertController *controller = [UIAlertController alertControllerWithTitle:@"" message:@"Would You like to change the language?" preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:NSLocalizedString(alertCancel, alertCancel) style:UIAlertActionStyleDefault handler:nil];
    
    UIAlertAction *yesAction = [UIAlertAction actionWithTitle:NSLocalizedString(alertYes, alertYes) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        [LanguageManager saveLanguageByIndex:indxPathSelected.row];
        [self reloadRootViewController];
    }];
    [controller addAction:cancelAction];
    [controller addAction:yesAction];
    [self presentViewController:controller animated:YES completion:nil];
   
}
@end
