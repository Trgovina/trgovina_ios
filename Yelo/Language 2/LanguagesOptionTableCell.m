//
//  LanguagesOptionTableCell.m
//  MobiVenta
//
//  Created by Rahul Sharma on 11/01/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

#import "LanguagesOptionTableCell.h"

@implementation LanguagesOptionTableCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setLanguageName:(NSString *)name andIsSelected:(BOOL)selected {
    self.languageLabel.text = name;
    
    if([self.previousSelection isEqualToString:self.languageLabel.text])
    {
        self.selectionImageView.hidden = NO;
    }
    else
    {
        self.selectionImageView.hidden = YES;
    }

}

@end
