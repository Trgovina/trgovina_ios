

//
//  Created by Rahul Sharma on 19/04/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OtpScreenViewController : UIViewController <WebServiceHandlerDelegate,UITextFieldDelegate>
{
    NSString *receivedOTP;
    NSString *getCodeFromTextfields;
}

@property(nonatomic) NSString *numb;
@property (nonatomic)BOOL  updateNumber;
// UIOutlets

@property (weak, nonatomic) IBOutlet UIButton *backButtonOutLet;

@property (weak, nonatomic) IBOutlet UILabel *phnNumbLabel;
@property (strong, nonatomic) IBOutlet UIButton *continueButtonOutlet;

@property (strong,nonatomic) NSDictionary *paramsDict;

//button actions

- (IBAction)continueButtonAction:(id)sender;


//ACTIVITY view.
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityViewOutlet;

- (IBAction)textFieldValueChangedAction:(id)sender;
- (IBAction)tapToDismissKeyboard:(id)sender;
@property (strong, nonatomic) IBOutlet UITextField *otpTextfield1;
@property (strong, nonatomic) IBOutlet UITextField *otpTextfield2;
@property (strong, nonatomic) IBOutlet UITextField *otpTextfield3;
@property (strong, nonatomic) IBOutlet UITextField *otpTextfield4;
@property (strong, nonatomic) IBOutlet UITextField *otpTextField5;
@property (strong, nonatomic) IBOutlet UITextField *otpTextfield6;

- (IBAction)backButtonAction:(id)sender;
- (IBAction)resendButtonAction:(id)sender;
@property (strong, nonatomic) IBOutlet UIView *timerView;
@property (strong, nonatomic) IBOutlet UILabel *timerLabel;
@property (strong, nonatomic) IBOutlet UIButton *resendOutlet;

@end
