//
//  StoreIDs+CoreDataProperties.h
//
//  Created by Rahul Sharma on 12/04/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import "StoreIDs+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface StoreIDs (CoreDataProperties)

+ (NSFetchRequest<StoreIDs *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *documentid;
@property (nullable, nonatomic, copy) NSString *groupid;

@end

NS_ASSUME_NONNULL_END
