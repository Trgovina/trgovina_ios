//
//  CurrencySelectVC.h
//  ADCurrencyPicker
//
//  Created by Adnan on 12/10/15.
//  Copyright © 2015 TheGoal. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CurrencySelectVC;

@protocol CurrencyDelegate <NSObject>

@optional

-(void)country:(CurrencySelectVC *)country didChangeValue:(id)value;

@end


@interface CurrencySelectVC : UIViewController
@property (strong, nonatomic) IBOutlet UISearchBar *searchBarOutlet;
@property (strong,nonatomic) NSString *previousSelection ;
@property (strong, nonatomic) IBOutlet UITableView *tableViewOutlet;
@property (nonatomic, assign) id<CurrencyDelegate> delegate;
- (IBAction)currencyDoneButtonAction:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UIView *currencyPopupView;

@end
