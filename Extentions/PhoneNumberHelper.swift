//
//  PhoneNumberHelper.swift
//  Trgovina.co
//
//  Created by 3Embed Software Technologies Pvt LTd on 30/01/19.
//  Copyright © 2019 Rahul Sharma. All rights reserved.
//

import Foundation

class  PhoneNumberHelper:NSObject {
   @objc func isPhoneNumValidForFinal(phoneNumber: String , code:String) -> Bool{
        
        
       // let countrydet = CountriesData()
      //  let selectedCountry = countrydet.selectedCountryDetails(code:code)
        let maximumSelectedCountryCount = 12
        let minimumSelectedCountryCount = 6
        
        
//        if let countInString = selectedCountry["max_digits"] as? String {
//            if let count = Int(countInString) {
//                maximumSelectedCountryCount = count
//            }
//        }
//
//        if let countInString = selectedCountry["min_digits"] as? String {
//            if let count = Int(countInString) {
//                minimumSelectedCountryCount = count
//            }
//        }
    
        if(maximumSelectedCountryCount == phoneNumber.count || minimumSelectedCountryCount == phoneNumber.count) {
            return true
        } else if minimumSelectedCountryCount > phoneNumber.count {
            return false
        }
        else if maximumSelectedCountryCount < phoneNumber.count {
            return false
    }
        return true
    }
}
